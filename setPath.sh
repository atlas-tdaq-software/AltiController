#!/bin/bash

 export tdaq_release="tdaq-10-00-00"
 export alti_release="l1ct-10-00-00"
 export detcommon_version="23.0.13"
 export partition_name="PartitionAlti_CTP"
 export logs_path="/logs/${TDAQ_VERSION}" 
 export pc_host="pc-tbed-pub-27.cern.ch"
 export sbc_host="sbcl1ct-33.cern.ch"
 export tag_name="x86_64-centos7-gcc11-opt"
 export linux="centos7"

 find . -type f  -name 'editPartition.sh' -exec  sed -i "s#PARTITION_NAME#$partition_name#g" {} \;
 find . -type f  -name 'editPartition.sh' -exec  sed -i "s#TDAQ_RELEASE#$tdaq_release#g" {} \;
 find . -type f  -name 'runPartition.sh' -exec  sed -i "s#PARTITION_NAME#$partition_name#g" {} \;
 find . -type f  -name 'runPartition.sh' -exec  sed -i "s#TDAQ_RELEASE#$tdaq_release#g" {} \;
 find . -type f  -name 'PartitionAlti.data.xml' -exec  sed -i "s#LOCAL_PATH#$PWD#g" {} \;
 find . -type f  -name '*data.xml' -exec  sed -i "s#PARTITION_NAME#$partition_name#g" {} \;
 find . -type f  -name '*data.xml' -exec  sed -i "s#LOG_PATH#$logs_path#g" {} \;
 find . -type f  -name '*data.xml' -exec  sed -i "s#PC_HOST#$pc_host#g" {} \;
 find . -type f  -name '*data.xml' -exec  sed -i "s#SBC_HOST#$sbc_host#g" {} \;
 find . -type f  -name '*data.xml' -exec  sed -i "s#CTP_INST_PATH#$CTP_INST_PATH#g" {} \;
 find . -type f  -name '*data.xml' -exec  sed -i "s#ALTI_RELEASE#$alti_release#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#DETCOMMON_VERSION#$detcommon_version#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#TDAQ_RELEASE#$tdaq_release#g" {} \;
 find . -type f  -name '*data.xml' -exec  sed -i "s#TAG_NAME#$tag_name#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#LINUX#$linux#g" {} \; 
 
