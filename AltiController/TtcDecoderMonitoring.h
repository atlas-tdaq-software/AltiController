#ifndef TTCDECODERMONITORING_H
#define TTCDECODERMONITORING_H

// ALTI low-level software
#include "ALTI/AltiModule.h"


// OKS configuration
#include "ALTIDAL/AltiTTCdecoder.h"

// IS
#include <is/info.h>
#include <is/inforeceiver.h>
 
class TtcDecoderMonitoring {
 public:
  
  TtcDecoderMonitoring(LVL1::AltiModule&, std:: string, const ALTIDAL::AltiTTCdecoder&);
  virtual ~TtcDecoderMonitoring();
  
 private:
  
  // IS callback to stop the TTC decoder
  static void IScallback(ISCallbackInfo *);
  static void StopDecoder();

  static LVL1::AltiModule* m_alti;  
  static std::string m_uid;

  ISInfoReceiver*    m_is_receiver;
  static std::string m_is_value; 
  static u_int       m_is_index; 

};
#endif
