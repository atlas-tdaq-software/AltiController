#ifndef ASYNCMESSAGESESSION_H
#define ASYNCMESSAGESESSION_H


#include "asyncmsg/Server.h"
#include "asyncmsg/Session.h"
#include "asyncmsg/Message.h"
#include "asyncmsg/NameService.h"
#include "asyncmsg/UDPSession.h"

#include "AltiController/AltiMasterBusy.h"

#include <atomic>

namespace ALTI {
  
  /** ********************************************************************
   * helper classes for daq::asyncmsg communication used by TTC2LAN
   * ********************************************************************/
  
  
  /**
   * Message from TTC2LAN to HLTSV.
   *
   * Event update.
   * Payload: a single extendend L1 ID (32bit)
   */
  class Update : public daq::asyncmsg::OutputMessage {
  public:
    
    // Must be same in HLTSV
    static const uint32_t ID = 0x00DCDF51;
    
    explicit Update(uint32_t l1id) : m_l1_id(l1id) {}
    
    // Message boiler plate
    uint32_t typeId() const override { return ID; }
  
    uint32_t transactionId() const override { return 0; }

    void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const override
    {
      buffers.push_back(boost::asio::buffer(&m_l1_id, sizeof(m_l1_id)));
    }

  private:
    
    uint32_t m_l1_id;
    
  };
  
  
  enum class XONOFFStatus : uint32_t { OFF = 0, ON = 1 };
  
  /**
   * Message from HLTSV to TTC2LAN.
   *
   * Change XON/XOFF status. Used to throttle the sender.
   * Payload: a single 32bit word, 0 = OFF, 1 == ON.
   */
  class XONOFF : public daq::asyncmsg::InputMessage {
  public:
    
    // Must be same in HLTSV
    static const uint32_t ID = 0x00DCDF50;
    
  XONOFF() : m_on_off(XONOFFStatus::ON) { }
   
    // The message identifier
    uint32_t typeId() const override { return ID; }
    
    // The transaction ID, not used.
    uint32_t transactionId() const override { return 0; }

    // Provide receive buffers.
    void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers) override {
      buffers.push_back(boost::asio::buffer(&m_on_off, sizeof(m_on_off)));
    }
    
    // Access received data
    XONOFFStatus status() const{ return m_on_off; }
    
  private:
    XONOFFStatus m_on_off;
  };


  /** 
   *   The Session class to talk to the HLTSV.
   *
   * The constructor takes a reference to a variable that it
   * updates with the XON/XOFF status as it receives messages.
   * This is the way it interacts with the rest of the application.
   */

class Session : public daq::asyncmsg::Session {
public:
  Session(boost::asio::io_context& service, std::atomic<XONOFFStatus>& status, uint32_t &xonCnt, uint32_t &xoffCnt, AltiMasterBusy *master_busy)
    : daq::asyncmsg::Session(service),
      m_master_busy(master_busy),
      m_status(status),
      m_hltsv_xon_count(xonCnt),
      m_hltsv_xoff_count(xoffCnt) { }
    
    // no copying please, just use the shared_ptr<Session>
    Session(const Session&) = delete;
    Session& operator=(const Session& ) = delete;
    
  protected:
    
    void onOpen() noexcept override;
    void onOpenError(const boost::system::error_code& error) noexcept override { ERS_LOG("Open error..." << error); }
    void onClose() noexcept override { ERS_LOG("Session is closed");}
    void onCloseError(const boost::system::error_code& error) noexcept override { ERS_LOG("Close error..." << error); }
    
 
    // This is called when the message header has been parsed.
    // It gives us the chance to create an object specific for the message type.
    // We only expect a XONOFF message, so that's all we check.
    
    std::unique_ptr<daq::asyncmsg::InputMessage> createMessage(std::uint32_t typeId, std::uint32_t transactionId, std::uint32_t size) noexcept override;
    
    // This is called when the full message has been received.
    void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) override;
    void onReceiveError(const boost::system::error_code& error, std::unique_ptr<daq::asyncmsg::InputMessage> message) noexcept override { 
      ERS_LOG("Receive error: " << error);
    }

    void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> message) noexcept override {;}
    void onSendError(const boost::system::error_code& error,
		     std::unique_ptr<const daq::asyncmsg::OutputMessage> message) noexcept override;
  
  private:
    AltiMasterBusy *m_master_busy;
    std::atomic<XONOFFStatus>& m_status;
    std::atomic<bool>          m_running;
    uint32_t &m_hltsv_xon_count;
    uint32_t &m_hltsv_xoff_count;
};


}


#endif /* defined(__CtpTTC2LAN_xcode__AsyncMessageSession__) */
