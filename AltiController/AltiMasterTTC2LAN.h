#ifndef ALTIMASTERTTC2LAN_H
#define ALTIMASTERTTC2LAN_H

#include <boost/thread/mutex.hpp>
#include <thread>
#include <atomic>
#include <memory>


// DAQ software
#include "ROSCore/ScheduledUserAction.h"
#include "ipc/partition.h"

// ALTI low-level software
#include "ALTI/AltiModule.h"

// ALTI high-level software
#include "AltiController/AltiMasterBusy.h"
#include "AltiController/AltiMasterEcr.h"
#include "AltiController/AsyncMessageSession.h"

class AltiMasterTTC2LAN {

 public:
  AltiMasterTTC2LAN(LVL1::AltiModule& alti, std::string uid, AltiMasterBusy& master_busy, IPCPartition& p, u_int period);
  virtual ~AltiMasterTTC2LAN();
 

  // run control commands
  bool connect();
  void prepareForRun(); // activate L1ID polling thread
  void stopRecording(); // stop L1ID polling thread
  void disconnect();
 
  void reset();

  void pollL1ID();
  void readL1ID();
  uint32_t getLastL1ID();
 
  void publishToIS();

 private:

  // typedef for work guard in boost >= 1.87
  using work = boost::asio::executor_work_guard<boost::asio::io_context::executor_type>;

  void sendMessage(uint32_t l1id);
  
  LVL1::AltiModule* m_alti;
  std::string m_uid;

  AltiMasterBusy* m_master_busy;
  
  IPCPartition m_ipc_partition;
 

  // EventCounts
  LVL1::EventIdentifier m_last_l1id;


  // statistics
  uint32_t m_current_l1id;
  uint64_t m_event_count;
  uint32_t m_hltsv_xon_count;
  uint32_t m_hltsv_xoff_count;
  uint32_t m_number_of_messages;
  
  bool m_hltsv_xon;
  
  bool m_is_connected;
    
  // TTC2LAN objects
  std::thread                                    m_io_thread;
  std::thread                                    m_read_l1id_thread;
  boost::asio::io_context                        m_service;
  std::unique_ptr<work>                          m_work;
  std::shared_ptr<ALTI::Session>                 m_session;
  std::atomic<ALTI::XONOFFStatus>                m_status;
  
  // configuration
  bool m_poll_l1id;
  u_int m_period;
  boost::mutex  m_mutex;
};


#endif
