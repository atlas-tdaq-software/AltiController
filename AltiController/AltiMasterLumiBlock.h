#ifndef ALTIMASTERLUMIBLOCK_H
#define ALTIMASTERLUMIBLOCK_H

#include <boost/thread/mutex.hpp>

// DAQ software
#include "ROSCore/ScheduledUserAction.h"
#include "ipc/partition.h"

// ALTI high-level software
#include "AltiController/AltiMasterTTC2LAN.h"



class AltiMasterLumiBlock : public ROS::ScheduledUserAction {

 public:
  AltiMasterLumiBlock(std::string uid, AltiMasterBusy& master_busy, AltiMasterTTC2LAN& master_ttc2lan, IPCPartition& p);
  virtual ~AltiMasterLumiBlock();
  
  virtual void requestIncreaseLBN();
  virtual void getCurrentLBN(uint32_t& lbn, uint64_t& timestamp);
  virtual void setPeriod(uint32_t period_ms);
  virtual void setMinimumDistance(uint32_t distance_ms);
  virtual void enablePeriodicLumiBlocks();
  virtual void disablePeriodicLumiBlocks();
  virtual void enableLumiBlocks();
  virtual void disableLumiBlocks();
  virtual void reset(uint32_t run_number);
  virtual uint32_t getRunNumber();
  
  // called by the ScheduledUserAction
  virtual void reactTo() override;

 private:

  uint64_t getTimeNow();
  void increaseLBN();

  void publishLBToIS(); 
  void publishLBSettingsToIS();

  std::string m_uid;
  
  AltiMasterBusy* m_master_busy;
  AltiMasterTTC2LAN* m_master_ttc2lan;
  IPCPartition m_ipc_partition;
  
  bool m_lb_change_in_progress;

  uint64_t m_lb_period;
  uint64_t m_lb_min_distance;

  uint64_t m_next_lb_update;
  bool     m_next_lb_update_is_a_request;
  uint64_t m_veto_lb_update;
  bool m_periodic_lumiblocks_enabled;
  bool m_lumiblocks_enabled;
  
  uint64_t m_current_lb_start_time;
  uint32_t m_lbn;

  uint32_t m_run_number;
  uint64_t m_nl1a_last;

  boost::mutex  m_mutex;
};
#endif
