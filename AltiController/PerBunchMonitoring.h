#ifndef PERBUNCHMONITORING_H
#define PERBUNCHMONITORING_H

// ALTI low-level software
#include "ALTI/AltiModule.h"
#include "RCDVme/RCDCmemSegment.h"

// IS
#include <is/info.h>
#include <is/inforeceiver.h>

// root
#include <TH1F.h>


class PerBunchMonitoring {
 public:
  
  PerBunchMonitoring(LVL1::AltiModule&, std:: string);
  virtual ~PerBunchMonitoring();
  
 private:
    
  // IS callback to stop the TTC decoder
  static void IScallback(ISCallbackInfo *);
  
  static LVL1::AltiModule* m_alti;  
  static std::string m_uid;
  static IPCPartition  m_ipc_partition;
  
  static RCD::CMEMSegment* m_segment;
  
  // vector of PBM histograms: L1A, TTR1..3, BGo0..3
  // each histogram has 3564 (BCID) bins + underflow for LB number and overflow for turn counts
  static std::vector<TH1F*> m_histo;
  
  ISInfoReceiver*    m_is_receiver;

 
};

#endif
