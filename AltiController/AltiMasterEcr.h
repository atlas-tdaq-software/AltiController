#ifndef ALTIMASTERLUMIECR_H
#define ALTIMASTERLUMIECR_H

#include <string>
#include <boost/thread/mutex.hpp>

// DAQ software
#include "ipc/partition.h"

// ALTI low-level software
#include "ALTI/AltiModule.h"

// ALTI high-level software
#include "AltiController/AltiMasterBusy.h"

class AltiMasterEcr {

 public:
  AltiMasterEcr(LVL1::AltiModule& alti, std::string uid, IPCPartition& p);
  virtual ~AltiMasterEcr();

  void reset();
  void setPeriod(uint32_t period_ms);
  void setDeadtime(uint32_t pre_ecr_deadtime, uint32_t post_ecr_deadtime);
  void setLength(uint32_t length);
 
  int startEcr();
  uint32_t stopEcr(); // return nb_ecr_counter
 
  void publishToIS();
 
 private:
  
  LVL1::AltiModule* m_alti;
  std::string m_uid;

  IPCPartition m_ipc_partition;

  // period and length of ECR signal
  uint32_t m_ecr_period;
  uint32_t m_ecr_length;

  // deadtime in BC before and after ECR
  uint32_t m_pre_ecr_deadtime;
  uint32_t m_post_ecr_deadtime;
  
  // keep track of ECR counts
  uint32_t m_ecr_counter; // total number of ECR
  uint32_t m_previous_ecr_counter; // previous 8 bits ECR counter
  bool     m_is_started; // to send 1 ECR after resume command


  boost::mutex  m_mutex;
};
#endif
