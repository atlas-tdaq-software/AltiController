#ifndef ALTIMASTERBUSY_H
#define ALTIMASTERBUSY_H

#include <boost/thread/mutex.hpp>

// DAQ software
#include "ipc/partition.h"

// ALTI low-level software
#include "ALTI/AltiModule.h"


class AltiMasterBusy {

 public:
  AltiMasterBusy(LVL1::AltiModule& alti, std::string uid, IPCPartition& p);
  virtual ~AltiMasterBusy();

  // resets the busies to a safe state, with
  // triggerbusy on and runcontrolbusy=1
  virtual void reset();
  virtual void dump();

  virtual void setTriggerBusy();
  virtual void unsetTriggerBusy();
  
  virtual void setRunControlBusy();
  virtual void setRunControlBusy(int counter); 
  virtual void unsetRunControlBusy();

  virtual void setLumiBlockBusy();
  virtual void unsetLumiBlockBusy();

  virtual void setEcrBusy();
  virtual void unsetEcrBusy();

  virtual void setHLTCounterBusy();
  virtual void unsetHLTCounterBusy();
  
  virtual void setHLTSVBusy();
  virtual void unsetHLTSVBusy();

  virtual bool isBusy();
  virtual bool isTriggerBusy();
  virtual uint32_t isRunControlBusy();
  virtual bool isLumiBlockBusy();
  virtual bool isEcrBusy();
  virtual bool isHLTCounterBusy();
  virtual bool isHLTSVBusy();
  virtual bool isBusy(bool& trigger, 
		      uint32_t& runcontrol, 
		      bool& lumiblock, 
		      bool& ecr,
		      bool& hltcounter,
		      bool& hltsv);
		      
  //virtual bool AltiConstantBusyAsserted();
  virtual void publishToIS();


 private:
  
  void updateGlobalBusy();

  LVL1::AltiModule* m_alti;
  std::string m_uid;

  IPCPartition m_ipc_partition;

 // internal busy channels
  bool m_busy_trigger;
  uint32_t m_busy_counter_runcontrol;
  bool m_busy_lumiblock;
  bool m_busy_ecr;
  bool m_busy_hltcounter;
  bool m_busy_hltsv;
  bool m_global_busy; 

  boost::mutex  m_mutex;

};
#endif
