#ifndef ALTICONTROLLER_H
#define ALTICONTROLLER_H

// DAQ software
#include "ROSCore/ReadoutModule.h"
#include "DFSubSystemItem/Config.h"
#include "TriggerCommander/MasterTrigger.h"
#include "TriggerCommander/CommandedTrigger.h"
#include "ipc/partition.h"

// ALTI low-level software
#include "ALTI/AltiModule.h"
#include "ALTI/AltiConfiguration.h"
#include "ALTI/AltiCounter.h"
#include "ALTI/AltiBusyCounter.h"

// ALTI high-level software
#include "AltiController/AltiMasterBusy.h"
#include "AltiController/AltiMasterEcr.h"
#include "AltiController/AltiMasterLumiBlock.h"
#include "AltiController/AltiMasterTTC2LAN.h"
#include "AltiController/TtcDecoderMonitoring.h"

// OKS configuration
#include "ALTIDAL/AltiBoard.h"
#include "ALTIDAL/AltiModeCtpSlave.h"
#include "ALTIDAL/AltiModeAltiSlave.h"
#include "ALTIDAL/AltiModeLemoSlave.h"
#include "ALTIDAL/AltiModeAltiMaster.h"
#include "ALTIDAL/AltiModeAltiExpert.h"
#include "ALTIDAL/AltiCycleBase.h"
#include "ALTIDAL/AltiBGoChannel.h"
#include "ALTIDAL/AltiBGoBCR.h"
#include "ALTIDAL/AltiBGoECR.h"
#include "ALTIDAL/AltiCalibrationRequest.h"
#include "ALTIDAL/AltiTiming.h"
#include "ALTIDAL/AltiTTCdecoder.h"
#include "ALTIDAL/AltiPatternGeneratorConfig.h"
#include "ALTIDAL/AltiMiniCtpConfig.h"

// IS
#include "AltiController/AltiMonitoringIS2.h"
#include <is/info.h>
#include <is/inforeceiver.h>

namespace RCD {

  using namespace ROS;
   
  class AltiController : public ReadoutModule, public daq::trigger::MasterTrigger
    {
    public:
      
      AltiController();
      virtual ~AltiController() override;
     
      /**
       * Methods inherited from the Controllable class. 
       * This methods are called at state transitions.
       * Only those needed for Alti are overloaded.
       */
      virtual void configure(const daq::rc::TransitionCmd&) override;
      virtual void connect(const daq::rc::TransitionCmd&) override;
      virtual void prepareForRun(const daq::rc::TransitionCmd&) override;
      
      virtual void stopROIB(const daq::rc::TransitionCmd&) override;
      virtual void stopRecording(const daq::rc::TransitionCmd&) override;
      virtual void stopArchiving(const daq::rc::TransitionCmd&) override;
      
      virtual void publish() override; // publish counters to IS
      virtual void publishFullStats() override; // publish Alti configuration to IS
      
      virtual void disconnect(const daq::rc::TransitionCmd&) override;
      virtual void unconfigure(const daq::rc::TransitionCmd&) override;

      virtual void resynch(const daq::rc::ResynchCmd& cmd) override;
      virtual void subTransition(const daq::rc::SubTransitionCmd& cmd) override; 
      virtual void user(const daq::rc::UserCmd& usrCmd) override;
      
      // methods inherited from ReadoutModule 
      virtual const std::vector<DataChannel *> *channels () override;  // Get the list of channels connected to this ReadoutModule
      virtual void clearInfo () override; // Reset internal statistics
     
      // MasterTrigger interface implementation
      virtual daq::trigger::HoldTriggerInfo hold(const std::string& dm) override;
      virtual void resume(const std::string& dm) override;
      virtual void setPrescales(uint32_t l1p, uint32_t hltp) override;
      virtual void setL1Prescales(uint32_t l1p) override;
      virtual void setHLTPrescales(uint32_t hltp) override;
      virtual void increaseLumiBlock(uint32_t run_number) override;
      virtual void setLumiBlockInterval(uint32_t seconds) override;
      virtual void setMinLumiBlockLength(uint32_t seconds) override;
      virtual void setBunchGroup(uint32_t bg) override;
      virtual void setConditionsUpdate(uint32_t folder_index, uint32_t lb) override;
      virtual void setPrescalesAndBunchgroup(uint32_t l1p, uint32_t hltp, uint32_t bg) override;
      
    private:
         
      // configuration of each running mode
      int configureCtpSlave(const ALTIDAL::AltiModeCtpSlave* config, const std::vector<const ALTIDAL::AltiCalibrationRequest*>& requests);
      int configureAltiSlave(const ALTIDAL::AltiModeAltiSlave* config, const std::vector<const ALTIDAL::AltiCalibrationRequest*>& requests);   
      int configureLemoSlave(const ALTIDAL::AltiModeLemoSlave* config, const std::vector<const ALTIDAL::AltiCalibrationRequest*>& requests);   
      int configureAltiMaster(const ALTIDAL::AltiModeAltiMaster* config, const std::vector<const ALTIDAL::AltiCalibrationRequest*>& requests);
      int configureAltiExpert(const ALTIDAL::AltiModeAltiExpert* config);
      
      // input signals synchronisation and clock phase shift
      int configureTiming(const ALTIDAL::AltiTiming* timing);

      // pattern generator configuration
      int configurationPatternGenerator(const ALTIDAL::AltiPatternGeneratorConfig* config);
    
      // mini-CTP configuration
      int configurationMiniCtp(const ALTIDAL::AltiMiniCtpConfig* config);
      int getTriggerItemLut(const std::vector<const ALTIDAL::AltiMiniCtpItem*> items, std::vector<u_int> &lut); 
      int getTriggerTypeLut(const std::vector<const ALTIDAL::AltiMiniCtpItem*> items, std::vector<u_int> &lut); 

      // B-Go configuration 
      void resetBGoChannelConfiguration();
      void setBGoChannel(const ALTIDAL::AltiBGoChannel* BGoChannel);
      void setBGoBCR(const ALTIDAL::AltiBGoBCR* bcr);
      void setBGoECR(const ALTIDAL::AltiBGoECR* ecr);
      u_int getCycle(const ALTIDAL::AltiCycleBase* const&);
      std::vector<u_int> getCycles(const std::vector<const ALTIDAL::AltiCycleBase*>& v);
      int setBGoCycles(std::vector<std::vector<u_int> >& cycles);
      int sendAsyncCycles(std::vector<u_int>& cycles);
      void dumpCycles(std::vector<u_int>& cycles);

      // calibration requests configuration
      int configureCalibrationRequests(const std::vector<const ALTIDAL::AltiCalibrationRequest*>& requests, LVL1::AltiModule::CALREQ_OUTPUT output);

      // clock setup
      int readClockStatus();
  
      // write the configuration in the HW
      int writeConfiguration();

      // read counters
      int readCounters(bool print_log);
      int readMinictpCounters(bool print_log);

      // busy monitoring
      int configureBusyMonitoring();
      int readBusyMonitoring();

      // IS
      void publishAltiInfo(); 
      
      // master trigger
      void configureMasterTrigger(const ALTIDAL::AltiMasterTriggerConfig* config);

      // ALTI HW
      LVL1::AltiModule* m_alti;
      LVL1::AltiConfiguration* m_alti_config;
      
      // OKS configuration of the Alti board
      const ALTIDAL::AltiBoard* m_alti_dal;
      std::string m_IS_server; 
      bool m_do_monitoring;
      bool m_do_status_monitoring;
      bool m_do_minictp;
      std::string m_config_mode;
      std::string m_name;
      
      // Busy inputs: from CTP out, ALTI out, Nim in
      bool m_busy_from_ctp;  // uid 0
      bool m_busy_from_alti; // uid 1
      bool m_busy_from_nim;  // uid 2

      std::vector<std::string> m_busy_uid;

      // BGo and Asynchronous cycles
      std::vector<u_int> m_cycles_for_configure;
      std::vector<u_int> m_cycles_for_connect;
      std::vector<u_int> m_cycles_for_prepareForRun;
      std::vector<std::vector<u_int> > m_bgo_cycles;
    
      // master trigger
      daq::trigger::CommandedTrigger* m_commanded_trigger;

      bool m_is_master_trigger;
      AltiMasterBusy* m_master_busy;
      AltiMasterEcr* m_master_ecr;
      AltiMasterLumiBlock* m_master_lumiblock;
      AltiMasterTTC2LAN* m_master_ttc2lan;
      
      bool        m_use_pg;
      std::string m_start_pg_transition;
      bool        m_keep_pg_running;
    
      // ECR configurationin master mode: take ECR from HW or pattern
      bool m_ecr_periodic;
      u_int m_ecr_word;

      // LB configuration
      u_int m_lumiblock_period;
      u_int m_lumiblock_min_distance;
      
      // TTC2LAN configuration
      bool m_use_ttc2lan;
      bool m_ttc2lan_connected;

      // TTC decoder monitoring
      TtcDecoderMonitoring* m_ttc_decoder_monitoring; 

      // CMEM segment for monitoring counters
      std::unique_ptr<RCD::CMEMSegment> m_cmem;
      std::unique_ptr<RCD::CMEMSegment> m_busy_cmem;
      LVL1::AltiBusyCounter             m_busy_counter;

      // total number of L1A to stop the pattern generator
      u_int m_nb_l1a;
      u_int m_max_l1a;

      // IS
      IPCPartition m_ipc_partition;
      AltiMonitoringIS2 *m_is_alti;
    };
  
  inline const std::vector<DataChannel *> *AltiController::channels ()
  {
    return 0;
  }
}


#endif
