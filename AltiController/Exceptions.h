#ifndef ALTICONTROLLER_EXCEPTIONS_H_
#define ALTICONTROLLER_EXCEPTIONS_H_

#include "ers/ers.h"
#include "ers/Issue.h"

namespace ALTI {
  ERS_DECLARE_ISSUE(AltiController, Issue, ERS_EMPTY, ERS_EMPTY)
    
    ERS_DECLARE_ISSUE_BASE
    (AltiController,
     ClockLoss,
     AltiController::Issue,
     appId <<  " " << reason,
     ERS_EMPTY,
     ((std::string) appId )
     ((std::string) reason)
     )
   
    ERS_DECLARE_ISSUE_BASE
    (AltiController,
     VmeError,
     AltiController::Issue,
     appId <<  " " << reason,
     ERS_EMPTY,
     ((std::string) appId )
     ((std::string) reason)
     )
    
    
    ERS_DECLARE_ISSUE_BASE
    (AltiController,
     CannotConfigure,
     AltiController::Issue,
     appId <<  " " << reason,
     ERS_EMPTY,
     ((std::string) appId )
     ((std::string) reason )
     )

  
    ERS_DECLARE_ISSUE_BASE
    (AltiController,
     FailedFirstLumiBlock,
     AltiController::Issue,
     appId <<  " " << reason,
     ERS_EMPTY,
     ((std::string) appId )
     ((std::string) reason )
     )

    ERS_DECLARE_ISSUE_BASE
    (AltiController,
     FailedIncreaseLBN,
     AltiController::Issue,
     appId <<  " " << reason,
     ERS_EMPTY,
     ((std::string) appId )
     ((std::string) reason)
     )

    ERS_DECLARE_ISSUE_BASE
    (AltiController,
     PublishToIS,
     AltiController::Issue,
     appId <<  " " << reason,
     ERS_EMPTY,
     ((std::string) appId )
     ((std::string) reason )
     )
    
    ERS_DECLARE_ISSUE_BASE
    (AltiController,
     ReadFromIS,
     AltiController::Issue,
     appId <<  " " << reason,
     ERS_EMPTY,
     ((std::string) appId )
     ((std::string) reason )
     )

    ERS_DECLARE_ISSUE_BASE
    (AltiController,
     ReadFromTriggerDB,
     AltiController::Issue,
     appId <<  " " << reason,
     ERS_EMPTY,
     ((std::string) appId )
     ((std::string) reason )
     )

    }

#endif
