#include "AltiController/AltiController.h"
#include "AltiController/Exceptions.h"

#include <iostream>
#include <string>
#include <fstream>
#include <bitset>
#include "ers/Issue.h"
 
// OKS configuration
#include "DFdal/RCD.h"
#include "dal/MasterTrigger.h"
#include "dal/Detector.h"
#include "ALTIDAL/AltiMasterTriggerConfig.h"
#include "ALTIDAL/AltiBChannelConfig.h"
#include "ALTIDAL/AltiTransmitter.h"
#include "ALTIDAL/AltiCycleContainer.h"
#include "ALTIDAL/AltiCycleShort.h"
#include "ALTIDAL/AltiCycleLong.h"
#include "ALTIDAL/AltiBGoChannel.h"
#include "ALTIDAL/AltiPatternGeneratorConfig.h"
#include "ALTIDAL/PGSignalMask.h"
#include "ALTIDAL/TTCL1aSource.h"
#include "ALTIDAL/AltiBGo2L1A.h"
#include "ALTIDAL/AltiDeadtimeConfig.h"
#include "ALTIDAL/AltiMiniCtpItem.h"
#include "ALTIDAL/AltiMiniCtpItemInput.h"

// DAQ software
#include "TriggerCommander/CommandedTrigger.h"
#include "TriggerCommander/Exceptions.h"


// Trigger menu 
//#include "TrigConfIO/TrigDBL1BunchGroupSetLoader.h"
//#include "TrigConfData/L1BunchGroupSet.h"

// IS
#include "is/infodictionary.h"
#include "AltiController/EcrConfigIS2.h"
#include "AltiController/VoltageIS2.h"
#include "AltiController/TemperatureIS2.h"
#include "TTCInfo/ECR.h"
#include "rc/RunParams.h"

using namespace std;
using namespace RCD;
using namespace LVL1;
 

//========================================================================================================================
  
AltiController::AltiController() :
  m_alti(nullptr),
  m_IS_server(""),
  m_do_monitoring(true),
  m_do_status_monitoring(true),
  m_do_minictp(false),
  m_config_mode(""),
  m_name(""),
  m_busy_uid(3,"not_used"),
  m_commanded_trigger(nullptr),
  m_is_master_trigger(false),
  m_master_busy(nullptr),
  m_master_ecr(nullptr),
  m_master_lumiblock(nullptr),
  m_master_ttc2lan(nullptr),
  m_use_pg(false),
  m_start_pg_transition(""),
  m_keep_pg_running(false),
  m_ecr_periodic(false),
  m_ecr_word(2),
  m_lumiblock_period(60),
  m_lumiblock_min_distance(10),
  m_use_ttc2lan(false),
  m_ttc2lan_connected(false),
  m_ttc_decoder_monitoring(nullptr),
  m_nb_l1a(0),
  m_max_l1a(0),
  m_ipc_partition(getenv("TDAQ_PARTITION"))
{
  ERS_LOG(" Entered");

  m_alti_config = new AltiConfiguration();

  // allocate memory for counters and busy monitoring CMEM segments
  m_cmem.reset(new RCD::CMEMSegment("AltiController counters",  AltiModule::CNT_SIZE*sizeof(uint32_t), true));
  if((*m_cmem)() != CMEM_RCC_SUCCESS) {
    ostringstream text;
    text << "error opening CMEM segment  of size " << m_cmem->Size();
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  }
  printf("opened counters CMEM segment of size 0x%08x, phys 0x%08x, virt 0x%08x\n", m_cmem->Size(), (unsigned int) m_cmem->PhysicalAddress(), (unsigned int) m_cmem->VirtualAddress());
  
  m_busy_cmem.reset(new RCD::CMEMSegment("AltiController Busy FIFO", AltiModule::BSY_FIFO_SIZE*sizeof(uint32_t)));
  if((*m_busy_cmem)() != CMEM_RCC_SUCCESS) {
    ostringstream text;
    text << "error opening CMEM segment  of size " << m_busy_cmem->Size();
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  }
  printf("opened Busy monitoring CMEM segment of size 0x%08x, phys 0x%08x, virt 0x%08x\n", m_busy_cmem->Size(), (unsigned int) m_busy_cmem->PhysicalAddress(), (unsigned int) m_busy_cmem->VirtualAddress());
  m_busy_counter.segment(m_busy_cmem.get());

  ERS_LOG(" Done");
}
  
//========================================================================================================================
  
AltiController::~AltiController() noexcept {
  ERS_LOG(" Entered");

  if (m_alti != nullptr) {
    ERS_LOG(m_name << " delete m_alti = " << std::hex << m_alti);
    delete m_alti;
    m_alti = nullptr;
  }
  
  /* temp
  if (m_alti_config != nullptr) {
    ERS_LOG(m_name << " delete m_alti_config = " << std::hex << m_alti_config);
    delete m_alti_config;
    m_alti_config = nullptr;
    }*/
  
  if (m_commanded_trigger != nullptr) {
    ERS_LOG(m_name << " delete m_commanded_trigger = " << std::hex << m_commanded_trigger);
    m_commanded_trigger->_destroy();
    m_commanded_trigger = nullptr;
  }
  
  if (m_master_busy != nullptr) {
    ERS_LOG(m_name << " delete m_master_busy = " << std::hex << m_master_busy);
    delete m_master_busy;
    m_master_busy = nullptr;
  }
  
  if (m_master_ecr != nullptr) {
    ERS_LOG(m_name << " delete m_master_ecr = " << std::hex << m_master_ecr);
    delete m_master_ecr;
    m_master_ecr = nullptr;
  }
  
  if (m_master_lumiblock != nullptr) {
    ERS_LOG(m_name << " delete m_master_lumiblock = " << std::hex << m_master_lumiblock);
    delete m_master_lumiblock;
    m_master_lumiblock = nullptr;
  }
  
  if (m_master_ttc2lan != nullptr){
    ERS_LOG(m_name << " delete m_master+ttc2lan = " << std::hex << m_master_ttc2lan);
    delete m_master_ttc2lan;
    m_master_ttc2lan = nullptr;
  }
  
  if (m_ttc_decoder_monitoring != nullptr){
    ERS_LOG(m_name << " delete m_ttc_decoder_monitoring = " << std::hex << m_ttc_decoder_monitoring);
    delete m_ttc_decoder_monitoring;
    m_ttc_decoder_monitoring = nullptr;
  }

  ERS_LOG(" Done");
}
  
//========================================================================================================================
  
void AltiController::subTransition(const daq::rc::SubTransitionCmd& cmd) { 
    
  m_name = ROS::IOMPlugin::m_uid;

  ERS_LOG(m_name << " Entered");

  std::cout << "cmd.subTransition() " << cmd.subTransition() << std::endl;
  if(cmd.subTransition() != "ALTI_CONFIGURATION") return;
  
  int status = 0;
  
  // might be move to constructor to make sure the ALTI is configured first (for the clock)
  
  cout << m_name << "  - uid " <<  ROS::IOMPlugin::m_uid << endl;
  cout << m_name << "  - partition " << ROS::IOMPlugin::m_partition->UID() << endl;

  // retrieve ALTI configuration from OKS
  try {
    m_alti_dal =  ROS::IOMPlugin::m_configurationDB->get<ALTIDAL::AltiBoard>(m_uid);
  } 
  catch(daq::config::Exception& ex) {
    // This from the Configuration class
    ers::error(ALTI::AltiController::CannotConfigure(ERS_HERE, ROS::IOMPlugin::m_uid, "cannot access the database", ex));
  } 
 
  //uint8_t address = m_alti_dal->get_PhysAddress();
  int slot = m_alti_dal->get_slot();  
  ERS_LOG(m_name << " Found module \"" << ROS::IOMPlugin::m_uid << "\" in slot " << slot);// << "\" address " << address);
  
  m_IS_server = m_alti_dal->get_IS_server();
  ERS_LOG(m_name << " IS server: " << m_IS_server);
  
  m_do_monitoring = m_alti_dal->get_do_monitoring();
  ERS_LOG(m_name << " do monitoring: " << m_do_monitoring);
       
  // initialise IS
  try{ 
    m_is_alti = new AltiMonitoringIS2();
    
    m_is_alti->is_configured = false;

    m_is_alti->signals_switch.resize(AltiModule::SIGNAL_NUMBER_ROUTED );
    for(u_int sig=0; sig<AltiModule::SIGNAL_NUMBER_ROUTED; sig++) {
      m_is_alti->signals_switch[sig].name = AltiModule::SIGNAL_NAME[sig];
    }

    m_is_alti->transmitters.resize(AltiModule::TRANSMITTER_NUMBER);
    for(u_int tx=0; tx<AltiModule::TRANSMITTER_NUMBER; tx++) {
      m_is_alti->transmitters[tx].name = "TX" + to_string(tx);
    }

    m_is_alti->bgos.resize(4);
    for(u_int bgo=0; bgo<4; bgo++) {
      m_is_alti->bgos[bgo].name = "BGO" + to_string(bgo);
    }

    m_is_alti->rate_counters.resize(AltiModule::CNT_SIZE);
    for(u_int cnt = 0; cnt < AltiModule::CNT_SIZE; cnt++) {
      m_is_alti->rate_counters[cnt].name =  AltiCounter::CNT_NAME[cnt];
    }

    m_is_alti->minictp_counters.resize(AltiModule::MINICTP_CNT_SIZE_TOTAL);
    u_int idx = 0;
    for(u_int itm = 0; itm < AltiModule::ITEM_NUMBER; itm++) {
      for(u_int cnt = 0; cnt < AltiModule::MINICTP_CNT_NUMBER; cnt++) {
	m_is_alti->minictp_counters[idx].name = "item" + to_string(itm) + "_" + AltiModule::MINICTP_CNT_NAME[cnt];
	idx++;
      }
    }
    m_is_alti->minictp_counters[AltiModule::MINICTP_CNT_SIZE_TOTAL-1].name = "L1A";

    m_is_alti->busy_counters.resize(AltiBusyCounter::BUSY_NUMBER - 1); // remove INTERVAL
    for(u_int cnt=0; cnt<AltiBusyCounter::BUSY_NUMBER - 1; cnt++) {
      m_is_alti->busy_counters[cnt].name =  AltiBusyCounter::BUSY_NAME[cnt];
    }

    m_is_alti->voltages.resize(9);
    m_is_alti->temperatures.resize(2);
    
    const std::string is_name = m_IS_server + "." + m_alti_dal->UID() + ".AltiMonitoring";
    ISInfoDictionary dict(m_ipc_partition);
    dict.checkin(is_name, *m_is_alti, true);
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(m_name << " Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, ROS::IOMPlugin::m_uid, " publishToIS ", ex._name()));
  } catch (daq::is::Exception & ex) {
    ERS_LOG(m_name << " Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, ROS::IOMPlugin::m_uid, " publishToIS ", ex.what()));
  } catch (std::exception& ex) {
    ERS_LOG(m_name << " Could not publish to IS: std::exception ex.what()=" << ex.what());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, ROS::IOMPlugin::m_uid, " publishToIS ", ex.what()));
  } catch (...) {
    ERS_LOG(m_name << " Could not publish to IS. Unknown exception. ");
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, ROS::IOMPlugin::m_uid, " publishToIS ", "Unknown exception" ));
  }
  
  // configure busy mask from OKS
  m_busy_from_ctp = false;
  m_busy_from_alti = false;
  m_busy_from_nim = false;

  m_busy_uid[0] = "not_used";
  m_busy_uid[1] = "not_used";
  m_busy_uid[2] = "not_used";
 
  // first check whether the module itself is enabled
  if (!m_alti_dal->daq::core::ResourceSet::disabled(*ROS::IOMPlugin::m_partition)) {
    ERS_LOG(m_name << " ALTI Module \"" << m_alti_dal->UID() << "\" itself is enabled. Configuring busy mask.");
    
    // get the busy channels linked to "Contains"
    std::vector<const daq::core::ResourceBase*> items = m_alti_dal->get_Contains();
    std::vector<const daq::core::ResourceBase*>::iterator it = items.begin();
    
      for(;it != items.end();it++){
	//check that it is a BusyChannel
	const RODBusydal::BusyChannel* busy_channel = ROS::IOMPlugin::m_configurationDB->cast<RODBusydal::BusyChannel> (*it);
	if(busy_channel){
	  int channel_number = busy_channel->get_Id();
	  ERS_LOG(m_name << " ALTI Module: found BusyChannel \"" << busy_channel->UID() << "\" for channel=" << channel_number);
	  
	  bool resource_enabled(false);
	  
	  // first check whether the busy channel is enabled
	  if (!busy_channel->disabled(*m_partition)) {
	    ERS_LOG(m_name << " BusyChannel \"" << busy_channel->UID() << "\" is enabled. Checking corresponding BusySource.");
	    
	    const daq::core::ResourceBase* source = busy_channel->get_BusySource();
	    if (source) {
	      if (!source->disabled(*m_partition)) {
		// resource is enabled
		ERS_LOG(m_name << " ALTI module: Resource for " << busy_channel->UID() << "@BusyChannel (Id=" << channel_number << ") is enabled -> enabling channel");
		resource_enabled = true;
	      } else {
		ERS_LOG(m_name << " Resource for BusyChannel \"" << busy_channel->UID() << "\" (" << channel_number << ") is disabled -> disabling channel.");
		resource_enabled = false;
	      }
	    }
	  } else {
	    ERS_LOG(m_name << " BusyChannel \"" << busy_channel->UID() << "\" is disabled. Disabling this channel in the ALTI.");
	  }
	 
         if (channel_number == 0) {
	    m_busy_from_ctp = resource_enabled;
	    m_busy_uid[0] = busy_channel->UID();
	  } else if (channel_number == 1) {
	    m_busy_from_alti = resource_enabled;
	    m_busy_uid[1] = busy_channel->UID();
	  } else if (channel_number == 2) {
	    m_busy_from_nim = resource_enabled;
	    m_busy_uid[2] = busy_channel->UID();
	 } else {
	    ostringstream text;
	    text << m_name << " Invalid Id (" << channel_number << ") for " << busy_channel->UID() << "@BusyChannel.  Must be 0, 1, or 2.";
	    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
	 }
	}
      }
  }
  cout << m_name << " BusyChannel(0): \"" << m_busy_uid[0] << "\" - m_busy_from_ctp = " << m_busy_from_ctp << endl;
  cout << m_name << " BusyChannel(1): \"" << m_busy_uid[1] << "\" - m_busy_from_alti = " << m_busy_from_alti << endl;
  cout << m_name << " BusyChannel(2): \"" << m_busy_uid[2] << "\" - m_busy_from_nim = " << m_busy_from_nim << endl;
  

  // Master trigger
  bool i_am_in_master_trigger_rcd = false;
  bool i_am_master_trigger_module = false;
  
  const daq::core::MasterTrigger* master_trigger = ROS::IOMPlugin::m_partition->get_MasterTrigger();
  string master_trigger_rcd_uid = "";
  if(master_trigger) {
    ERS_LOG(m_name << " Found MasterTrigger object: " << master_trigger->UID());
   
    // check master trigger RCD
    const  daq::df::RCD* master_trigger_rcd = ROS::IOMPlugin::m_configurationDB->cast<daq::df::RCD>(master_trigger->get_Controller());
    if(master_trigger_rcd) {
      master_trigger_rcd_uid = master_trigger_rcd->UID();
      ERS_LOG(m_name << " Found master trigger RCD: " << master_trigger_rcd_uid);
      ERS_LOG(m_name << " Detector id of this RCD: " << (u_int)master_trigger_rcd->get_Detector()->get_LogicalId());
      
      // check if this Alti is linked to this RCD
      std::vector<const daq::core::ResourceBase*> items = master_trigger_rcd->get_Contains();
      std::vector<const daq::core::ResourceBase*>::iterator it = items.begin();
      for(;it != items.end();it++){
	//check that it is an Alti board
	const ALTIDAL::AltiBoard* alti = ROS::IOMPlugin::m_configurationDB->cast<ALTIDAL::AltiBoard> (*it);
	if(alti){
	  if(alti->UID() == ROS::IOMPlugin::m_uid) {
	    ERS_LOG(m_name << " MasterTrigger_Controller application is my RCD application: \"" << master_trigger_rcd_uid << "\"");
	    i_am_in_master_trigger_rcd = true;
	  } else {
	    ERS_LOG(m_name << " MasterTrigger RCD is \"" << master_trigger_rcd_uid << "\", this application = \"" << ROS::IOMPlugin::m_uid << "\"");
	  }
	}
      }
    }
    
    // check  master trigger module
    const ALTIDAL::AltiBoard* master_module = ROS::IOMPlugin::m_configurationDB->cast<ALTIDAL::AltiBoard>(master_trigger->get_TriggerModule());
    if(master_module) {
      if(master_module->UID() == ROS::IOMPlugin::m_uid) {
	ERS_LOG(m_name << " TriggerModule in the MasterTrigger is myself: \"" << ROS::IOMPlugin::m_uid << "\"");
	i_am_master_trigger_module = true;
      } else {
	ERS_LOG(m_name << " TriggerModule is \"" << master_module->UID() << "\", myself = \"" << ROS::IOMPlugin::m_uid << "\"");
      }
    }
  } else {
    ERS_LOG(m_name << " No MasterTrigger object found in Partition object");
  }
  
  // if has same RCD as MasterTrigger and is the MasterTrigger module: set Alti as MasterTrigger
  if (i_am_in_master_trigger_rcd && i_am_master_trigger_module) { 
    ERS_LOG("I am the MasterTrigger");
    m_is_master_trigger = true;
    ERS_LOG("Creating CommandedTrigger object for application \"" << master_trigger_rcd_uid << "\" and module \"" << ROS::IOMPlugin::m_uid );
    m_commanded_trigger = new daq::trigger::CommandedTrigger(m_ipc_partition, master_trigger_rcd_uid, this);
  }
 
  // Open ALTI only once
  ERS_LOG(m_name << " Open ALTI in slot " << slot);
  m_alti = new AltiModule(slot);
  
  // reset the phase
  int clock_phase = 0;
  if((status |= m_alti->CLKPhasePositionReadPLL(clock_phase)) != AltiModule::SUCCESS) {
    string reason = "during CLKPhasePositionReadPLL with code = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  if ((status |= m_alti->CLKPhaseResetPLL()) != AltiModule::SUCCESS) {
    string reason = "reset CLK phase with code = " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  } 
  // check the PLL phase position, and reset PLL if it is not 0
  if(clock_phase != 0) {
    ERS_LOG(m_name << " initial PLL clock phase = " << clock_phase << " : reset PLL" );
    if ((status |= m_alti->CLKResetPLL()) != AltiModule::SUCCESS) {
      string reason = "reset CLK PLL with code = " + to_string(status); 
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }     
  }

  // check the clock 
  ERS_LOG(m_name << " Check the output clock");
  bool lol, los, sticky_lol, sticky_los;
  if ((status |= m_alti->CLKStatusReadPLL(lol, los, sticky_lol, sticky_los)) != AltiModule::SUCCESS) {
    string reason = "reading CLK MMCM status with code = " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  } 
  std::printf("%s PLL  output clock: %s \n", m_name.c_str(), lol ? "ERR" : "OK");
  if(lol) {
    // set internal clock
    ERS_LOG(m_name << " Set internal clock");
    if ((status |= m_alti->CLKInputSelectWriteJitterCleaner(AltiModule::OSCILLATOR)) != AltiModule::SUCCESS) { 
      string reason = " during CLKInputSelectWriteJitterCleaner(AltiModule::OSCILLATOR) with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }    
    if ((status |= m_alti->CLKInputSelectWritePLL(AltiModule::JITTER_CLEANER)) != AltiModule::SUCCESS) { 
      string reason = " during CLKInputSelectWritePLL(AltiModule::JITTER_CLEANER) with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    } 
    // wait for PLL to lock
    this_thread::sleep_for(chrono::milliseconds(1) );
  } 
  
  // inputs equalizer
  m_alti_config->sig_config = true;
  int ctp_cable_length = m_alti_dal->get_Ctp_cable_length();
  int alti_cable_length = m_alti_dal->get_Alti_cable_length();
  const AltiModule::EQUALIZER_CONFIG ctp_eqz_config = (ctp_cable_length < 10 ? AltiModule::EQUALIZER_CONFIG::SHORT_CABLE : AltiModule::EQUALIZER_CONFIG::LONG_CABLE);
  const AltiModule::EQUALIZER_CONFIG alti_eqz_config = (alti_cable_length < 10 ? AltiModule::EQUALIZER_CONFIG::SHORT_CABLE : AltiModule::EQUALIZER_CONFIG::LONG_CABLE);
  ERS_LOG(m_name << " Configure Alti equalizer for input cables length :");
  cout << m_name << " - CTP input cable length = " << ctp_cable_length << " m" << endl;
  cout << m_name << " - ALTI input cable length = " << alti_cable_length << " m" << endl;
  m_alti_config->sig_eqz_ctp_in_mode = ctp_eqz_config;
  m_alti_config->sig_eqz_alti_in_mode = alti_eqz_config;

  // Busy   
  m_alti_config->bsy_config = true;
  m_alti_config->bsy_level = m_alti_dal->get_busy_level() == "NIM" ? AltiModule::BUSY_LEVEL::NIM : AltiModule::BUSY_LEVEL::TTL;

  // calibration requests
  const std::vector<const ALTIDAL::AltiCalibrationRequest*>& requests = m_alti_dal->get_calibration_requests();
  if(requests.size()>0) {
    ERS_LOG(m_name << " Found calibration requests: ");
    for(auto request : requests) request->print(1,true,std::cout);
  }
  else  ERS_LOG(m_name << " no calibration requests found ");
  
  //  configuration mode
  const ALTIDAL::AltiModeConfigBase* mode = m_alti_dal->get_mode();
  const ALTIDAL::AltiTiming* timing = nullptr;
  bool is_expert = false;
  ERS_LOG(m_name << " Running mode: " << mode); 
  if (mode != nullptr) {
    
    if (const ALTIDAL::AltiModeCtpSlave* ctp_slave = ROS::IOMPlugin::m_configurationDB->cast<ALTIDAL::AltiModeCtpSlave>(mode)) {
      ERS_LOG(m_name << " Found AltiModeCtpSlave");
      if(m_is_master_trigger){
	ERS_LOG(m_name << " Alti can not be MasterTrigger in mode CtpSlave");  
	ers::error(ALTI::AltiController::CannotConfigure(ERS_HERE, ROS::IOMPlugin::m_uid, "Alti can not be MasterTrigger in mode CtpSlave"));
      }
      timing = ctp_slave->get_timing();
      status |= this->configureCtpSlave(ctp_slave, requests);
      m_config_mode = "Ctp slave";
      
    } 
    else if (const ALTIDAL::AltiModeAltiSlave* alti_slave = ROS::IOMPlugin::m_configurationDB->cast<ALTIDAL::AltiModeAltiSlave>(mode)) {
      ERS_LOG(m_name << " Found AltiModeAltiSlave");
      if(m_is_master_trigger){
	ERS_LOG(m_name << " Alti can not be MasterTrigger in mode AltiSlave");  
	ers::error(ALTI::AltiController::CannotConfigure(ERS_HERE, ROS::IOMPlugin::m_uid, "Alti can not be MasterTrigger in mode AltiSlave"));
      }
      timing = alti_slave->get_timing();
      status |= this->configureAltiSlave(alti_slave, requests);
      m_config_mode = "Alti slave";
    } 
    else if (const ALTIDAL::AltiModeLemoSlave* lemo_slave = ROS::IOMPlugin::m_configurationDB->cast<ALTIDAL::AltiModeLemoSlave>(mode)) {
      ERS_LOG(m_name << " Found AltiModeLemoSlave");
      if(m_is_master_trigger){
	ERS_LOG(m_name << " Alti can not be MasterTrigger in mode LemoSlave");  
	ers::error(ALTI::AltiController::CannotConfigure(ERS_HERE, ROS::IOMPlugin::m_uid, "Alti can not be MasterTrigger in mode LemoSlave"));
      }
      timing = lemo_slave->get_timing();
      status |= this->configureLemoSlave(lemo_slave, requests);
      m_config_mode = "Lemo slave";
    } 
    else if (const ALTIDAL::AltiModeAltiMaster* alti_master = ROS::IOMPlugin::m_configurationDB->cast<ALTIDAL::AltiModeAltiMaster>(mode)) {
      ERS_LOG(m_name << " Found AltiModeAltiMaster");
      if(!m_is_master_trigger){
	ERS_LOG(m_name << " Alti has to be MasterTrigger in mode AltiMaster");  
	ers::fatal(ALTI::AltiController::CannotConfigure(ERS_HERE, ROS::IOMPlugin::m_uid, "Alti has to be MasterTrigger in mode AltiMaster"));
      }
      timing = alti_master->get_timing();
      status |= this->configureAltiMaster(alti_master, requests);
      m_config_mode = "Alti master";
    } 
    else if (const ALTIDAL::AltiModeAltiExpert* alti_expert = ROS::IOMPlugin::m_configurationDB->cast<ALTIDAL::AltiModeAltiExpert>(mode)) {
      ERS_LOG(m_name << " Found AltiModeAltiExpert");
      status |= this->configureAltiExpert(alti_expert);
      is_expert = true;
      m_config_mode = "Alti expert";
    } 
    else {   // else: no configuration
      ERS_LOG(m_name << " No Alti mode configuration provided in configuration database");
    } 
  } else {
    ERS_LOG(m_name << " No AltiModeConfigBase provided in configuration database");
  }
  
  //------------------------
  //   TTC configuration  
  //------------------------
  
  // Transmitters configuration
  ERS_LOG(m_name << " Transmitter configuration");
  const std::vector<const ALTIDAL::AltiTransmitter*> transmitters = m_alti_dal->get_transmitters(); 
  cout << m_name << " Found " << transmitters.size() << " transmitters in OKS:" << endl;
  for(u_int tx=0; tx<transmitters.size(); tx++) {
    u_int slot = transmitters[tx]->get_slot();
    cout << m_name << "  - transmitter slot " << slot 
	 << ", name: " << transmitters[tx]->get_name() 
	 << ", enabled: " << transmitters[tx]->get_enabled() 
	 << ", keep enabled: " << transmitters[tx]->get_keep_enabled()  << endl;
    m_alti_config->ttc_tx_enable[slot] = transmitters[tx]->get_enabled();
    m_alti_config->ttc_tx_delay[slot]  = transmitters[tx]->get_delay();
    // update IS transmitter name
    m_is_alti->transmitters[slot].name = transmitters[tx]->get_name();
  }
  
  // L1A source 
  ERS_LOG(m_name << " TTC L1A source configuration");
  const std::vector<const ALTIDAL::TTCL1aSource*> l1a_sources = m_alti_dal->get_TTC_L1A_source(); 
  cout << m_name << " Found " << l1a_sources.size() << " TTC L1A sources in OKS:" << endl;
  string l1a_sources_list = "";
  for(u_int source=0; source < l1a_sources.size(); source++) {
    string name = l1a_sources[source]->get_signal();
    if(l1a_sources_list != "") l1a_sources_list += ", ";
    l1a_sources_list += name;

    if(name=="L1A") {
      if(m_alti_config->sig_l1a_source != "MINICTP") m_alti_config->ttc_l1a_source[0] = true; // from external
    }
    else if(name=="TTR1") m_alti_config->ttc_l1a_source[1] = true;
    else if (name=="TTR2") m_alti_config->ttc_l1a_source[2] = true;
    else if (name=="TTR3") m_alti_config->ttc_l1a_source[3] = true;
    else if (name=="CALREQ") m_alti_config->ttc_l1a_source[4] = true;
    cout << m_name << " L1A sources: " << l1a_sources_list << endl;
    m_is_alti->ttc_l1a_source = l1a_sources_list;
  }
 
  // B-channel configuration
  const ALTIDAL::AltiBChannelConfig* Bchannel = m_alti_dal->get_Bchannel_config();
  ERS_LOG(m_name << " B-channel configuration: " << Bchannel); 
  if (Bchannel != nullptr) {    
    m_alti_config->ttc_config = true;
    
    // Trigger type word 
    m_alti_config->ttc_ttyp_delay = Bchannel->get_trigger_type_delay();
    m_alti_config->ttc_ttyp_addr = Bchannel->get_trigger_type_address();
    m_alti_config->ttc_ttyp_subaddr = Bchannel->get_trigger_type_subaddress();
    m_alti_config->ttc_ttyp_address_space = ((Bchannel->get_trigger_type_address_space() == "External")? AltiModule::TTC_ADDRESS_SPACE::EXTERNAL : AltiModule::TTC_ADDRESS_SPACE::INTERNAL);
    
    if(Bchannel->get_trigger_type_enabled()) { 
      ERS_LOG(m_name << " enable Trigger Type cycles");
      if ((status |= m_alti->ENCTriggerTypeControlWrite(m_alti_config->ttc_ttyp_addr, m_alti_config->ttc_ttyp_address_space, m_alti_config->ttc_ttyp_subaddr) != 0)) {
	ostringstream text;
	text << m_name << " Alti::ENCTriggerTypeControlWrite returned with status = " << status ;
	ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
      }
      if ((status |= m_alti->ENCTriggerTypeEnable(true) != 0)) {
	ostringstream text;
	text << m_name << " Alti::ENCTriggerTypeEnable returned with status = " << status ;
	ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
      }
    }
    else {
      ERS_LOG(m_name << " disable Trigger Type cycles");
      if ((status |= m_alti->ENCTriggerTypeEnable(false) != 0)) {
	ostringstream text;
	text << m_name << " Alti::ENCTriggerTypeEnable returned with status = " << status ;
	ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
      }  
    }
    
    // BGo front panel nim output selection
    if(Bchannel->get_BGo2_output() == "BGo2") m_alti_config->sig_fp_bgo2enable = true;
    else if(Bchannel->get_BGo2_output() == "BGo0") m_alti_config->sig_fp_bgo2enable = false;
    
    if(Bchannel->get_BGo3_output() == "BGo3") m_alti_config->sig_fp_bgo3enable = true;
    else if(Bchannel->get_BGo3_output() == "BGo1") m_alti_config->sig_fp_bgo3enable = false;
    
    // reset BGoChannel Configuration
    this->resetBGoChannelConfiguration();
    
    // get asynchronous cycles
    m_cycles_for_configure = this->getCycles(Bchannel->get_cycles_for_configure());
    m_cycles_for_connect = this->getCycles(Bchannel->get_cycles_for_connect());
    m_cycles_for_prepareForRun = this->getCycles(Bchannel->get_cycles_for_prepareForRun());   
    ERS_LOG(m_name << " dump cycles for configure");
    this->dumpCycles(m_cycles_for_configure);
    ERS_LOG(m_name << " dump cycles for connect");
    this->dumpCycles(m_cycles_for_connect);
    ERS_LOG(m_name << " dump cycles for prepare for run");
    this->dumpCycles(m_cycles_for_prepareForRun);

  } else {
    ERS_LOG(m_name << " No AltiBChannelConfig provided in configuration database");
  }
  
  // go through the list of BGo channels and overwrite the corresponding channels
  if(Bchannel->get_BGo_channels().size() == 0) ERS_LOG("no BGo channel defined in OKS");
  std::vector<const ALTIDAL::AltiBGoChannelBase*>::const_iterator it;
  for (it = Bchannel->get_BGo_channels().begin(); it != Bchannel->get_BGo_channels().end(); ++it) {
    if (const ALTIDAL::AltiBGoBCR* bcr = ROS::IOMPlugin::m_configurationDB->cast<ALTIDAL::AltiBGoBCR>(*it)) {
      ERS_LOG(m_name << " Found BCR channel: " << bcr->UID());
      this->setBGoBCR(bcr);
    } 
    else if (const ALTIDAL::AltiBGoECR* ecr = ROS::IOMPlugin::m_configurationDB->cast<ALTIDAL::AltiBGoECR>(*it)) {
      ERS_LOG(m_name << " Found ECR channel: " << ecr->UID());
      this->setBGoECR(ecr);
    } 
    else if (const ALTIDAL::AltiBGoChannel* BGoChannel = ROS::IOMPlugin::m_configurationDB->cast<ALTIDAL::AltiBGoChannel>(*it)) {
      // Read configuration parameters of the current B-channel
      ERS_LOG(m_name << " Found BGo channel: " << BGoChannel->UID());
      this->setBGoChannel(BGoChannel);
    }
  }

  // input signals synchronisation and clock phase shift
  if(!is_expert) status |= this->configureTiming(timing);
  
  // configure TTYP LUT for monitoring counters 
  m_alti_config->cnt_ttyp_lut_file = m_alti_dal->get_cnt_ttyp_lut();

  // dump configuration
  ERS_LOG(m_name << " Dump configuration:");
  m_alti_config->print();
   
  // write configuration in HW
  ERS_LOG(m_name << " Write configuration in the Hardware");
  status |= this->writeConfiguration();

  if(m_is_master_trigger) {
    ERS_LOG(m_name << " calling m_master_busy->reset()");
    m_master_busy->reset();
    m_master_busy->dump();
  }

  // configure Busy monitoring
  ERS_LOG(m_name << " configure Busy monitoring");
  status |= this->configureBusyMonitoring();
    
  // clean clock status
  ERS_LOG(m_name << " Clean clock status");
  if ((status = m_alti->CLKJitterCleanerStatusClear()) != AltiModule::SUCCESS) {
    string reason = " during CLKJitterCleanerStatusClear with code = " + to_string(status) ;
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  if ((status = m_alti->CLKStatusClearJitterCleaner()) != AltiModule::SUCCESS) {
    string reason = " during CLKStatusClearJitterCleaner with code = " + to_string(status) ;
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  if ((status = m_alti->CLKStatusClearPLL()) != AltiModule::SUCCESS) {
    string reason = " during CLKStatusClearPLL with code = " + to_string(status) ;
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  if ((status = m_alti->CLKMonitorStatusClearPLL()) != AltiModule::SUCCESS) {
    string reason = " during CLKMonitorStatusClearPLL with code = " + to_string(status) ;
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  if ((status = m_alti->CLKStickyBitsReset()) != AltiModule::SUCCESS) {
    string reason = " during CLKStickyBitsReset with code = " + to_string(status) ;
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }

  // send cycles
  ERS_LOG(m_name << " Set BGo cycles");
  status |= this->setBGoCycles(m_bgo_cycles);
  
  if(!m_is_master_trigger) {
    ERS_LOG(m_name << " m_is_master_trigger = false; Disabling constant busy since I'm not in charge of the busy");
    if((status |= m_alti->BSYConstLevelRegWrite(false)) != AltiModule::SUCCESS) { 
      string reason = "during BSYConstLevelRegWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
  }

  if (status != 0) {
    string reason = " during configure with VME error with code = " + status;
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }

  m_is_alti->is_configured = true;
  
  // start pattern generator
  if(m_use_pg && m_start_pg_transition=="subTransition") {
    ERS_LOG(m_name << " start pattern generator");
    if ((status |= m_alti->PRMModeWrite("PATTERN")) != AltiModule::SUCCESS) {
      string reason = " during PRMModeWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if ((status |= m_alti->PRMEnableWrite(true)) != AltiModule::SUCCESS) {
      string reason = " during PRMEnableWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }   
  }

  // publish once to IS
  this->publishAltiInfo();
   
  ERS_LOG(m_name << " Done");
}

//========================================================================================================================
 
void AltiController::configure(const daq::rc::TransitionCmd&) {
    
  m_name = ROS::IOMPlugin::m_uid;

  ERS_LOG(m_name << " Entered");

  int status = 0;

  // send cycles
  ERS_LOG(m_name << " Send cycles for configure");
  status |= this->sendAsyncCycles(m_cycles_for_configure);
  
  if(m_is_master_trigger) {
    m_master_busy->dump();
    
    // start pattern generator
    if(m_use_pg && m_start_pg_transition=="Configure") {
      ERS_LOG(m_name << " start pattern generator");
      if ((status |= m_alti->PRMModeWrite("PATTERN")) != AltiModule::SUCCESS) {
	string reason = " during PRMModeWrite with code = " + to_string(status);
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      }
      if ((status |= m_alti->PRMEnableWrite(true)) != AltiModule::SUCCESS) {
	string reason = " during PRMEnableWrite with code = " + to_string(status);
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      }
    }
  }
  
  if (status != 0) {
    string reason = " during configure with VME error with code = " + status;
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  
 ERS_LOG(m_name << " Done");
}

//========================================================================================================================
 
void AltiController::connect(const daq::rc::TransitionCmd&) {
  ERS_LOG(m_name << " Entered");
  
  int status = 0;
  
  // check that the clock status
  ERS_LOG(m_name << " read clock status");
  status |= this->readClockStatus();
  
  if(m_is_master_trigger) {
    ERS_LOG(m_name << " disable lumi-blocks");
    m_master_lumiblock->disableLumiBlocks();
    m_master_lumiblock->disablePeriodicLumiBlocks();
    
    ERS_LOG(m_name << " reset all busy channels");
    // note that the reset will set the trigger busy, which will be unset during prepareForRun,
    // as well as the runcontrol busy, which will be unset with the first resume command
    m_master_busy->reset();
    m_master_busy->dump();
    
   
    // setup daq::asyncmsg connection for TTC2LAN communication
    if(m_use_ttc2lan) {
      ERS_LOG(m_name << " calling m_master_ttc2lan->connect()");
      if(!m_master_ttc2lan->connect()) m_use_ttc2lan = false;
      else m_ttc2lan_connected = true;
    }
    
    m_master_busy->dump();
  }
  
  // Switch on inhibit signal associated to B-channels  
  for (u_int bgo = 0; bgo < 4; bgo++) {
    ERS_LOG(m_name << " start inhibit for B-channel " << bgo);
    if ((status |= m_alti->ENCInhibitParamsWrite(bgo, m_alti_config->ttc_bgo_inhibit_width[bgo], m_alti_config->ttc_bgo_inhibit_delay[bgo]) != AltiModule::SUCCESS)) {
      string reason = " during TTCParamsBGOInhibitWrite for B-Go " + to_string(bgo) + " with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
  }
 
  // send cycles
  ERS_LOG(m_name << " send cycles for connect");
  status |= this->sendAsyncCycles(m_cycles_for_connect);

  if (status != 0) {
    ostringstream text;
    text << m_name << " VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  } 

  // start pattern generator
  if(m_use_pg && m_start_pg_transition=="Connect") {
    ERS_LOG(m_name << " start pattern generator");
    if ((status |= m_alti->PRMModeWrite("PATTERN")) != AltiModule::SUCCESS) {
      string reason = " during PRMModeWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if ((status |= m_alti->PRMEnableWrite(true)) != AltiModule::SUCCESS) {
      string reason = " during PRMEnableWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }   
  }

  ERS_LOG(" Done");
}

//========================================================================================================================

void AltiController::prepareForRun(const daq::rc::TransitionCmd&){
  ERS_LOG(m_name << " Entered");
  
  int status = 0;

  // TTC decoder
  const ALTIDAL::AltiTTCdecoder* ttc_decoder = m_alti_dal->get_TTC_decoder(); 
  if (ttc_decoder != nullptr && ttc_decoder->get_enabled()) {  
    m_ttc_decoder_monitoring = new TtcDecoderMonitoring(*m_alti, m_uid, *ttc_decoder);
  }
  
  if(m_is_master_trigger) {
    u_int run_number = m_runParams->getUInt("runNumber");
    ERS_LOG(m_name << " retrieved run number " << run_number);
    
    // set LAr calibration: L1A generation from BGo-2
    const ALTIDAL::AltiBGo2L1A* bgo2_l1a = m_alti_dal->get_BGo2_L1A(); 
  
    ISInfoDictionary dict(m_ipc_partition);
    RunParams is_run_params;
    try {
      dict.getValue("RunParams.RunParams", is_run_params);
    }
    catch (CORBA::SystemException& ex) {
      ERS_LOG(m_uid << " Could not read from IS: CORBA::SystemException ex._name()=" << ex._name());
      ers::error(ALTI::AltiController::ReadFromIS(ERS_HERE, m_uid, " readFromIS ", ex._name()));
    } catch (daq::is::Exception & ex) {
      ERS_LOG(m_uid << " Could not read from IS: daq::is::Exception ex.what()=" << ex.what());
      ers::error(ALTI::AltiController::ReadFromIS(ERS_HERE, m_uid, " readFromIS ", ex.what()));
    }
    catch (std::exception& ex) {
      ERS_LOG(m_uid << " Could not read from IS: std::exception ex.what()=" << ex.what());
      ers::error(ALTI::AltiController::ReadFromIS(ERS_HERE, m_uid, " readFromIS ", ex.what()));
    } 
    catch (...) {
      ERS_LOG(m_uid << " Could not read from IS. Unknown exception. ");
      ers::error(ALTI::AltiController::ReadFromIS(ERS_HERE, m_uid, " readFromIS ", "Unknown exception" ));
    }
    
    if (bgo2_l1a != nullptr) {
      if((bgo2_l1a->get_run_type() == is_run_params.run_type) || (bgo2_l1a->get_run_type() == "All" )) { 
	ERS_LOG(m_name << " Run type = " << is_run_params.run_type << ": enable generation of L1A from BGo-2");
	m_alti_config->sig_l1a_source = "CALIBSIGNAL";
	if ((status = m_alti->SIGOutputSelectL1aWrite(m_alti_config->sig_l1a_source)) !=  AltiModule::SUCCESS) {
	  string reason = "during SIGOutputSelectL1aWrite with code = " + to_string(status);
	  ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
	}
	
	m_alti_config->bgo2_l1a_delay = bgo2_l1a->get_delay();
	if ((status = m_alti->SIGBGo2L1aDelayWrite(m_alti_config->bgo2_l1a_delay)) !=  AltiModule::SUCCESS) {
	  string reason = "during SIGBGo2L1aDelayWrite with code = " + to_string(status);
	  ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
	}
	
	// disable busy mask for L1A in case busy occurs between BGo-2 and L1A
	m_alti_config->bsy_l1a_masked = false;
	if ((status = m_alti->BSYMaskingL1aEnableWrite(m_alti_config->bsy_l1a_masked)) !=  AltiModule::SUCCESS) {
	  string reason = "during BSYMaskingL1aEnableWrite( with code = " + to_string(status);
	  ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
	}
      }
    }
    
    // reset all busy channels
    // note that the reset will set the trigger busy, which will be unset at the end of prepareForRun,
    // as well as the runcontrol busy, which will be unset with the first resume command
    ERS_LOG(m_name << " busy status before reset");
    m_master_busy->dump();
    ERS_LOG(m_name << " calling m_master_busy->reset()");
    m_master_busy->reset();
    ERS_LOG(m_name << " busy status after reset");
    m_master_busy->dump();
    
    // reset ECR
    ERS_LOG(m_name << " calling m_master_ecr->reset()");
    m_master_ecr->reset();
    // send 1 ECR if periodic ECR is disabled only for LAr   
    if(!m_ecr_periodic && m_ecr_word == 0x800000) {
      ERS_LOG(m_name << " generate 1 ECR");
      if((status |= m_alti->SIGOutputSelectBgoWrite(1, "MINICTP")) != AltiModule::SUCCESS) {
	string reason = "during SIGOutputSelectBgoWrite with code = " + to_string(status);
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      }  
      // set ECR from VME
      AltiModule::ECR_GENERATION ecr;
      if((status |= m_alti->ECRGenerationRead(ecr)) != AltiModule::SUCCESS) {
	string reason = "reading ECR generation = " + to_string(status);
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
      }
      ecr.type = AltiModule::ECR_TYPE::ECR_VME; 
      if((status |= m_alti->ECRGenerationWrite(ecr)) != AltiModule::SUCCESS) {
	string reason = "writing ECR generation = " + to_string(status);
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
      }
      // wait for ECR to be ready 
      if((status |= m_alti->ECRReadyWait()) != AltiModule::SUCCESS) {
	string reason = "waiting for ECR to be ready again = " + to_string(status);
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
      }
      // sleep for 5s as requested by LAr
       std::this_thread::sleep_for (std::chrono::seconds(5));       
      // generate 1 ECR 
      if((status |= m_alti->ECRGenerate()) != AltiModule::SUCCESS) {
	string reason = "generating ECR = " + to_string(status);
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
      }
    }
    
    // reset and enable LB
    try {
      ERS_LOG(m_name << " calling m_masterlumiblock->reset("<< run_number << ")");
      m_master_lumiblock->reset(run_number);
    } catch (ALTI::AltiController::FailedFirstLumiBlock& ex) {
      ers::error(ALTI::AltiController::FailedFirstLumiBlock(ERS_HERE, m_uid, "couldn't retrieve reasonable LumiBlock information for LBN=0. ", ex.what())); 
    }
    if(m_lumiblock_period>0) {
      m_master_lumiblock->enableLumiBlocks();
      m_master_lumiblock->enablePeriodicLumiBlocks();
    }

    // start TTC2LAN
    if (m_use_ttc2lan) {
      ERS_LOG(m_name << " calling m_master_ttc2lan->prepareForRum()");
      m_master_ttc2lan->prepareForRun();
    }
    
    // unset Trigger busy
    ERS_LOG(m_name << " unsetting trigger busy");
    m_master_busy->unsetTriggerBusy();
    // RunControl busy will be reset with the first resume
    ERS_LOG(m_name << " busy status at completion of prepareForRun");
    m_master_busy->dump();    
  }

  // send cycles
  ERS_LOG(m_name << " send cycles for prepare for run");
  status |= this->sendAsyncCycles(m_cycles_for_prepareForRun);
  
  // start and reset all counters 
  if (m_do_monitoring) {
    ERS_LOG(m_name << " start and reset all counters ");
    // enable counters
    if((status |= m_alti->CNTEnableWrite(true)) != AltiModule::SUCCESS) {
      string reason = "during CNTEnableWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if((status |= m_alti->CNTEnableWait(true)) != AltiModule::SUCCESS) {
      string reason = "during CNTEnableWait with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    // set counters to edge selection
    if((status |= m_alti->CNTEdgeDetectionWrite(true)) != AltiModule::SUCCESS) {
      string reason = "during CNTEdgeDetectionWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    // enable all BCIDs
    std::vector<bool> bcids(AltiModule::BCID_NUMBER, true);
    if((status |= m_alti->CNTBcidMaskWrite(bcids)) != AltiModule::SUCCESS) {
      string reason = "during CNTBcidMaskWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    // reset all counters
    if((status |= m_alti->CNTClearWrite()) != AltiModule::SUCCESS) {
      string reason = "during CNTClearWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if((status |= m_alti->CNTClearWait()) != AltiModule::SUCCESS) {
      string reason = "during CNTClearWait with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if((status |= m_alti->CNTOrbitReset()) != AltiModule::SUCCESS) {
      string reason = "during CNTOrbitReset with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }

    ERS_LOG(m_name << " read counters after reset");
    // read counters and print values in log file
    status |= readCounters(true);
    
     ERS_LOG(m_name << " read Busy counters after reset");
     // read Busy counters 
     status |= readBusyMonitoring();

     // mini-CTP counters
     if(m_do_minictp) { 
       // enable counters
       if((status |= m_alti->CTPCntEnableWrite(true)) != AltiModule::SUCCESS) {
	 string reason = "during CTPCntEnableWrite with code = " + to_string(status);
	 ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
       }
       if((status |= m_alti->CTPCntEnableWait(true)) != AltiModule::SUCCESS) {
	 string reason = "during CTPCntEnableWait with code = " + to_string(status);
	 ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
       }
       // enable all BCIDs
       std::vector<bool> bcids(AltiModule::BCID_NUMBER, true);
       if((status |= m_alti->CTPCntBcidMaskWrite(bcids)) != AltiModule::SUCCESS) {
	 string reason = "during CTPCntBcidMaskWrite with code = " + to_string(status);
	 ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
       }
       // reset all counters
       if((status |= m_alti->CTPCntClearWrite()) != AltiModule::SUCCESS) {
	 string reason = "during CTPCntClearWrite with code = " + to_string(status);
	 ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
       }
       if((status |= m_alti->CTPCntClearWait()) != AltiModule::SUCCESS) {
	 string reason = "during CTPCntClearWait with code = " + to_string(status);
	 ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
       }
       status |= readMinictpCounters(true);
     }
  }
 
  // start pattern generator
  if(m_use_pg && m_start_pg_transition=="PrepareForRun") {
    ERS_LOG(m_name << " start pattern generator");
    if ((status |= m_alti->PRMModeWrite("PATTERN")) != AltiModule::SUCCESS) {
      string reason = " during PRMModeWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if ((status |= m_alti->PRMEnableWrite(true)) != AltiModule::SUCCESS) {
      string reason = " during PRMEnableWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }   
  }

  if (status != 0) {
    ostringstream text;
    text << m_name << " VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  } 

  ERS_LOG(m_name << " Done");
}

//========================================================================================================================

void AltiController::stopROIB(const daq::rc::TransitionCmd&){
  ERS_LOG(m_name << " Entered");
  
  int status = 0;
  
  // stop pattern generator
  if(m_use_pg && !m_keep_pg_running) {
     ERS_LOG(m_name << " stop pattern generator");
     if ((status |= m_alti->PRMEnableWrite(false)) != AltiModule::SUCCESS) {
       string reason = " during PRMEnableWrite with code = " + to_string(status);
       ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
     }   
  }
  
  if (m_is_master_trigger) {
    ERS_LOG(m_name << " calling m_master_lumiblock->disablePeriodicLumiBlocks()");
    m_master_lumiblock->disablePeriodicLumiBlocks();
  }
  
  if (status != 0) {
    ostringstream text;
    text << m_name << " VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  } 
  
  ERS_LOG(m_name << " Done");
}

//========================================================================================================================

void AltiController::stopRecording(const daq::rc::TransitionCmd&){
  ERS_LOG(m_name << " Entered");
 
  if (m_use_ttc2lan && m_master_ttc2lan != nullptr) {
    ERS_LOG(m_name << " calling m_master_ttc2lan->stopRecording()");
    m_master_ttc2lan->stopRecording();
    }

  ERS_LOG(m_name << " Done");
}

//========================================================================================================================

void AltiController::stopArchiving(const daq::rc::TransitionCmd&){
  ERS_LOG(m_name << " Entered");
  
  if(m_is_master_trigger) {
    ERS_LOG(m_name << " calling m_master_lumiblock->disableLumiBlocks()");
    m_master_lumiblock->disableLumiBlocks();
    ERS_LOG(m_name << " calling m_master_lumiblock->disablePeriodicLumiBlock()");
    m_master_lumiblock->disablePeriodicLumiBlocks();
    
    // at this point it is wise to reset the busy
    ERS_LOG(m_name << " calling m_master_busy->reset()");
    m_master_busy->reset();
    m_master_busy->dump();
  }

  ERS_LOG(m_name << " Done");
}

//========================================================================================================================

void AltiController::disconnect(const daq::rc::TransitionCmd&){
  ERS_LOG(m_name << " Entered");
  
 

  if (m_is_master_trigger) {
    ERS_LOG(m_name << " set Trigger and RunControl busy");
    m_master_busy->setTriggerBusy();
    m_master_busy->setRunControlBusy(1); // will be unset with the first resume command
    
    if ((m_use_ttc2lan || m_ttc2lan_connected) && m_master_ttc2lan != nullptr) {
      ERS_LOG(m_name << " calling m_master_ttc2lan->disconnect()");
      m_master_ttc2lan->disconnect();
    }
  }
  
  ERS_LOG(m_name << " Done");
}

//========================================================================================================================

void AltiController::unconfigure(const daq::rc::TransitionCmd&){
  ERS_LOG(m_name << " Entered"); 
  int status = 0;
  
  // disable monitoring
  m_do_monitoring = false;

  if (m_is_master_trigger) {
    ERS_LOG(m_name << " calling m_master_busy->reset()");
    m_master_busy->reset();
    m_master_busy->dump();
  }
  
  // disable the transmitters 
  ERS_LOG(m_name << " disabling the transmitters");
  const std::vector<const ALTIDAL::AltiTransmitter*> transmitters = m_alti_dal->get_transmitters(); 
  for(u_int tx=0; tx<transmitters.size(); tx++) {
    u_int slot = transmitters[tx]->get_slot();
    bool keep_enabled = transmitters[tx]->get_keep_enabled();
    if(!keep_enabled) {
      ERS_LOG(m_name << " disabling the equaliser in slot " << slot);
      if((status |= m_alti->ENCTransmitterEnableWrite(slot, false)) != AltiModule::SUCCESS) {
	string reason = "during ENCTransmitterEnableWrite for slot " + to_string(slot) + " with code = " + to_string(status);
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      }
    }
  }
  
  // disable the equalizers
  if(!m_alti_dal->get_CTP_EQZ_enabled()) {
    ERS_LOG(m_name << " disabling the equaliser for CTP_IN");
    if((status |= m_alti->SIGEqualizersEnableWrite(AltiModule::SIGNAL_SOURCE::CTP_IN, false)) != AltiModule::SUCCESS) {
      string reason = "during SIGEqualizersEnableWrite for CTP_IN with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
  }
  if(!m_alti_dal->get_ALTI_EQZ_enabled()) {
    ERS_LOG(m_name << " disabling the equaliser for ALTI_IN");
    if((status |= m_alti->SIGEqualizersEnableWrite(AltiModule::SIGNAL_SOURCE::ALTI_IN, false)) != AltiModule::SUCCESS) {
      string reason = "during SIGEqualizersEnableWrite for ALTI_IN with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
  }

  // re-configure the ALTI according to OKS configuration
  string config_file = m_alti_dal->get_config_shutdown();
  m_alti_config->read(config_file);
  if(config_file != "") {
    // write configuration in HW
    if((status |= m_alti->AltiConfigWrite(*m_alti_config, cout)) !=  AltiModule::SUCCESS) {
      string reason = "during configuration with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }  
  }

  // cleanup IS
  if(m_do_monitoring) {
    try{ 
      ERS_LOG(m_name << " remove IS publication");
      const std::string is_name = m_IS_server + "." + m_alti_dal->UID() + ".AltiMonitoring";
      ISInfoDictionary dict(m_ipc_partition);
      dict.remove(is_name);
    } catch (CORBA::SystemException& ex) {
      ERS_LOG(m_name << " Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());
      ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, ROS::IOMPlugin::m_uid, " publishToIS ", ex._name()));
    } catch (daq::is::Exception & ex) {
      ERS_LOG(m_name << " Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
      ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, ROS::IOMPlugin::m_uid, " publishToIS ", ex.what()));
    } catch (std::exception& ex) {
      ERS_LOG(m_name << " Could not publish to IS: std::exception ex.what()=" << ex.what());
      ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, ROS::IOMPlugin::m_uid, " publishToIS ", ex.what()));
    } catch (...) {
      ERS_LOG(m_name << " Could not publish to IS. Unknown exception. ");
      ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, ROS::IOMPlugin::m_uid, " publishToIS ", "Unknown exception" ));
    }   
  }

  if (status != 0) {
    ostringstream text;
    text << m_name << " VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  } 
  
  ERS_LOG(m_name << " Done");
}

//========================================================================================================================

// publish counters to IS
void AltiController::publish(){
  //ERS_LOG(m_name << " Entered");
  int status = 0;
  
  // publish counter value
  if(m_do_monitoring) {
    // read counters and publish values in IS (not in log)
    status |= readCounters(false);
    // read Busy counters 
    status |= readBusyMonitoring();
    // mini-CTP counters
    if(m_do_minictp) status |= readMinictpCounters(false);
    this->publishAltiInfo();
  }
  
  if(m_is_master_trigger) { 
    //publish BUSY and ECR
    m_master_busy->publishToIS();
    m_master_ecr->publishToIS();
  }

  if (status != 0) {
    ostringstream text;
    text << m_name << " VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  } 

  //ERS_LOG(m_name << " Done");
   }

//========================================================================================================================

void AltiController::publishFullStats(){
  ERS_LOG(m_name << " Publish Full Statistics to IS");

  int status = 0;

  // get configuration from HW and publish to IS 
  if(m_do_monitoring) {
    // read the ALTI configuration once
    if(m_do_status_monitoring) {
      m_is_alti->mode = m_config_mode;
      LVL1::AltiConfiguration config;
      if((status |= m_alti->AltiConfigRead(config)) != AltiModule::SUCCESS) {
	string reason = "during AltiConfigRead with code = " + to_string(status);
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      }
      //config.print();
      
      // clear fifo to avoid data corruption
      if ((status |= m_alti->BSYFifoReset()) != AltiModule::SUCCESS) {
	string reason = "VME error during BSYFifoReset(): " + to_string(status);
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      }
      
      // clock
      m_is_alti->clock_selection = (config.clk_jc_select == AltiModule::CLK_JC_TYPE::OSCILLATOR)? "internal" : "external";
      int clock_phase = 0;
      if((status |= m_alti->CLKPhasePositionReadPLL(clock_phase)) != AltiModule::SUCCESS) {
	string reason = "during CLKPhasePositionReadPLL with code = " + to_string(status);
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      }
      m_is_alti->clock_phase = clock_phase*0.015;
      
      // input / output signals 
      m_is_alti->equalizer_config_ctp = config.sig_eqz_ctp_in_enable ? AltiModule::EQUALIZER_CONFIG_NAME[config.sig_eqz_ctp_in_mode] : "DISABLED";
      m_is_alti->equalizer_config_alti = config.sig_eqz_alti_in_enable ? AltiModule::EQUALIZER_CONFIG_NAME[config.sig_eqz_alti_in_mode] : "DISABLED";
      
      m_is_alti->input_clock_phase_sync = config.sig_io_sync_phase[0];
      m_is_alti->input_shaping = config.sig_io_shaping[0];
      m_is_alti->bgo2_output = (config.sig_fp_bgo2enable)? "BGo-2" : "BGo-0";
      m_is_alti->bgo3_output = (config.sig_fp_bgo3enable)? "BGo-3" : "BGo-1";
      
      for(u_int sig=0; sig<AltiModule::SIGNAL_NUMBER_ROUTED; sig++) {
	m_is_alti->signals_switch[sig].to_ctp  = AltiModule::SIGNAL_SOURCE_NAME[config.sig_swx[sig][AltiModule::SIGNAL_DESTINATION::CTP_OUT]];
	m_is_alti->signals_switch[sig].to_alti = AltiModule::SIGNAL_SOURCE_NAME[config.sig_swx[sig][AltiModule::SIGNAL_DESTINATION::ALTI_OUT]];
	m_is_alti->signals_switch[sig].to_nim  = AltiModule::SIGNAL_SOURCE_NAME[config.sig_swx[sig][AltiModule::SIGNAL_DESTINATION::NIM_OUT]];
	m_is_alti->signals_switch[sig].to_fpga = AltiModule::SIGNAL_SOURCE_NAME[config.sig_swx[sig][AltiModule::SIGNAL_DESTINATION::TO_FPGA]];
      }
      
      // signal sources
      m_is_alti->orb_source = config.sig_orb_source;
      m_is_alti->l1a_source = config.sig_l1a_source;
      m_is_alti->ttr_source = "";
      for(u_int ttr = 0; ttr < 3; ttr++) {
	m_is_alti->ttr_source += config.sig_ttr_source[ttr];
	if(ttr < 2) m_is_alti->ttr_source += " , ";
      }
      m_is_alti->bgo_source = "";
      for(u_int bgo = 0; bgo < 4; bgo++) {
	m_is_alti->bgo_source += config.sig_bgo_source[bgo];
	if(bgo < 3) m_is_alti->bgo_source += " , ";
      }
      m_is_alti->ttyp_source = config.sig_ttyp_source;
      
      // busy
      m_is_alti->busy_local_source = "";
      for(u_int src=0; src<AltiModule::BUSY_INPUT_NUMBER; src++) {
	if(config.bsy_in_select[src]) m_is_alti->busy_local_source +=  AltiModule::BUSY_INPUT_NAME[src] + ", ";
      }
      m_is_alti->busy_to_ctp  = AltiModule::BUSY_SOURCE_NAME[config.bsy_out_select[AltiModule::BUSY_OUTPUT::BUSY_TO_CTP]];
      m_is_alti->busy_to_alti = AltiModule::BUSY_SOURCE_NAME[config.bsy_out_select[AltiModule::BUSY_OUTPUT::BUSY_TO_ALTI]];
      m_is_alti->busy_vme = (config.bsy_data_vme)? "ACTIVE" : "INACTIVE";
      m_is_alti->busy_level = AltiModule::BUSY_LEVEL_NAME[config.bsy_level];
      
      // calibration request
      m_is_alti->cal_req_local_source = ""; 
      m_is_alti->cal_req_to_ctp = "";
      m_is_alti->cal_req_to_alti = "";
      for(u_int crq=0; crq<3; crq++) {
	m_is_alti->cal_req_local_source +=  AltiModule::CALREQ_INPUT_NAME[config.crq_in_select[crq]];
	m_is_alti->cal_req_to_ctp  += AltiModule::CALREQ_SOURCE_NAME[config.crq_out_select[AltiModule::CALREQ_OUTPUT::CALREQ_TO_CTP][crq]];
	m_is_alti->cal_req_to_alti += AltiModule::CALREQ_SOURCE_NAME[config.crq_out_select[AltiModule::CALREQ_OUTPUT::CALREQ_TO_ALTI][crq]];    
	if(crq<2) {
	  m_is_alti->cal_req_local_source += ", ";
	  m_is_alti->cal_req_to_ctp  += ", ";
	  m_is_alti->cal_req_to_alti += ", ";
	}
      }
      
      // pattern generator
      m_is_alti->pg_enabled = config.mem_pg_enable;
      m_is_alti->pg_repeat = (config.mem_pg_repeat)? "REPEATED" : "ONESHOT";
      m_is_alti->pg_start = config.mem_pg_start_addr;
      m_is_alti->pg_stop = config.mem_pg_stop_addr;
      m_is_alti->pg_file = config.mem_pg_file;
      
      // TTC signals
      m_is_alti->ttyp_delay = config.ttc_ttyp_delay;
      m_is_alti->ttyp_address = config.ttc_ttyp_addr;
      m_is_alti->ttyp_subaddress = config.ttc_ttyp_subaddr;
      m_is_alti->ttyp_address_space = AltiModule::TTC_ADDRESS_SPACE_NAME[config.ttc_ttyp_address_space];
      
      for(u_int tx=0; tx<AltiModule::TRANSMITTER_NUMBER; tx++) {
	m_is_alti->transmitters[tx].enabled = config.ttc_tx_enable[tx];
	m_is_alti->transmitters[tx].delay   = config.ttc_tx_delay[tx];
      }
      
      for(u_int bgo=0; bgo<4; bgo++) {
	m_is_alti->bgos[bgo].inhibit_delay = config.ttc_bgo_inhibit_delay[bgo];
	m_is_alti->bgos[bgo].inhibit_width = config.ttc_bgo_inhibit_width[bgo];
	m_is_alti->bgos[bgo].mode          = AltiModule::TTC_BGO_MODE_NAME[config.ttc_bgo_mode[bgo]];
	m_is_alti->bgos[bgo].busy_gate     = config.bsy_bgo_masked[bgo];
	m_is_alti->bgos[bgo].retransmit    = config.ttc_fifo_retransmit[bgo];
      }
      
      // ECR
      if (m_ecr_periodic) {
	AltiModule::ECR_GENERATION ecr;
	if((status |= m_alti->ECRGenerationRead(ecr)) != AltiModule::SUCCESS) {
	  string reason = "during ECRGenerationRead with code = " + to_string(status);
	  ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
	}
	
	EcrConfigIS2 ecr_is;
	ecr_is.period = (ecr.frequency * 25.) / 1000000.;
	ecr_is.pre_deadtime = (ecr.busy_before * 25.) / 1000000.;
	ecr_is.post_deadtime = (ecr.busy_after * 25.) / 1000000.;
	ecr_is.length = ecr.length;
	ecr_is.is_started = (ecr.type == AltiModule::ECR_TYPE::ECR_INTERNAL)? "started" : "stopped";
	m_is_alti->ecr_config = ecr_is;
      }
      m_do_status_monitoring = false;
    }

    // HW voltages 
    std::vector<float> voltages(9), nominal(9);
    if((status |= m_alti->MONVoltageReadLTC2991(voltages, nominal)) != AltiModule::SUCCESS) {
      string reason = "during MONVoltageReadLTC2991 with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
  
    for(u_int v=0; v<9; v++) {
      float difference = (voltages[v]/nominal[v] - 1)*100;
      string name = "";
      if(v<8) name = "V" + to_string(v);
      else    name = "VCC";
      m_is_alti->voltages[v].name = name;
      m_is_alti->voltages[v].nominal = nominal[v];
      m_is_alti->voltages[v].measured = voltages[v];
      m_is_alti->voltages[v].difference = difference;
    }
   
    // HW temperature
    float internal_temp;
    if((status |= m_alti->MONInternalTempReadLTC2991(internal_temp)) != AltiModule::SUCCESS) {
      string reason = "during MONInternalTempReadLTC2991 with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }     
    float local_temp;
    if((status |= m_alti->MONLocalTempRead(local_temp)) != AltiModule::SUCCESS) {
      string reason = "during MONLocalTempReadLTC2991 with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }     
    m_is_alti->temperatures[0].name = "LTC2991 internal";
    m_is_alti->temperatures[0].value = internal_temp;
    m_is_alti->temperatures[1].name = "MAX1617/6695 local";
    m_is_alti->temperatures[1].value = local_temp;


    this->publishAltiInfo();
  }
  
  if (status != 0) {
    ostringstream text;
    text << m_name << " VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  } 

  //ERS_LOG(m_name << " Done");
}

//========================================================================================================================

void AltiController::publishAltiInfo() {
  try{ 
    const std::string is_name = m_IS_server + "." + m_alti_dal->UID() + ".AltiMonitoring";
    ISInfoDictionary dict(m_ipc_partition);
    dict.checkin(is_name, *m_is_alti, true);
   } catch (CORBA::SystemException& ex) {
    ERS_LOG(m_name << " Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, ROS::IOMPlugin::m_uid, " publishToIS ", ex._name()));
  } catch (daq::is::Exception & ex) {
    ERS_LOG(m_name << " Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, ROS::IOMPlugin::m_uid, " publishToIS ", ex.what()));
  } catch (std::exception& ex) {
    ERS_LOG(m_name << " Could not publish to IS: std::exception ex.what()=" << ex.what());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, ROS::IOMPlugin::m_uid, " publishToIS ", ex.what()));
  } catch (...) {
    ERS_LOG(m_name << " Could not publish to IS. Unknown exception. ");
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, ROS::IOMPlugin::m_uid, " publishToIS ", "Unknown exception" ));
  }
}

//========================================================================================================================

void AltiController::clearInfo() {
  ERS_LOG(m_name << " Entered");
  
  ERS_LOG(m_name << " Done");
}

//========================================================================================================================

void AltiController::resynch(const daq::rc::ResynchCmd& cmd) {
  ERS_LOG(m_name << " Entered");
  
  int status = 0;

  std::cout << "ECR word = 0x"  << std::hex << m_ecr_word << std::endl;
  // send ECR commands only for LAr
  if(m_ecr_word & 0x800000) {
    
    std::this_thread::sleep_for (std::chrono::seconds(1));
    
    ISInfoDictionary dict(m_ipc_partition);
    ECR is_ecr;
    try {
      dict.getValue("RunParams.ECR", is_ecr);
    }
    catch (CORBA::SystemException& ex) {
      ERS_LOG(m_uid << " Could not read from IS: CORBA::SystemException ex._name()=" << ex._name());
      ers::error(ALTI::AltiController::ReadFromIS(ERS_HERE, m_uid, " readFromIS ", ex._name()));
    } catch (daq::is::Exception & ex) {
      ERS_LOG(m_uid << " Could not read from IS: daq::is::Exception ex.what()=" << ex.what());
      ers::error(ALTI::AltiController::ReadFromIS(ERS_HERE, m_uid, " readFromIS ", ex.what()));
    }
    catch (std::exception& ex) {
      ERS_LOG(m_uid << " Could not read from IS: std::exception ex.what()=" << ex.what());
      ers::error(ALTI::AltiController::ReadFromIS(ERS_HERE, m_uid, " readFromIS ", ex.what()));
    } catch (...) {
      ERS_LOG(m_uid << " Could not read from IS. Unknown exception. ");
      ers::error(ALTI::AltiController::ReadFromIS(ERS_HERE, m_uid, " readFromIS ", "Unknown exception" ));
    }
 
    u_int ecr_counter = is_ecr.ECRCounter;
    ERS_LOG(m_name << " send " << ecr_counter << " ECR commands with data = 0x" << std::hex << m_ecr_word);
    std::vector<u_int> v_ecr;
    for(u_int i = 0; i < (u_int) ecr_counter; i++) v_ecr.push_back(m_ecr_word);  
    status |= this->sendAsyncCycles(v_ecr);
  }

  ERS_LOG(m_name << " Done");
}

//========================================================================================================================

void AltiController::user(const daq::rc::UserCmd& usrCmd) {
  ERS_LOG(m_name << " Entered");
  ERS_LOG(m_name << " Executing command " << usrCmd.toString() << " in state " << usrCmd.currentFSMState());

  int status = 0;

  // User command name and arguments
  const std::string& command_name = usrCmd.commandName();
  const auto& params = usrCmd.commandParameters();

  std::string argument;
  for(const auto& p : params) {
    argument += p + " ";
  }

  if (command_name == "StartPatternGeneratorIfNotBusy") {
    
    // check Busy
    while(m_master_busy->isBusy()) {
      this_thread::sleep_for(chrono::milliseconds(100) );
    } 
    
    ERS_LOG(m_name << " start pattern generator");
    if ((status |= m_alti->PRMModeWrite("PATTERN")) != AltiModule::SUCCESS) {
      string reason = " during PRMModeWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if ((status |= m_alti->PRMEnableWrite(true)) != AltiModule::SUCCESS) {
      string reason = " during PRMEnableWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }   
  }

  else if (command_name == "StartPatternGenerator") {
    bool enabled = false;
    if ((status |= m_alti->PRMModeWrite("PATTERN")) != AltiModule::SUCCESS) {
      string reason = " during PRMModeWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if ((status |= m_alti->PRMEnableRead(enabled)) != AltiModule::SUCCESS) {
      string reason = " during PRMEnableRead with code = " + to_string(status);
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if(enabled) {
      ERS_LOG(m_name << " pattern generator already enabled, stop it");
      if ((status |= m_alti->PRMEnableWrite(false)) != AltiModule::SUCCESS) {
	string reason = " during PRMEnableWrite with code = " + to_string(status);
	ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      }
    }
    ERS_LOG(m_name << " start pattern generator");
    if ((status |= m_alti->PRMEnableWrite(true)) != AltiModule::SUCCESS) {
      string reason = " during PRMEnableWrite with code = " + to_string(status);
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
  }

  else if (command_name == "StopPatternGenerator") {
    bool enabled = false;
    if ((status |= m_alti->PRMModeWrite("PATTERN")) != AltiModule::SUCCESS) {
      string reason = " during PRMModeWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if ((status |= m_alti->PRMEnableRead(enabled)) != AltiModule::SUCCESS) {
      string reason = " during PRMEnableRead with code = " + to_string(status);
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if(enabled) {
      ERS_LOG(m_name << " stop pattern generator");
      if ((status |= m_alti->PRMEnableWrite(false)) != AltiModule::SUCCESS) {
	string reason = " during PRMEnableWrite with code = " + to_string(status);
	ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      }
      else ERS_LOG(m_name << " pattern generator already stopped, do nothing");
    }
  }

  else if (command_name == "RunPatternGeneratorOneShot") {
    bool enabled = false;
    if ((status |= m_alti->PRMModeWrite("PATTERN")) != AltiModule::SUCCESS) {
      string reason = " during PRMModeWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if ((status |= m_alti->PRMEnableRead(enabled)) != AltiModule::SUCCESS) {
      string reason = " during PRMEnableRead with code = " + to_string(status);
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if(enabled) {
      ERS_LOG(m_name << " pattern generator already enabled, stop it");
      if ((status |= m_alti->PRMEnableWrite(false)) != AltiModule::SUCCESS) {
	string reason = " during PRMEnableWrite with code = " + to_string(status);
	ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      }
    }
    ERS_LOG(m_name << " set pattern generator in One Shot mode");
    if ((status |= m_alti->PRMRepeatWrite(false)) != AltiModule::SUCCESS) {
      string reason = " writing pattern generartor repeat mode";
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));		 
    }	
    ERS_LOG(m_name << " start pattern generator");
    if ((status |= m_alti->PRMEnableWrite(true)) != AltiModule::SUCCESS) {
      string reason = " during PRMEnableWrite with code = " + to_string(status);
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    ERS_LOG(m_name << " wait for the end of the pattern");
    bool mem_full;
    string pg_status;
    while (pg_status != "DONE") {
      if ((status |= m_alti->PRMStatusRead(mem_full, pg_status)) != AltiModule::SUCCESS) {
	string reason = " during PRMStatusRead with code = " + to_string(status);
	ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      }
      this_thread::sleep_for(chrono::milliseconds(10) );
    }
  }

  else if (command_name == "SetPatternGeneratorRepeat") {
    if(params.size() != 1) {
      string reason = " should have 1 parameter, " + to_string(params.size()) + " received";
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    
    bool repeat = stoi(params[0]);
    if ((status |= m_alti->PRMRepeatWrite(repeat)) != AltiModule::SUCCESS) {
      string reason = " writing pattern generartor repeat mode";
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));		 
    }		 
    else {
      ERS_LOG(m_name << " set pattern generator to " << (repeat ? "repeat" : "one-shot") << ")");
    }
  }

  else if (command_name == "SetPatternGeneratorMemory") {
    if(params.size() != 1) {
      string reason = " should have 1 parameter, " + to_string(params.size()) + " received";
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    std::string pg_file = params[0];

    bool enabled = false;
    if ((status |= m_alti->PRMModeWrite("PATTERN")) != AltiModule::SUCCESS) {
      string reason = " during PRMModeWrite with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if ((status |= m_alti->PRMEnableRead(enabled)) != AltiModule::SUCCESS) {
      string reason = " during PRMEnableRead with code = " + to_string(status);
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if(enabled) {
      ERS_LOG(m_name << " pattern generator enabled, stop it");
      if ((status |= m_alti->PRMEnableWrite(false)) != AltiModule::SUCCESS) {
	string reason = " during PRMEnableWrite with code = " + to_string(status);
	ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      }
    }
    ERS_LOG(m_name << " set pattern generator memory from file " << pg_file);
    if(!std::ifstream(pg_file)) {
      ERS_REPORT_IMPL( ers::error, ers::Message, m_name << + " pattern generation file " + pg_file + " doesn't exist", );
    }
    
    std::ifstream inf(pg_file.c_str(), std::ifstream::in);
    if(!inf) {
      string reason = " cannot open input file " + pg_file;
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }

    CMEMSegment* segment = new RCD::CMEMSegment("AltiController", sizeof(u_int)*AltiModule::MAX_MEMORY, true);
    if ((status |= m_alti->PRMWriteFile(segment, inf)) != AltiModule::SUCCESS) {
      string reason = " during PRMWriteFile with code = " + to_string(status);
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    // delete CMEM segment
    if(segment) delete segment;
  }

  else if (command_name == "SendAsyncShortCommand") {
    if(params.size() != 1) {
      string reason = " should have 1 parameter, " + to_string(params.size()) + " received";
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    
    unsigned int data = stoi(params[0], 0, 16);
    if ((status |= m_alti->ENCAsyncCommandPutShort(data)) != AltiModule::SUCCESS) {
      string reason = " sending short command (data = 0x" + params[0] + ")";
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));		 
    }		 
    else {
      ERS_LOG(m_name << " sent BGo command (data = 0x" << std::hex << data << std::dec << ")");
    }
  }

  else if (command_name == "SendAsyncLongCommand") {
    if(params.size() != 4) {
      string reason = " should have 4 parameter, " + to_string(params.size()) + " received";
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    if(params[1] != "External" && params[1] != "Internal") {
      string reason = " address space is " + params[1] + ", should be External or Internal";
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    } 
    unsigned int address = stoi(params[0], 0, 16);
    AltiModule::TTC_ADDRESS_SPACE space = (params[1] == "External")? AltiModule::TTC_ADDRESS_SPACE::EXTERNAL : AltiModule::TTC_ADDRESS_SPACE::INTERNAL;
    unsigned int subaddress = stoi(params[2], 0, 16);
    unsigned int data = stoi(params[3], 0, 16);
    if ((status |= m_alti->ENCAsyncCommandPutLong(address, space, subaddress, data)) != AltiModule::SUCCESS) {
      string reason = " sending long command (address = 0x" + params[0] + " space = " + params[1] + " subaddress = " + params[2] + " data = 0x" + params[3] + ")";
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));		 
    }		 
    else {
      ERS_LOG(m_name << " sent Long BGo command(address = 0x" << std::hex << params[0] << " space = " <<  params[1] + " subaddress = 0x" << params[2] << " data = 0x" << data << std::dec << ")");
    }
  }

  else if (command_name == "SetBusy") {
    if ((status |= m_alti->BSYConstLevelRegWrite(true)) != AltiModule::SUCCESS) {
      string reason = " writing constant level busy register)";
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));		 
    }		 
    else {
      ERS_LOG(m_name << " set busy)");
    }
  }

  else if (command_name == "UnsetBusy") {
   if ((status |= m_alti->BSYConstLevelRegWrite(false)) != AltiModule::SUCCESS) {
      string reason = " writing constant level busy register)";
      ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));		 
    }		 
    else {
      ERS_LOG(m_name << " unset busy)");
    }
  }

  else if (command_name == "EnableTTC2LAN") {
    m_use_ttc2lan = true;
    ERS_LOG(m_name << " enable TTC2LAN)");
  }
  
  else if (command_name == "DisableTTC2LAN") {
    m_use_ttc2lan = false;
    ERS_LOG(m_name << " disable TTC2LAN)");
  }

  ERS_LOG(m_name << " Done");
}
 
    
 //========================================================================================================================

int AltiController::configureCtpSlave(const ALTIDAL::AltiModeCtpSlave* config, const std::vector<const ALTIDAL::AltiCalibrationRequest*>& requests){
  ERS_LOG(m_name << " Entered");
  
  int status = 0;

  // dump configuration
  config->print(1,true,std::cout);

  // clock from external
  m_alti_config->clk_config = true;
  m_alti_config->clk_select = AltiModule::CLK_PLL_TYPE::JITTER_CLEANER;
  m_alti_config->clk_jc_select = AltiModule::CLK_JC_TYPE::SWITCH;
  
  // disable pattern generator
  m_alti_config->mem_config = true;
  m_alti_config->mem_pg_enable = false;
 
  // Busy 
  // Busy propagated only to the CTP link in. 
  // Busy can come from:
  //                     - the CTP link-out: local ALTI connected to the CTP link-out 
  //                     - the ALTI link-out: local ALTI connected to the ALTI link-out 
  //                     - the LEMO cable
  // check that the busy are active in the database
  if (m_busy_from_ctp) {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_CTP] = true;
    cout << AltiModule::BUSY_INPUT::BUSY_FROM_CTP << endl;
    ERS_LOG(m_name << " Enabling busy from CTP link-out (\"" << m_busy_uid[0] << "\")");
  } 
  if (m_busy_from_alti) {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_ALTI] = true;
    cout << AltiModule::BUSY_INPUT::BUSY_FROM_ALTI << endl;
    ERS_LOG(m_name << " Enabling busy from ALTI link-out (\"" << m_busy_uid[1] << "\")");
 } 
  if (m_busy_from_nim) {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_FRONT_PANEL] = true;
    cout << AltiModule::BUSY_INPUT::BUSY_FROM_FRONT_PANEL << endl;
    ERS_LOG(m_name << " Enabling busy from LEMO cable (\"" << m_busy_uid[2] << "\")");
  } 

  // send busy to CTP link in 
  m_alti_config->bsy_out_select[AltiModule::BUSY_OUTPUT::BUSY_TO_CTP] = AltiModule::BUSY_SOURCE::BUSY_LOCAL; 
      
  // Switching of input -> ouput signal: all from CTP_IN
  for (u_int dst = 0; dst < AltiModule::SIGNAL_DESTINATION_NUMBER; dst++) {
    for (u_int sig = 0; sig < AltiModule::SIGNAL_NUMBER_ROUTED; sig++) {
      m_alti_config->sig_swx[sig][dst] = AltiModule::SIGNAL_SOURCE::CTP_IN;
    }
    m_alti_config->sig_swx_ttyp[dst] = AltiModule::SIGNAL_SOURCE::CTP_IN;
  }

  // enable all signals from external
  m_alti_config->sig_orb_source = "EXTERNAL";
  m_alti_config->sig_orb_input_source = "EXTERNAL";
  m_alti_config->sig_l1a_source = "EXTERNAL";
  for(u_int ttr = 0; ttr < 3; ttr++) {
    m_alti_config->sig_ttr_source[ttr] = "EXTERNAL";
  }
  for(u_int bgo = 0; bgo < 4; bgo++) {
    m_alti_config->sig_bgo_source[bgo] = "EXTERNAL";    
  }
  m_alti_config->sig_ttyp_source = "EXTERNAL";
 
  // enable equalizer for CTPIN and keep the ALTI equalizer in its current state
  bool alti_eqz_ena = false;
  if ((status |= m_alti->SIGEqualizersEnableRead(AltiModule::SIGNAL_SOURCE::ALTI_IN, alti_eqz_ena)) != AltiModule::SUCCESS) {
    string reason = " reading the ALTI_IN equalizer status)";
    ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));		 
  }	
  
  m_alti_config->sig_eqz_ctp_in_enable = true;
  m_alti_config->sig_eqz_alti_in_enable = alti_eqz_ena;

  // calibration requests: to CTP link-in 
  status |= this->configureCalibrationRequests(requests, AltiModule::CALREQ_OUTPUT::CALREQ_TO_CTP);
  
  ERS_LOG(m_name << " Done");
  return status;
}


//========================================================================================================================
 
int AltiController::configureAltiSlave(const ALTIDAL::AltiModeAltiSlave* config, const std::vector<const ALTIDAL::AltiCalibrationRequest*>& requests){ 
  ERS_LOG(m_name << " Entered");
  
  int status = 0;

  // dump configuration
  //config->print(1,true,std::cout);
  
  // clock from external
  m_alti_config->clk_config = true;
  m_alti_config->clk_select = AltiModule::CLK_PLL_TYPE::JITTER_CLEANER;
  m_alti_config->clk_jc_select = AltiModule::CLK_JC_TYPE::SWITCH;
 
  // disable pattern generator
  m_alti_config->mem_config = true;
  m_alti_config->mem_pg_enable = false;
  
  // Busy   
  // Busy propagated only to the ALTI link in. 
  // Busy can come from:
  //                     - the CTP link-out: local ALTI connected to the CTP link-out 
  //                     - the ALTI link-out: ALTI from other partition connected to ALTI link-out
  //                     - the LEMO cable
  // check that the busy are active in the database
  if (m_busy_from_ctp) {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_CTP] = true;
    cout << m_name << " " << AltiModule::BUSY_INPUT::BUSY_FROM_CTP << endl;
    ERS_LOG(m_name << " enabling busy from CTP link-out (\"" << m_busy_uid[0] << "\")");
  } 
 if (m_busy_from_alti) {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_ALTI] = true;
    cout << m_name <<  " " << AltiModule::BUSY_INPUT::BUSY_FROM_ALTI << endl;
    ERS_LOG( m_name << " enabling busy from ALTI link-out (\"" << m_busy_uid[1] << "\")");
  } 
  if (m_busy_from_nim) {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_FRONT_PANEL] = true;
    cout << m_name <<  " " << AltiModule::BUSY_INPUT::BUSY_FROM_FRONT_PANEL << endl;
    ERS_LOG(m_name << " enabling busy from LEMO cable (\"" << m_busy_uid[2] << "\")");
  } 

  // send busy to Alti link in
  m_alti_config->bsy_out_select[AltiModule::BUSY_OUTPUT::BUSY_TO_ALTI] = AltiModule::BUSY_SOURCE::BUSY_LOCAL; 

  // enable equalizer for ALTI_IN 
  m_alti_config->sig_eqz_alti_in_enable = true;

  // enable equalizer for ALTI_IN and keep the CTP_IN equalizer in its current state
  bool ctp_eqz_ena = false;
  if ((status |= m_alti->SIGEqualizersEnableRead(AltiModule::SIGNAL_SOURCE::CTP_IN, ctp_eqz_ena)) != AltiModule::SUCCESS) {
    string reason = " reading the CTP_IN equalizer status)";
    ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));		 
  }	
  m_alti_config->sig_eqz_ctp_in_enable = ctp_eqz_ena;

  // check input clock
  AltiModule::SIGNAL_SOURCE bc_source = AltiModule::SIGNAL_SOURCE::ALTI_IN;
  if(config->get_input_clock() == "CTP_IN") { 
    bc_source = AltiModule::SIGNAL_SOURCE::CTP_IN;
    m_alti_config->sig_eqz_ctp_in_enable = true;
  }

  // Switching of input -> ouput signal: all from ALTI_IN
  for (u_int dst = 0; dst < AltiModule::SIGNAL_DESTINATION_NUMBER; dst++) {
    for (u_int sig = 0; sig < AltiModule::SIGNAL_NUMBER_ROUTED; sig++) {
      if(sig == AltiModule::SIGNAL::BC) {
	m_alti_config->sig_swx[sig][dst] = bc_source;
      }
      else {
	m_alti_config->sig_swx[sig][dst] = AltiModule::SIGNAL_SOURCE::ALTI_IN;
      }
      m_alti_config->sig_swx_ttyp[dst] = AltiModule::SIGNAL_SOURCE::ALTI_IN;
    }
  }

  // enable all signals from external
  m_alti_config->sig_orb_source = "EXTERNAL";
  m_alti_config->sig_orb_input_source = "EXTERNAL";
  m_alti_config->sig_l1a_source = "EXTERNAL";
  for(u_int ttr = 0; ttr < 3; ttr++) {
    m_alti_config->sig_ttr_source[ttr] = "EXTERNAL";
  }
  for(u_int bgo = 0; bgo < 4; bgo++) {
    m_alti_config->sig_bgo_source[bgo] = "EXTERNAL";    
  }
  m_alti_config->sig_ttyp_source = "EXTERNAL";

  // calibration requests: to ALTI link-in 
  status |= this->configureCalibrationRequests(requests, AltiModule::CALREQ_OUTPUT::CALREQ_TO_ALTI);

  ERS_LOG(m_name << " Done");
  return status;
}  
 

//========================================================================================================================
 
int AltiController::configureLemoSlave(const ALTIDAL::AltiModeLemoSlave* config, const std::vector<const ALTIDAL::AltiCalibrationRequest*>& requests){ 
  ERS_LOG(m_name << " Entered");
  
  int status = 0;

  // dump configuration
  //config->print(1,true,std::cout);
  
  // clock from external
  m_alti_config->clk_config = true;
  m_alti_config->clk_select = AltiModule::CLK_PLL_TYPE::JITTER_CLEANER;
  m_alti_config->clk_jc_select = AltiModule::CLK_JC_TYPE::SWITCH;
  
  // disable pattern generator
  m_alti_config->mem_config = true;
  m_alti_config->mem_pg_enable = false;
  
  // Busy   
  // Busy propagated only to the CTP and ALTI link in. 
  // Busy can come from:
  //                     - the CTP link-out: local ALTI connected to the CTP link-out 
  //                     - the ALTI link-out: ALTI from other partition connected to ALTI link-out
  //                     - the LEMO cable
  // check that the busy are active in the database
  if (m_busy_from_ctp) {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_CTP] = true;
    cout << m_name << " " << AltiModule::BUSY_INPUT::BUSY_FROM_CTP << endl;
    ERS_LOG(m_name << " enabling busy from CTP link-out (\"" << m_busy_uid[0] << "\")");
  } 
 if (m_busy_from_alti) {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_ALTI] = true;
    cout << m_name << " " << AltiModule::BUSY_INPUT::BUSY_FROM_ALTI << endl;
    ERS_LOG(m_name << " enabling busy from ALTI link-out (\"" << m_busy_uid[1] << "\")");
  } 
  if (m_busy_from_nim) {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_FRONT_PANEL] = true;
    cout << m_name << " " << AltiModule::BUSY_INPUT::BUSY_FROM_FRONT_PANEL << endl;
    ERS_LOG(m_name << " enabling busy from LEMO cable (\"" << m_busy_uid[2] << "\")");
  } 

  // send busy to CTP and Alti link in
  m_alti_config->bsy_out_select[AltiModule::BUSY_OUTPUT::BUSY_TO_CTP] = AltiModule::BUSY_SOURCE::BUSY_LOCAL; 
  m_alti_config->bsy_out_select[AltiModule::BUSY_OUTPUT::BUSY_TO_ALTI] = AltiModule::BUSY_SOURCE::BUSY_LOCAL; 
 
  // Switching of input -> ouput signal: all from NIM_IN
  for (u_int dst = 0; dst < AltiModule::SIGNAL_DESTINATION_NUMBER; dst++) {
    for (u_int sig = 0; sig < AltiModule::SIGNAL_NUMBER_ROUTED; sig++) {
      if(AltiModule::SIGNAL_DESTINATION(dst) == AltiModule::SIGNAL_DESTINATION::TO_FPGA) m_alti_config->sig_swx[sig][dst] = AltiModule::SIGNAL_SOURCE::NIM_IN;
      else m_alti_config->sig_swx[sig][dst] = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
    }
    m_alti_config->sig_swx_ttyp[dst] = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
  }

  // enable all signals from external
  m_alti_config->sig_orb_source = "EXTERNAL";
  m_alti_config->sig_orb_input_source = "EXTERNAL";
  m_alti_config->sig_l1a_source = "EXTERNAL";
  for(u_int ttr = 0; ttr < 3; ttr++) {
    m_alti_config->sig_ttr_source[ttr] = "EXTERNAL";
  }
  for(u_int bgo = 0; bgo < 4; bgo++) {
    m_alti_config->sig_bgo_source[bgo] = "EXTERNAL";    
  }
  m_alti_config->sig_ttyp_source = "EXTERNAL";
  
  // keep current equalizer config
  bool ctp_eqz_ena = false;
  if ((status |= m_alti->SIGEqualizersEnableRead(AltiModule::SIGNAL_SOURCE::CTP_IN, ctp_eqz_ena)) != AltiModule::SUCCESS) {
    string reason = " reading the CTP_IN equalizer status)";
    ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));		 
  }	
  bool alti_eqz_ena = false;
  if ((status |= m_alti->SIGEqualizersEnableRead(AltiModule::SIGNAL_SOURCE::ALTI_IN, alti_eqz_ena)) != AltiModule::SUCCESS) {
    string reason = " reading the ALTI_IN equalizer status)";
    ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));		 
  }	
  m_alti_config->sig_eqz_ctp_in_enable = ctp_eqz_ena;
  m_alti_config->sig_eqz_alti_in_enable = alti_eqz_ena;

  // calibration requests: to CTP and ALTI link-in 
  status |= this->configureCalibrationRequests(requests, AltiModule::CALREQ_OUTPUT::CALREQ_TO_CTP);
  status |= this->configureCalibrationRequests(requests, AltiModule::CALREQ_OUTPUT::CALREQ_TO_ALTI);

  ERS_LOG(m_name << " Done");
  return status;
}  
 
//========================================================================================================================
 
int AltiController::configureAltiMaster(const ALTIDAL::AltiModeAltiMaster* config, const std::vector<const ALTIDAL::AltiCalibrationRequest*>& requests){
  ERS_LOG(m_name << " Entered");
  
  int status = 0;
  
  // dump configuration
  config->print(1,true,std::cout);

  // keep current equalizer config
  bool ctp_eqz_ena = false;
  if ((status |= m_alti->SIGEqualizersEnableRead(AltiModule::SIGNAL_SOURCE::CTP_IN, ctp_eqz_ena)) != AltiModule::SUCCESS) {
    string reason = " reading the CTP_IN equalizer status)";
    ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));		 
  }	
  bool alti_eqz_ena = false;
  if ((status |= m_alti->SIGEqualizersEnableRead(AltiModule::SIGNAL_SOURCE::ALTI_IN, alti_eqz_ena)) != AltiModule::SUCCESS) {
    string reason = " reading the ALTI_IN equalizer status)";
    ers::warning(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));		 
  }	
  m_alti_config->sig_eqz_ctp_in_enable = ctp_eqz_ena;
  m_alti_config->sig_eqz_alti_in_enable = alti_eqz_ena;

  // clock selection: internal or external (from CTP, ALTI or NIM)  
  m_alti_config->clk_config = true;
  m_alti_config->clk_select = AltiModule::CLK_PLL_TYPE::JITTER_CLEANER; // PLL from Jitter Cleaner
  AltiModule::SIGNAL_SOURCE bc_source = AltiModule::SIGNAL_SOURCE::FROM_FPGA; // BC source: internal, from CTP, from ALTI, from NIM
  bool stopped, out_of_range, glitch;
  if(config->get_input_clock() == "Internal") {
    bc_source = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
    m_alti_config->clk_jc_select = AltiModule::CLK_JC_TYPE::OSCILLATOR; // Jitter Cleaner from oscillator
  }
  else {
    if(config->get_input_clock() == "CTP_IN") { 
      if ((status = m_alti->CLKMonitorReadPLL(AltiModule::EXT_CLK_SOURCE::EXT_CTP_IN, stopped, out_of_range, glitch)) != 0) {
	string reason = " reading clock monitor status with code = " + to_string(status); 
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
	return status;
      }      
      bc_source = AltiModule::SIGNAL_SOURCE::CTP_IN;
      m_alti_config->sig_eqz_ctp_in_enable = true;
    }
    else if(config->get_input_clock() == "ALTI_IN") { 
      if ((status = m_alti->CLKMonitorReadPLL(AltiModule::EXT_CLK_SOURCE::EXT_ALTI_IN, stopped, out_of_range, glitch)) != 0) {
	string reason = " reading clock monitor status with code = " + to_string(status); 
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
	return status;
      }      
      bc_source = AltiModule::SIGNAL_SOURCE::ALTI_IN;
      m_alti_config->sig_eqz_alti_in_enable = true;
    }
    else if(config->get_input_clock() == "NIM_IN") {
      if ((status = m_alti->CLKMonitorReadPLL(AltiModule::EXT_CLK_SOURCE::EXT_NIM_IN, stopped, out_of_range, glitch)) != 0) {
	string reason = " reading clock monitor status with code = " + to_string(status); 
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
	return status;
      }      
      bc_source = AltiModule::SIGNAL_SOURCE::NIM_IN;
    }
    m_alti_config->clk_jc_select = AltiModule::CLK_JC_TYPE::SWITCH; // Jitter Cleaner from switch
  }
  
  // Master trigger
  const ALTIDAL::AltiMasterTriggerConfig* master_trigger_config = config->get_master_trigger_config();
  if (master_trigger_config) {
    this->configureMasterTrigger(master_trigger_config);
  }
  else {
    ERS_LOG(m_name << " no MasterTrigger object found");
  }
  
  // Busy   
  m_alti_config->bsy_l1a_masked = true;
  // set the busy to avoid spurious L1A
  m_alti_config->bsy_data_vme = true;
  
  for(u_int ttr = 0; ttr < 3; ttr++) m_alti_config->bsy_ttr_masked[ttr] = true;
  // Busy propagated only to the ALTI link in. 
  // Busy can come from:
  //                     - the CTP link-out: local ALTI connected to the CTP link-out 
  //                     - the ALTI link-out: ALTI from other partition connected to ALTI link-out
  //                     - the LEMO cable
  //                     - the PG (enabled by default) 
  //                     - the VME register (enabled)
  //                     - the ECR
  // check that the busy are active in the database
  if (m_busy_from_ctp) {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_CTP] = true;
    ERS_LOG(m_name << " enabling busy from CTP link-out (\"" << m_busy_uid[0] << "\")");
  } 
  if (m_busy_from_alti) {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_ALTI] = true;
    ERS_LOG(m_name << " enabling busy from ALTI link-out (\"" << m_busy_uid[1] << "\")");
  } 
  if (m_busy_from_nim) {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_FRONT_PANEL] = true;
    ERS_LOG(m_name << " enabling busy from LEMO cable (\"" << m_busy_uid[2] << "\")");
  } 
  m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_VME] = true;
  ERS_LOG(m_name << " enabling busy from VME");
  if(m_ecr_periodic) {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_ECR] = true;
    ERS_LOG(m_name << " enabling busy from ECR");
  }

  // do not send busy to CTP and ALTI link-in
  m_alti_config->bsy_out_select[AltiModule::BUSY_OUTPUT::BUSY_TO_CTP] = AltiModule::BUSY_SOURCE::BUSY_INACTIVE; 
  m_alti_config->bsy_out_select[AltiModule::BUSY_OUTPUT::BUSY_TO_ALTI] = AltiModule::BUSY_SOURCE::BUSY_INACTIVE; 
  
  
  // memory configuration of snapshot and pattern generator
  m_alti_config->mem_config = true;

  // pattern generator 
  const ALTIDAL::AltiPatternGeneratorConfig* pattern_generator = config->get_pattern_generator_config();
  if (pattern_generator) {
    ERS_LOG("Configure pattern generator"); 
    status = this->configurationPatternGenerator(pattern_generator);
  }
  
  
  // Switching of input -> ouput signal (except BC already done): all from FPGA, or NIM for Orbit, L1A, TTR1/2/3, or from CTP_IN or ALTI_IN for Orbit)
  AltiModule::SIGNAL_SOURCE orbit_source = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
  if(config->get_input_orbit() == "CTP_IN")       {
    orbit_source = AltiModule::SIGNAL_SOURCE::CTP_IN;
    m_alti_config->sig_orb_source = "EXTERNAL";    
    m_alti_config->sig_orb_input_source = "EXTERNAL";    
    m_alti_config->sig_eqz_ctp_in_enable = true;
  }
  else if(config->get_input_orbit() == "ALTI_IN") {
    orbit_source = AltiModule::SIGNAL_SOURCE::ALTI_IN;
    m_alti_config->sig_orb_source = "EXTERNAL";    
    m_alti_config->sig_orb_input_source = "EXTERNAL";    
    m_alti_config->sig_eqz_alti_in_enable = true;
  }
  else if(config->get_input_orbit() == "NIM_IN") {
    orbit_source = AltiModule::SIGNAL_SOURCE::NIM_IN;
    m_alti_config->sig_orb_source = "EXTERNAL";    
    m_alti_config->sig_orb_input_source = "EXTERNAL";    
  }
  else if(config->get_input_orbit() == "Internal") {
    orbit_source = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
    m_alti_config->sig_orb_source = "MINICTP";    
    m_alti_config->sig_orb_input_source = "MINICTP";    
  }

  AltiModule::SIGNAL_SOURCE l1a_source = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
  if(config->get_input_l1a() == "NIM_IN") {
    l1a_source = AltiModule::SIGNAL_SOURCE::NIM_IN;
    m_alti_config->sig_l1a_source = "EXTERNAL";    
  }
  else if(config->get_input_l1a() == "MINICTP") {
    m_alti_config->sig_l1a_source = "MINICTP";   
    m_alti_config->sig_ttyp_source = "MINICTP";
  }
  AltiModule::SIGNAL_SOURCE ttr1_source = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
  if(config->get_input_ttr1() == "NIM_IN")  {
    ttr1_source = AltiModule::SIGNAL_SOURCE::NIM_IN;
    m_alti_config->sig_ttr_source[0] = "EXTERNAL";    
  }
  
  AltiModule::SIGNAL_SOURCE ttr2_source = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
  if(config->get_input_ttr2() == "NIM_IN") {
    ttr2_source = AltiModule::SIGNAL_SOURCE::NIM_IN;
    m_alti_config->sig_ttr_source[1] = "EXTERNAL";    
  }
  AltiModule::SIGNAL_SOURCE ttr3_source = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
  if(config->get_input_ttr3() == "NIM_IN")  { 
    ttr3_source = AltiModule::SIGNAL_SOURCE::NIM_IN;
    m_alti_config->sig_ttr_source[2] = "EXTERNAL";    
  }
  for(u_int dst=0; dst< AltiModule::SIGNAL_DESTINATION_NUMBER; dst++) {
    for(u_int src=0; src< AltiModule::SIGNAL_NUMBER_ROUTED; src++) {
      if(src == AltiModule::SIGNAL::BC) {
	m_alti_config->sig_swx[src][dst] = bc_source;
      }
      else {
	// take external source only for the FPGA, and propagate signal from the FPGA
	if(AltiModule::SIGNAL_DESTINATION(dst) == AltiModule::SIGNAL_DESTINATION::TO_FPGA) {
	  if(src == AltiModule::SIGNAL::ORB)       m_alti_config->sig_swx[src][dst] = orbit_source;
	  else if(src == AltiModule::SIGNAL::L1A)  m_alti_config->sig_swx[src][dst] = l1a_source;
	  else if(src == AltiModule::SIGNAL::TTR1) m_alti_config->sig_swx[src][dst] = ttr1_source;     
	  else if(src == AltiModule::SIGNAL::TTR2) m_alti_config->sig_swx[src][dst] = ttr2_source;
	  else if(src == AltiModule::SIGNAL::TTR3) m_alti_config->sig_swx[src][dst] = ttr3_source;
	  else                                     m_alti_config->sig_swx[src][dst] = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
	}     
	else m_alti_config->sig_swx[src][dst] = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
      }
    }
    m_alti_config->sig_swx_ttyp[dst] = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
  } 
 
  // calibration requests: to ALTI link-in 
  status |= this->configureCalibrationRequests(requests, AltiModule::CALREQ_OUTPUT::CALREQ_TO_CTP);
  status |= this->configureCalibrationRequests(requests, AltiModule::CALREQ_OUTPUT::CALREQ_TO_ALTI);
 
  // mini-CTP 
  const ALTIDAL::AltiMiniCtpConfig* minictp = config->get_minictp_config();
  if (minictp) {
    ERS_LOG("Configure mini-CTP"); 
    status = this->configurationMiniCtp(minictp);
    m_do_minictp = true;
  }

  ERS_LOG(m_name << " Done");
  return status;
}

//========================================================================================================================
 
int AltiController::configureAltiExpert(const ALTIDAL::AltiModeAltiExpert* config) {
  ERS_LOG(m_name << " Entered");
  
  int status = 0;
 
  // dump configuration
  config->print(1,true,std::cout);

  string config_file = config->get_configuration_filename();
  m_alti_config->read(config_file);

  ERS_LOG(m_name << " Done");
  return status;
}

//========================================================================================================================

int AltiController::configureTiming(const ALTIDAL::AltiTiming* timing) {
  ERS_LOG(m_name << " Entered");
  int status = 0;

  u_int phase = timing->get_input_clock_phase_sync();
  bool  shape = timing->get_input_shaping();

  cout << m_name << " input signal synchronisation : " << endl;
  AltiModule::SIGNAL sig;
  for(u_int src = 0; src < AltiModule::SYNCHRONIZER_INPUT_SIGNAL_NUMBER; src++) {
    m_alti_config->sig_io_sync_phase[src] = phase;
    // apply shaping from OKS only to SWX from NIM
    if(src>=AltiModule::SYNCHRONIZER_INPUT_SIGNAL::SYNC_IN_TTYP0 && src<=AltiModule::SYNCHRONIZER_INPUT_SIGNAL::SYNC_IN_TTYP7) continue;
    m_alti_config->sig_io_shaping[src] = 0;
    if(src == AltiModule::SYNCHRONIZER_INPUT_SIGNAL::SYNC_IN_L1A) sig = AltiModule::SIGNAL::L1A;
    else if(src == AltiModule::SYNCHRONIZER_INPUT_SIGNAL::SYNC_IN_ORB) sig = AltiModule::SIGNAL::ORB;
    else if(src == AltiModule::SYNCHRONIZER_INPUT_SIGNAL::SYNC_IN_TTR1) sig = AltiModule::SIGNAL::TTR1;
    else if(src == AltiModule::SYNCHRONIZER_INPUT_SIGNAL::SYNC_IN_TTR2) sig = AltiModule::SIGNAL::TTR2;
    else if(src == AltiModule::SYNCHRONIZER_INPUT_SIGNAL::SYNC_IN_TTR3) sig = AltiModule::SIGNAL::TTR3;
    else if(src == AltiModule::SYNCHRONIZER_INPUT_SIGNAL::SYNC_IN_BGO0) sig = AltiModule::SIGNAL::BGO0;
    else if(src == AltiModule::SYNCHRONIZER_INPUT_SIGNAL::SYNC_IN_BGO1) sig = AltiModule::SIGNAL::BGO1;
    else if(src == AltiModule::SYNCHRONIZER_INPUT_SIGNAL::SYNC_IN_BGO2) sig = AltiModule::SIGNAL::BGO2;
    else if(src == AltiModule::SYNCHRONIZER_INPUT_SIGNAL::SYNC_IN_BGO3) sig = AltiModule::SIGNAL::BGO3;
    if(shape && (m_alti_config->sig_swx[sig][AltiModule::TO_FPGA] == AltiModule::SIGNAL_SOURCE::NIM_IN)) {
      m_alti_config->sig_io_shaping[src] = 1;
    }
    printf("  - signal: %s, phase = %d, %s \n", AltiModule::SIGNAL_NAME[src].c_str(), 
	   m_alti_config->sig_io_sync_phase[src], m_alti_config->sig_io_shaping[src] ? "SHAPE 1" : "SYNCHRONIZED");
  }

  // clock phase shift
  m_alti_config->clk_phase_shift = timing->get_clock_phase(); 
  cout << m_name << " clock phase shift: " << timing->get_clock_phase() << " steps of 15 ps = " << timing->get_clock_phase()*0.015  << " ns" << endl; 
   
  // PBM BCID offset
  int input = timing->get_pbm_bcid_offset();
  unsigned int offset; 
  if (input < 0) {
    offset = 3563+input;
  } 
  else offset = input;
  m_alti_config->pbm_bcid_offset = offset;

  // Jitter Cleaner configuration
  string jc_config = timing->get_JitterCleaner_filename();
  m_alti_config->clk_jc_file = jc_config;
  m_is_alti->jc_config = jc_config;
  cout << m_name << " Jitter Cleaner configuration: " << jc_config;
  
  ERS_LOG(m_name << " Done");
  return status;
}

//========================================================================================================================

int AltiController::configurationPatternGenerator(const ALTIDAL::AltiPatternGeneratorConfig* pattern_generator) {
  ERS_LOG(m_name << " Entered");
  int status = 0;
  
  pattern_generator->print(1,true,std::cout);
  m_use_pg = true;

  m_alti_config->mem_pg_repeat = pattern_generator->get_repeat();
  m_alti_config->mem_pg_start_addr = pattern_generator->get_start_address();
  m_alti_config->mem_pg_stop_addr = pattern_generator->get_stop_address();
  string pg_file = pattern_generator->get_pattern_filename();
  if(!std::ifstream(pg_file)) {
    ERS_REPORT_IMPL( ers::error, ers::Message, m_name << + " pattern generation file " + pg_file + " doesn't exist", );
  }
  m_alti_config->mem_pg_file = pg_file;
  
  // start PG during the FSM transition defined in OKS 
  m_alti_config->mem_pg_enable = false;
  m_start_pg_transition = pattern_generator->get_start_pattern_transition();
  m_keep_pg_running     = pattern_generator->get_keep_running();
  
  // number of L1A before stopping the pattern
  m_nb_l1a = 0;
  m_max_l1a = pattern_generator->get_max_L1A();
  
  // enable all signals in the BG, except the masked signals
  m_alti_config->sig_orb_source = "PATTERN";
  m_alti_config->sig_orb_input_source = "PATTERN";    
  m_alti_config->sig_l1a_source = "PATTERN";
  for(u_int ttr = 0; ttr < 3; ttr++) {
    m_alti_config->sig_ttr_source[ttr] = "PATTERN";
  }
  for(u_int bgo = 0; bgo < 4; bgo++) {
    m_alti_config->sig_bgo_source[bgo] = "PATTERN";    
    // disable BGo-1 if periodic ECR generated from HW
    if(bgo==1 && m_ecr_periodic) {
      m_alti_config->sig_bgo_source[bgo] = "MINICTP";    
    }
  }
  m_alti_config->sig_ttyp_source = "PATTERN";
  
  bool busy_masked = false;
  std::vector<const ALTIDAL::PGSignalMask*> v_masks = pattern_generator ->get_signals_masked();
  for(const ALTIDAL::PGSignalMask* mask : v_masks){
    if(mask->get_signal() == ALTIDAL::PGSignalMask::Signal::ORB) {
      m_alti_config->sig_orb_source = "MINICTP";
      m_alti_config->sig_orb_input_source = "MINICTP";    
    }
    else if(mask->get_signal() == ALTIDAL::PGSignalMask::Signal::L1A)  m_alti_config->sig_l1a_source = "INACTIVE";
    else if(mask->get_signal() == ALTIDAL::PGSignalMask::Signal::TTR1) m_alti_config->sig_ttr_source[0] = "INACTIVE";
    else if(mask->get_signal() == ALTIDAL::PGSignalMask::Signal::TTR2) m_alti_config->sig_ttr_source[1] = "INACTIVE";
    else if(mask->get_signal() == ALTIDAL::PGSignalMask::Signal::TTR3) m_alti_config->sig_ttr_source[2] = "INACTIVE";
    else if(mask->get_signal() == ALTIDAL::PGSignalMask::Signal::TTYP) m_alti_config->sig_ttyp_source = "INACTIVE";
    else if(mask->get_signal() == ALTIDAL::PGSignalMask::Signal::BGO0) m_alti_config->sig_bgo_source[0] = "INACTIVE";
    else if(mask->get_signal() == ALTIDAL::PGSignalMask::Signal::BGO1) m_alti_config->sig_bgo_source[1] = "INACTIVE";
    else if(mask->get_signal() == ALTIDAL::PGSignalMask::Signal::BGO2) m_alti_config->sig_bgo_source[2] = "INACTIVE";
    else if(mask->get_signal() == ALTIDAL::PGSignalMask::Signal::BGO3) m_alti_config->sig_bgo_source[3] = "INACTIVE";
    
    // for BUSY, do not mask the signal, but disable PG from local BUSY sources
    if(mask->get_signal() == ALTIDAL::PGSignalMask::Signal::BUSY) busy_masked = true;
  }
  if(busy_masked) {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_PG] = false;
    ERS_LOG(m_name << " diabling busy from Pattern Generator");
  }
  else {
    m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_PG] = true;
    ERS_LOG(m_name << " enabling busy from Pattern Generator");
  }
  
  ERS_LOG(m_name << " Done");
  return status;
}
  

//========================================================================================================================

int AltiController::configurationMiniCtp(const ALTIDAL::AltiMiniCtpConfig* minictp) {
  ERS_LOG(m_name << " Entered");
  int status = 0;
  
  minictp->print(1,true,std::cout);
  
  // enable L1A source in TTC from mini-CTP 
  m_alti_config->ttc_l1a_source[6] = true;
 
  //input signals
  if(minictp->get_input_l1a() == "CTP") {
    m_alti_config->ctp_l1a_source = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::L1A][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::CTP_IN;
  }
  else if(minictp->get_input_l1a() ==  "ALTI") {
    m_alti_config->ctp_l1a_source = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::L1A][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::ALTI_IN;
  }
  else if(minictp->get_input_l1a() ==  "NIM") {
    m_alti_config->ctp_l1a_source = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::L1A][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::NIM_IN;
  }
  else if(minictp->get_input_l1a() ==  "PATTERN") {
    m_alti_config->ctp_l1a_source = "PATTERN";
    m_alti_config->sig_swx[AltiModule::SIGNAL::L1A][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
  }
  else {
    ERS_REPORT_IMPL( ers::error, ers::Message, m_name << + " invalid mini-CTP L1A source: " + minictp->get_input_l1a(),);
  }
  
  if(minictp->get_input_bgo2() == "CTP") {
    m_alti_config->ctp_bgo2_source = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::BGO2][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::CTP_IN;
  }
  else if(minictp->get_input_bgo2() ==  "ALTI") {
    m_alti_config->ctp_bgo2_source = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::BGO2][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::ALTI_IN;
  }
  else if(minictp->get_input_bgo2() ==  "NIM") {
    m_alti_config->ctp_bgo2_source = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::BGO2][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::NIM_IN;
  }
  else if(minictp->get_input_bgo2() ==  "PATTERN") {
    m_alti_config->ctp_bgo2_source = "PATTERN";
    m_alti_config->sig_swx[AltiModule::SIGNAL::BGO2][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
  }
  else {
    ERS_REPORT_IMPL( ers::error, ers::Message, m_name << + " invalid mini-CTP BGo-2 source: " + minictp->get_input_bgo2(),);
  }
  
  if(minictp->get_input_bgo3() == "CTP") {
    m_alti_config->ctp_bgo3_source = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::BGO3][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::CTP_IN;
  }
  else if(minictp->get_input_bgo3() ==  "ALTI") {
    m_alti_config->ctp_bgo3_source = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::BGO3][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::ALTI_IN;
  }
  else if(minictp->get_input_bgo3() ==  "NIM") {
    m_alti_config->ctp_bgo3_source = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::BGO3][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::NIM_IN;
  }
  else if(minictp->get_input_bgo3() ==  "PATTERN") {
    m_alti_config->ctp_bgo3_source = "PATTERN";
    m_alti_config->sig_swx[AltiModule::SIGNAL::BGO3][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
  }
  else {
    ERS_REPORT_IMPL( ers::error, ers::Message, m_name << + " invalid mini-CTP BGo-3 source: " + minictp->get_input_bgo3(),);
  }
  
  if(minictp->get_input_ttr1() == "CTP") {
    m_alti_config->ctp_ttr_source[0] = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::TTR1][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::CTP_IN;
  }
  else if(minictp->get_input_ttr1() ==  "ALTI") {
    m_alti_config->ctp_ttr_source[0] = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::TTR1][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::ALTI_IN;
  }
  else if(minictp->get_input_ttr1() ==  "NIM") {
    m_alti_config->ctp_ttr_source[0] = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::TTR1][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::NIM_IN;
  }
  else if(minictp->get_input_ttr1() ==  "PATTERN") {
    m_alti_config->ctp_ttr_source[0] = "PATTERN";
    m_alti_config->sig_swx[AltiModule::SIGNAL::TTR1][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
  }
  else {
    ERS_REPORT_IMPL( ers::error, ers::Message, m_name << + " invalid mini-CTP TTR-1 source: " + minictp->get_input_ttr1(),);
  }
  
  if(minictp->get_input_ttr2() == "CTP") {
    m_alti_config->ctp_ttr_source[1] = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::TTR2][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::CTP_IN;
  }
  else if(minictp->get_input_ttr2() ==  "ALTI") {
    m_alti_config->ctp_ttr_source[1] = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::TTR2][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::ALTI_IN;
  }
  else if(minictp->get_input_ttr2() ==  "NIM") {
    m_alti_config->ctp_ttr_source[1] = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::TTR2][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::NIM_IN;
  }
  else if(minictp->get_input_ttr2() ==  "PATTERN") {
    m_alti_config->ctp_ttr_source[1] = "PATTERN";
    m_alti_config->sig_swx[AltiModule::SIGNAL::TTR2][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
  }
  else {
    ERS_REPORT_IMPL( ers::error, ers::Message, m_name << + " invalid mini-CTP TTR-2 source: " + minictp->get_input_ttr2(),);
  }
  
  if(minictp->get_input_ttr3() == "CTP") {
    m_alti_config->ctp_ttr_source[2] = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::TTR3][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::CTP_IN;
  }
  else if(minictp->get_input_ttr3() ==  "ALTI") {
    m_alti_config->ctp_ttr_source[2] = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::TTR3][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::ALTI_IN;
  }
  else if(minictp->get_input_ttr3() ==  "NIM") {
    m_alti_config->ctp_ttr_source[2] = "EXTERNAL";
    m_alti_config->sig_swx[AltiModule::SIGNAL::TTR3][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::NIM_IN;
  }
  else if(minictp->get_input_ttr3() ==  "PATTERN") {
    m_alti_config->ctp_ttr_source[2] = "PATTERN";
    m_alti_config->sig_swx[AltiModule::SIGNAL::TTR3][AltiModule::SIGNAL_DESTINATION::TO_FPGA] = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
  }
  else {
    ERS_REPORT_IMPL( ers::error, ers::Message, m_name << + " invalid mini-CTP TTR-3 source: " + minictp->get_input_ttr3(),);
  }
  
  m_alti_config->ctp_crq_source[0] = minictp->get_input_crq0();
  m_alti_config->ctp_crq_source[1] = minictp->get_input_crq1();
  m_alti_config->ctp_crq_source[2] = minictp->get_input_crq2();
  
  // items 
  std::vector<const ALTIDAL::AltiMiniCtpItem*> items = minictp->get_items();
  ERS_LOG(m_name << " Found " << items.size() << (items.size() > 1 ? " items" : "item"));
  u_int idx = 0;
  for (auto item : items) {
    item->print(1,true,std::cout);
    u_int thr;
    const double psc = item->get_prescale();
    if ((status |=  m_alti->CTPPrescalerToThreshold(psc, thr)) !=  AltiModule::SUCCESS) {
      string reason = "during CTPPrescalerToThreshold with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    m_alti_config->ctp_prsc_threshold[idx] = thr;
    // use CPU time + trigger item number as seed
    int seed = static_cast<int>(time(0) + idx);
    m_alti_config->ctp_prsc_seed[idx] = seed;
    m_alti_config->ctp_tav_enable |= (0x1 << idx);
    if(item->get_bgrp0()) m_alti_config->ctp_bgrp_mask[idx] |= 0x1;
    if(item->get_bgrp1()) m_alti_config->ctp_bgrp_mask[idx] |= 0x2;
    idx++;
  }
  // LUTs
  getTriggerItemLut(items, m_alti_config->ctp_item_lut);
  getTriggerTypeLut(items, m_alti_config->ctp_ttyp_lut);
 
  // deadtime
  const ALTIDAL::AltiDeadtimeConfig* deadtime = minictp->get_deadtime();
  m_alti_config->ctp_smpl_deadtime         = deadtime->get_simple();
  m_alti_config->ctp_cplx_bucket[0].rate   = deadtime->get_leakyBucket0_rate();
  m_alti_config->ctp_cplx_bucket[0].level  = deadtime->get_leakyBucket0_size();
  m_alti_config->ctp_cplx_bucket[0].enable = deadtime->get_leakyBucket0_enable();
  m_alti_config->ctp_cplx_bucket[1].rate   = deadtime->get_leakyBucket1_rate();
  m_alti_config->ctp_cplx_bucket[1].level  = deadtime->get_leakyBucket1_size();
  m_alti_config->ctp_cplx_bucket[1].enable = deadtime->get_leakyBucket1_enable();
  m_alti_config->ctp_cplx_bucket[2].rate   = deadtime->get_leakyBucket2_rate();
  m_alti_config->ctp_cplx_bucket[2].level  = deadtime->get_leakyBucket2_size();
  m_alti_config->ctp_cplx_bucket[2].enable = deadtime->get_leakyBucket2_enable();
  m_alti_config->ctp_cplx_bucket[3].rate   = deadtime->get_leakyBucket3_rate();
  m_alti_config->ctp_cplx_bucket[3].level  = deadtime->get_leakyBucket3_size();
  m_alti_config->ctp_cplx_bucket[3].enable = deadtime->get_leakyBucket3_enable();
  m_alti_config->ctp_cplx_sliding_window.window = deadtime->get_slidingWindow_window();
  m_alti_config->ctp_cplx_sliding_window.number = deadtime->get_slidingWindow_size();
  m_alti_config->ctp_cplx_sliding_window.enable = deadtime->get_slidingWindow_enable();
  
  // bunch-group
  std::string bg_file = minictp->get_bg_file();
  m_alti_config->ctp_bgrp_file = bg_file;
  /*if(bg_file.empty()) {
    TrigConf::L1BunchGroupSet bunch_group_set;
    u_int nb_bunch_groups = 0;
    u_int bgk = minictp->get_bg_key();
    std::string db_alias = minictp->get_db_alias();
    bool success = false;
    try {
      TrigConf::TrigDBL1BunchGroupSetLoader db_loader(db_alias);
      success = db_loader.loadBunchGroupSet(bgk, bunch_group_set);	
      if(success) {
	nb_bunch_groups = bunch_group_set.size();
	ERS_LOG(m_name << " Bunch-Groups loaded from triggerDB,  number of bunch-groups: " << nb_bunch_groups);
	if(nb_bunch_groups == 0) 
	  {
	    string reason("0 bunch groups defined!");
	    ers::error(ALTI::AltiController::ReadFromTriggerDB(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
	  }
	else if(nb_bunch_groups > 16) 
	  {
	    string reason = "more than 16 bunch groups defined!";
	    ers::error(ALTI::AltiController::ReadFromTriggerDB(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
	  }
	
	bunch_group_set.printSummary(true);
      }
      else {
	string reason( "Loading of Bunch-Groups from TriggerDB failed");
	ers::error(ALTI::AltiController::ReadFromTriggerDB(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      }
    }
    catch (std::exception& ex) {
      string what = ex.what();
      string reason = "Caught exception while connecting to TriggerDB: " + what + "; do not download bunch groups";
      ers::error(ALTI::AltiController::ReadFromTriggerDB(ERS_HERE, ROS::IOMPlugin::m_uid, reason));      
      throw;
    }
    catch (...) {
      string reason = "Unknown Exception caught";
      ers::error(ALTI::AltiController::ReadFromTriggerDB(ERS_HERE, ROS::IOMPlugin::m_uid, reason));      
      throw;      
    }
    
    std::vector<u_int> v_bg;
    v_bg.resize(AltiModule::BGRP_WORD_TOTAL);
    for (u_int i = 0; i < AltiModule::BGRP_NUMBER; i++) {
      const std::shared_ptr<TrigConf::L1BunchGroup> bunch_group = bunch_group_set.getBunchGroup(i);
      std::vector<uint16_t> bunches = bunch_group->bunches();
      for (int bcid : bunches) { 
	u_int word_number = (bcid / AltiModule::WORD_SIZE) + (i * AltiModule::BCID_WORD_NUMBER);
	u_int offset = (bcid % AltiModule::WORD_SIZE); 
	v_bg[word_number] |= (1 << offset);
      }
    }
    m_alti_config->ctp_bgrp = v_bg;
    }*/ 
  
  ERS_LOG(m_name << " Done");
  return status;
}
  
//========================================================================================================================

int AltiController::getTriggerItemLut(const std::vector<const ALTIDAL::AltiMiniCtpItem*> items, std::vector<u_int> &lut) {
  ERS_LOG(m_name << " Entered");
  int status = 0;
  
  lut.clear();
  lut.resize(AltiModule::ITEM_LUT_SIZE);
  
  for (u_int i = 0; i < AltiModule::ITEM_LUT_SIZE; i++) {
    u_int word = 0x0;
    // list of items set
    std::bitset<AltiModule::MINICTP_INPUT_NUMBER> inputs_id(i);
    for (u_int j = 0; j < items.size(); j++) {
      std::vector<const ALTIDAL::AltiMiniCtpItemInput*> inputs = items[j]->get_inputs();
      bool match = true;
      for (auto input : inputs) {
	u_int bit = 0;
	// get the corresponding bit of each input
	for (std::string minictp_input : AltiModule::MINICTP_INPUT_NAME) {
	  if(input->get_signal() == minictp_input) break;
	  bit ++;
	}
	// if the input is not in the bitset, skip the item
	if(!inputs_id.test(bit)) {
	  match = false;
	  continue;
	}
      }
      // if all inputs found, set the item
      if(match) word |= (0x1 << j);
    }
    lut[i] = word;
  }

  ERS_LOG(m_name << " Done");
  return status;
}

//========================================================================================================================

int AltiController::getTriggerTypeLut(const std::vector<const ALTIDAL::AltiMiniCtpItem*> items, std::vector<u_int> &lut) {
  ERS_LOG(m_name << " Entered");
  int status = 0;
  
  lut.clear();
  lut.resize(AltiModule::TTYP_LUT_SIZE);
  
  for (u_int i = 0; i < AltiModule::TTYP_LUT_SIZE; i++) {
    u_int word = 0x0;
    // list of items set
    std::bitset<AltiModule::ITEM_NUMBER> items_id(i);
    for (u_int j = 0; j < items.size(); j++) {
      if(items_id.test(j)) word |= items[j]->get_trigger_type();
    }
    lut[i] = word;
  }

  ERS_LOG(m_name << " Done");
  return status;
}

//========================================================================================================================
 
void AltiController::resetBGoChannelConfiguration() {
  ERS_LOG(m_name << " Entered");
  int status = 0;

  m_cycles_for_configure.clear();
  m_cycles_for_connect.clear();
  m_cycles_for_prepareForRun.clear();
  
  m_bgo_cycles.clear();
  // set to defaults the 4 BGo signals: disable output, async, single, no fifo, basic
  for (u_int i = 0; i < AltiModule::TTC_FIFO_NUMBER; ++i) {
    m_alti_config->ttc_bgo_mode[i] = LVL1::AltiModule::TTC_BGO_MODE::ASYNCHRONOUS_BGO_SIGNAL; 
    m_alti_config->ttc_fifo_retransmit[i] = true;
    std::vector<u_int> list(0);
    m_bgo_cycles.push_back(list);
  }

  // reset FIFOs
  if ((status |= m_alti->ENCFifoReset()) !=  AltiModule::SUCCESS) {
    string reason = "during ENCFifoReset with code = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }

  ERS_LOG(m_name << " Done");
}

//========================================================================================================================

void AltiController::setBGoChannel(const ALTIDAL::AltiBGoChannel* BGoChannel) {
 
  ERS_LOG(m_name << " Entered");
 
  ERS_LOG(m_name << " setting up BGo-Channel " << BGoChannel->UID());
 
  // BGo channel number
  u_int number = BGoChannel->get_number();   
  ERS_LOG(m_name << " - channel number = " << number);
   
  // source if the BGo signal
  AltiModule::SIGNAL bgo_signal = AltiModule::SIGNAL::BGO0;
  if(number==0) bgo_signal = AltiModule::SIGNAL::BGO0;
  else if(number==1) bgo_signal = AltiModule::SIGNAL::BGO1;
  else if(number==2) bgo_signal = AltiModule::SIGNAL::BGO2;
  else if(number==3) bgo_signal = AltiModule::SIGNAL::BGO3;
  
  string source_str = BGoChannel->get_source();
  AltiModule::SIGNAL_SOURCE source = AltiModule::SIGNAL_SOURCE::CTP_IN;
  if(source_str=="CTP") {
    source = AltiModule::SIGNAL_SOURCE::CTP_IN;
    m_alti_config->sig_bgo_source[number] = "EXTERNAL";    
  }
  else if(source_str=="ALTI") {
    source = AltiModule::SIGNAL_SOURCE::ALTI_IN;
    m_alti_config->sig_bgo_source[number] = "EXTERNAL";    
  }
  else if(source_str=="NIM") { 
    source = AltiModule::SIGNAL_SOURCE::NIM_IN;
    m_alti_config->sig_bgo_source[number] = "EXTERNAL";    
  }
  else if(source_str=="PG") { 
    source = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
    m_alti_config->sig_bgo_source[number] = "PATTERN";    
  }
  else if(source_str=="MINICTP") { 
    source = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
    m_alti_config->sig_bgo_source[number] = "MINICTP";    
  }
  for(u_int dst=0; dst<AltiModule::SIGNAL_DESTINATION_NUMBER; dst++) {
    m_alti_config->sig_swx[bgo_signal][dst] = source;
  }
  
  
  // BGo mode
  string mode = BGoChannel->get_mode();
  if(mode=="synchronous_single_bgo_signal")        m_alti_config->ttc_bgo_mode[number] = LVL1::AltiModule::TTC_BGO_MODE::SYNCHRONOUS_SINGLE_BGO_SIGNAL;
  else if(mode=="synchronous_repetitive")          m_alti_config->ttc_bgo_mode[number] = LVL1::AltiModule::TTC_BGO_MODE::SYNCHRONOUS_REPETITIVE;
  else if(mode=="asynchronous_bgo_signal")         m_alti_config->ttc_bgo_mode[number] = LVL1::AltiModule::TTC_BGO_MODE::ASYNCHRONOUS_BGO_SIGNAL;
  else if(mode=="asynchonous_vme_on_trigger")      m_alti_config->ttc_bgo_mode[number] = LVL1::AltiModule::TTC_BGO_MODE::ASYNCHRONOUS_VME_ON_TRIGGER;
  else if(mode=="asynchronous_vme_when_not_empty") m_alti_config->ttc_bgo_mode[number] = LVL1::AltiModule::TTC_BGO_MODE::ASYNCHRONOUS_FIFO_MODE;
   
  // retransmit
  m_alti_config->ttc_fifo_retransmit[number] = BGoChannel->get_retransmit();
  

  // Inhibit Delay
  m_alti_config->ttc_bgo_inhibit_delay[number] = BGoChannel->get_inhibit_delay();
  ERS_LOG(m_name << " - InhibitDelay = 0x" << hex <<  m_alti_config->ttc_bgo_inhibit_delay[number] << " (" << dec <<  m_alti_config->ttc_bgo_inhibit_delay[number] << " dec)");
  // Inhibit width
  m_alti_config->ttc_bgo_inhibit_width[number] = BGoChannel->get_inhibit_width();
  ERS_LOG(m_name << " - Inhibit width = " << hex <<  m_alti_config->ttc_bgo_inhibit_width[number] << "(" << dec <<  m_alti_config->ttc_bgo_inhibit_width[number] << " dec)");

  // busy gate
  m_alti_config->bsy_bgo_masked[number] = BGoChannel->get_busy_gate();

  // Cycles  
  m_bgo_cycles[number] = this->getCycles(BGoChannel->get_cycles());
  
  ERS_LOG(m_name << " dump cycles");
  this->dumpCycles(m_bgo_cycles[number]);

  ERS_LOG(m_name << " Done");
}

//========================================================================================================================

void AltiController::setBGoBCR(const ALTIDAL::AltiBGoBCR* bcr) {
  ERS_LOG(m_name << " Entered");
  
  cout << m_name << " setting up BCR " << bcr->UID() << endl;
  m_alti_config->ttc_bgo_inhibit_delay[0] =  bcr->get_delay();
  u_int data = bcr->get_data();
  cout << m_name << " - inhibit delay = " <<  m_alti_config->ttc_bgo_inhibit_delay[0] << endl;
  cout << m_name << " - data = " << data <<  endl;
  m_alti_config->ttc_bgo_inhibit_width[0] = 52;
  m_alti_config->ttc_bgo_mode[0] = LVL1::AltiModule::TTC_BGO_MODE::SYNCHRONOUS_REPETITIVE;
  m_alti_config->bsy_bgo_masked[0] = false;
  m_alti_config->ttc_fifo_retransmit[0] = true;
  m_bgo_cycles[0].clear();
  u_int bcr_cycle = (data << 23);
  m_bgo_cycles[0].push_back(bcr_cycle);
  cout << m_name << " dump cycles" << endl;
  this->dumpCycles(m_bgo_cycles[0]);
 
  ERS_LOG(m_name << " Done");
}


//========================================================================================================================

void AltiController::setBGoECR(const ALTIDAL::AltiBGoECR* ecr) {
  ERS_LOG(m_name << " Entered");
 
  cout << m_name << " setting up ECR " << ecr->UID() << endl;
  u_int data = ecr->get_data();
  cout << m_name << " - data = " << data <<  endl;
  m_alti_config->ttc_bgo_inhibit_delay[1] = 0;
  m_alti_config->ttc_bgo_inhibit_width[1] = 0;
  m_alti_config->ttc_bgo_mode[1] = LVL1::AltiModule::TTC_BGO_MODE::ASYNCHRONOUS_BGO_SIGNAL;
  m_alti_config->bsy_bgo_masked[1] = false;
  m_alti_config->ttc_fifo_retransmit[1] = true;
  m_bgo_cycles[1].clear();
  m_ecr_word = (data << 23);
  m_bgo_cycles[1].push_back(m_ecr_word);
  cout << m_name << " dump cycles" << endl;
  this->dumpCycles(m_bgo_cycles[1]);

  ERS_LOG(m_name << " Done");
}

//========================================================================================================================
 
u_int AltiController::getCycle(const ALTIDAL::AltiCycleBase* const& cycle) {
  u_int result = 0;
  if (const ALTIDAL::AltiCycleShort* cycle_short = ROS::IOMPlugin::m_configurationDB->cast<ALTIDAL::AltiCycleShort>(cycle)) {
    result = (0xff & cycle_short->get_command()) << 23;
  }
  else if (const ALTIDAL::AltiCycleLong* cycle_long = ROS::IOMPlugin::m_configurationDB->cast<ALTIDAL::AltiCycleLong>(cycle)) {
    // encode the command:
    result = (0x1 << 31);
    result |= 0xff & cycle_long->get_data();
    result |= ((0xff & cycle_long->get_subaddress()) << 8);
    bool external = (cycle_long->get_registers()=="External") ? true : false;
    if (external) {
      result |= (0x1 << 16);
    }
    result |= ((0x3fff & cycle_long->get_TTCrxAddress()) << 17);
  }
  return(result);
}

//========================================================================================================================

std::vector<u_int> AltiController::getCycles(const std::vector<const ALTIDAL::AltiCycleBase*>& cycles) {
  std::vector<u_int> results;
  for (std::vector<const ALTIDAL::AltiCycleBase*>::const_iterator it = cycles.begin(); it != cycles.end(); ++it) {
    // if it is a container, loop over all cycles
    if (const ALTIDAL::AltiCycleContainer* container = ROS::IOMPlugin::m_configurationDB->cast<ALTIDAL::AltiCycleContainer>(*it)) {
      std::vector<u_int> results_container = this->getCycles(container->get_cycles());
      for(u_int cycle : results_container) {
	results.push_back(cycle);
      }
    }
    else {
      results.push_back(this->getCycle(*it));
    }
  }
  return results;
}
 
//========================================================================================================================

int AltiController::sendAsyncCycles(std::vector<u_int>& cycles) {
  ERS_LOG(m_name << " Entered");

  int status = 0;
  
  for(std::vector<u_int>::const_iterator it = cycles.begin(); it != cycles.end(); ++it) {
    
    if( ((*it) & (0x1<<31)) == 0x0 ) {
      u_int data = static_cast<u_char>(((*it) & (0xff<<23))>>23);
      ERS_LOG(m_name << " sending asynchronous short-format cycle = 0x" 
	      << std::hex <<  static_cast<unsigned short>(data) << std::dec);
      
      if ((status |= m_alti->ENCAsyncCommandPutShort(data)) !=  AltiModule::SUCCESS) {
	string reason = "during ENCAsyncCommandPutShort with code = " + to_string(status);
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
	}
      } else {
	u_int address = (((*it) & (0x3fff<<17)) >> 17);
	bool external = (((*it)&(0x1<<16))!=0) ? true : false;
	AltiModule::TTC_ADDRESS_SPACE space = (external)? AltiModule::TTC_ADDRESS_SPACE::EXTERNAL : AltiModule::TTC_ADDRESS_SPACE::INTERNAL;
        u_int sub_address = (((*it) & (0xff<<8)) >>8);
	u_int data = (*it) & 0xff;
	
	ERS_LOG(m_name << " sending asynchronous long-format cycle: address=" 
		<< std::hex << address 
		<< " space=" << AltiModule::TTC_ADDRESS_SPACE_NAME[space]
		<< " sub address=0x" << static_cast<u_short>(sub_address)
		<< " data=0x" << static_cast<u_short>(data) << std::dec);
	
	if ((status = m_alti->ENCAsyncCommandPutLong(address, space, sub_address, data))  != AltiModule::SUCCESS) {
	    string reason = "during ENCAsyncCommandPutLong with code = " + to_string(status);
	    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
	}
      }
    }
								  
  ERS_LOG(m_name << " Done");
  return status;
}


//========================================================================================================================

int AltiController::setBGoCycles(std::vector<std::vector<u_int> >& cycles) {
  ERS_LOG(m_name << " Entered");

  int status = 0;
  
  for(u_int bgo=0; bgo < AltiModule::TTC_FIFO_NUMBER; bgo++) {
    ERS_LOG(m_name << " cycles for BGo channel : " << bgo);
    for(std::vector<u_int>::const_iterator it = cycles[bgo].begin(); it != cycles[bgo].end(); ++it) {
      
      if( ((*it) & (0x1<<31)) == 0x0 ) {
	u_int data = static_cast<u_char>(((*it) & (0xff<<23))>>23);
	ERS_LOG(m_name << " set short-format cycle = 0x" 
		<< std::hex <<  static_cast<unsigned short>(data) << std::dec);
	
	if ((status |= m_alti->ENCBgoCommandPutShort(bgo, data)) !=  AltiModule::SUCCESS) {
	  string reason = "during TTCPutBGOCommandShort with code = " + to_string(status);
	  ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
	}
      } else {
	u_int address = (((*it) & (0x3fff<<17)) >> 17);
	bool external = (((*it)&(0x1<<16))!=0) ? true : false;
	AltiModule::TTC_ADDRESS_SPACE space = (external)? AltiModule::TTC_ADDRESS_SPACE::EXTERNAL : AltiModule::TTC_ADDRESS_SPACE::INTERNAL;
        u_int sub_address = (((*it) & (0xff<<8)) >>8);
	u_int data = (*it) & 0xff;
	
	ERS_LOG(m_name << " set long-format cycle: address=" 
		<< std::hex << address 
		<< " space=" << AltiModule::TTC_ADDRESS_SPACE_NAME[space]
		<< " sub address=0x" << static_cast<u_short>(sub_address)
		<< " data=0x" << static_cast<u_short>(data) << std::dec);
	
	if ((status = m_alti->ENCBgoCommandPutLong(bgo, address, space, sub_address, data))  != AltiModule::SUCCESS) {
	    string reason = "during TTCPutBGOCommandLong with code = " + to_string(status);
	    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
	}
      }
    }
  }  
  ERS_LOG(m_name << " Done");
  return status;
}

//========================================================================================================================

void AltiController::dumpCycles(std::vector<u_int>& cycles) {
  u_int i = 0;
  for(std::vector<u_int>::const_iterator it = cycles.begin(); it != cycles.end(); ++it) {
    cout << m_name << "   - word(" << i << ")=0x" << std::hex << *it << std::dec << endl;
    ++i;
  }
  
}

//========================================================================================================================

 int AltiController::configureCalibrationRequests(const std::vector<const ALTIDAL::AltiCalibrationRequest*>& requests, AltiModule::CALREQ_OUTPUT output) {
   ERS_LOG(m_name << " Entered");
   
   m_alti_config->crq_config = true;
   for(const ALTIDAL::AltiCalibrationRequest* request : requests) {
     u_int bit = request->get_bit();
    
     // input signal
     AltiModule::CALREQ_INPUT input = AltiModule::CALREQ_INPUT::CALREQ_FROM_FRONT_PANEL;
     string input_str = request->get_input();
     if(input_str=="RJ45")             input = AltiModule::CALREQ_INPUT::CALREQ_FROM_RJ45;
     else if(input_str=="CTP")         input = AltiModule::CALREQ_INPUT::CALREQ_FROM_CTP;
     else if(input_str=="ALTI")        input = AltiModule::CALREQ_INPUT::CALREQ_FROM_ALTI;
     else if(input_str=="PG")          input = AltiModule::CALREQ_INPUT::CALREQ_FROM_PG;
     else if(input_str=="VME")         input = AltiModule::CALREQ_INPUT::CALREQ_FROM_VME;
     else if(input_str=="FRONT_PANEL") { 
       input = AltiModule::CALREQ_INPUT::CALREQ_FROM_FRONT_PANEL; 
       // set TTR from NIM and disable shaping
       for (u_int sig = AltiModule::SIGNAL::TTR1; sig <= AltiModule::SIGNAL::TTR3; sig++) {
	 for (u_int dst = 0; dst < AltiModule::SIGNAL_DESTINATION_NUMBER; dst++) {
	   m_alti_config->sig_swx[sig][dst] = AltiModule::SIGNAL_SOURCE::NIM_IN;
	 }
       }
       for (u_int sig = AltiModule::SWX_TTR1; sig <= AltiModule::SWX_TTR3; sig++) {
	 m_alti_config->sig_io_shaping[sig] = 1;
       }
     }
     m_alti_config->crq_in_select[bit] = input;

     // source of output signal
     AltiModule::CALREQ_SOURCE source = AltiModule::CALREQ_SOURCE::CALREQ_LOCAL;
     string source_str = request->get_source();
     if(source_str=="Inactive")   source = AltiModule::CALREQ_SOURCE::CALREQ_INACTIVE;
     else if(source_str=="Local") source = AltiModule::CALREQ_SOURCE::CALREQ_LOCAL;
     else if(source_str=="CTP")   source = AltiModule::CALREQ_SOURCE::CALREQ_CTP;
     else if(source_str=="ALTI")  source = AltiModule::CALREQ_SOURCE::CALREQ_ALTI;
     m_alti_config->crq_out_select[output][bit] = source;

     // turn counter signal
     string turn_signal_output = request->get_turn_signal_output();
     if(turn_signal_output=="TTR1") {
       m_alti_config->sig_ttr_source[0] = "TURNSIGNAL";
       m_alti_config->sig_swx[AltiModule::SIGNAL::TTR1][AltiModule::SIGNAL_DESTINATION::NIM_OUT] = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
       m_alti_config->bsy_ttr_masked[0] = false;
     }
     else if(turn_signal_output=="TTR2") {
       m_alti_config->sig_ttr_source[1] = "TURNSIGNAL";
       m_alti_config->sig_swx[AltiModule::SIGNAL::TTR2][AltiModule::SIGNAL_DESTINATION::NIM_OUT] = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
       m_alti_config->bsy_ttr_masked[1] = false;
     }
     else if(turn_signal_output=="TTR3") {
       m_alti_config->sig_ttr_source[2] = "TURNSIGNAL";
       m_alti_config->sig_swx[AltiModule::SIGNAL::TTR3][AltiModule::SIGNAL_DESTINATION::NIM_OUT] = AltiModule::SIGNAL_SOURCE::FROM_FPGA;
       m_alti_config->bsy_ttr_masked[2] = false;
     }
     m_alti_config->turn_cnt_mask =  request->get_turn_signal_mask();
     m_alti_config->turn_cnt_max =  request->get_turn_signal_maximum();
   }
   ERS_LOG(m_name << " Done");
   
   return AltiModule::SUCCESS;
 }   

//========================================================================================================================

int AltiController::readClockStatus() {
  
  int status;

  AltiModule::CLK_PLL_TYPE pll_selection;
  if ((status = m_alti->CLKInputSelectReadPLL(pll_selection)) != AltiModule::SUCCESS) {
    string reason = "reading CLK PLL input selection with code = " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    return status;
  }

  bool lol, los, sticky_lol, sticky_los;
  if ((status = m_alti->CLKStatusReadPLL(lol, los, sticky_lol, sticky_los)) != AltiModule::SUCCESS) {
    string reason = "reading CLK PLL status with code = " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    return status;
  }

  bool jc_intr, jc_lol, jc_los, jc_sticky_intr, jc_sticky_lol, jc_sticky_los;
  if ((status = m_alti->CLKStatusReadJitterCleaner(jc_intr, jc_lol, jc_los, jc_sticky_intr, jc_sticky_lol, jc_sticky_los)) != AltiModule::SUCCESS) {
    string reason = "reading CLK jitter cleaner status with code = " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    return status;  
  }

  u_int i;
  AltiModule::EXT_CLK_SOURCE src;
  bool stopped[AltiModule::EXT_CLK_SOURCE_NUMBER], oor[AltiModule::EXT_CLK_SOURCE_NUMBER], glitch[AltiModule::EXT_CLK_SOURCE_NUMBER];
  for(i = 0; i < AltiModule::EXT_CLK_SOURCE_NUMBER; i++) {
    src = AltiModule::EXT_CLK_SOURCE(i);
    if ((status = m_alti->CLKMonitorReadPLL(src, stopped[i], oor[i], glitch[i])) != AltiModule::SUCCESS) {
      string reason = "reading CLK PLL monitor status with code = " + to_string(status); 
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      return status;      
    }
  }

  std::string designid;
  if ((status = m_alti->CLKJitterCleanerDesignIDRead(designid)) != AltiModule::SUCCESS) {
    string reason = "reading design ID from I2C JitterCleaner with code = " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    return status;  
  }

  AltiModule::CLK_JC_TYPE jc_selection;
  if ((status = m_alti->CLKInputSelectReadJitterCleaner(jc_selection)) != AltiModule::SUCCESS) {
    string reason = "reading JC input selection with code = " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    return status;
  }

  // from I2C
  std::vector<bool> v_jc_oof, v_jc_los;
  bool jc_hold, jc_lol2;
  if ((status = m_alti->CLKJitterCleanerStatusRead(v_jc_oof, v_jc_los, jc_hold, jc_lol2)) != AltiModule::SUCCESS) {
    string reason = "reading statuses from I2C JitterCleaner with code = " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    return status;
  }
  std::vector<bool> v_jc_sticky_oof, v_jc_sticky_los;
  bool jc_sticky_hold, jc_sticky_lol2;
  if ((status = m_alti->CLKJitterCleanerStickyStatusRead(v_jc_sticky_oof, v_jc_sticky_los, jc_sticky_hold, jc_sticky_lol2)) != AltiModule::SUCCESS) {
    string reason = "reading sticky statuses from I2C JitterCleaner with code = " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    return status;
  }
 
  int steps;
  if ((status = m_alti->CLKPhasePositionReadPLL(steps)) != 0) {
    string reason = "reading PLL output clock phase position with code = " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    return status;
  }
  
  std::printf("\n");
  std::printf("PLL  input  clock: %s, %s (sticky) \n", ((pll_selection == AltiModule::FROM_SWITCH) ? "from switch" : ((pll_selection == AltiModule::JITTER_CLEANER) ? "from jitter cleaner" : "")), sticky_los ? "loss of signal" : "OK");
  std::printf("PLL  output clock: %-3s(current)%s \n", lol ? "ERR" : "OK", sticky_lol ? ", loss of lock (sticky)" : "");
  
  std::printf("\n");
  if (steps > 30000){
    steps = (65535 - steps);
    std::printf("PLL  output phase shift: ~ -%5.2f ns \n", steps*0.014);
  } else{
    std::printf("PLL  output phase shift: ~ %5.2f ns \n", steps*0.014);
  }
  
  std::printf("\n");
  std::printf("Jitter cleaner input  clock: %s, %s (sticky)  \n", ((jc_selection == AltiModule::OSCILLATOR) ? "from oscillator" : ((jc_selection == AltiModule::SWITCH) ? "from switch" : "")), jc_sticky_los ? "loss of signal" : "OK");
  std::printf("Jitter cleaner  output clock: %-3s(current)%s \n", jc_lol ? "ERR" : "OK", jc_sticky_lol ? ", loss of lock (sticky)" : "");
  std::printf("Jitter cleaner design ID: \"%s\"\n", designid.c_str());
  std::printf("Jitter cleaner interrupt: %s (sticky)\n", jc_intr ? "active" : "inactive");
  
  std::printf("%s", jc_hold ? "\nWarning: jitter cleaner is in holdover mode!\n" : "");
  
  std::printf("\n");
  std::printf("(read from the VME)\n");
  std::printf("+-----------------------------------------------------------+\n");
  std::printf("| %-20s || %-7s | %-12s | %-8s |\n", "PLL input monitoring", "Signal", "Frequency", "Glitches");
  std::printf("+-----------------------------------------------------------+\n");
  for (i = 0; i < AltiModule::EXT_CLK_SOURCE_NUMBER; i++) {
    src = (AltiModule::EXT_CLK_SOURCE) i;
    std::printf("| %-20s || %-7s | %-12s | %-8s | (sticky)\n", AltiModule::EXT_CLK_SOURCE_NAME[src].c_str(), stopped[i] ? "stopped" : "OK", oor[i] ? "out of range" : "OK", glitch[i] ? "glitch" : "OK");
    std::printf("+-----------------------------------------------------------+\n");
  }
  
  std::string jc_input_name;
  std::printf("\n");
  std::printf("(read from the I2C)\n");
  std::printf("+---------------------------------------------------------------------------------------+\n");
  std::printf("| %-31s || %-21s | %-26s |\n", "Jitter cleaner input monitoring", "Signal", "Frequency");
  std::printf("+---------------------------------------------------------------------------------------+\n");
  for (i = 0; i < 2; i++) {
    switch (i) {
    case 0:
      jc_input_name = "from oscillator";
      break;
    case 1:
      jc_input_name = "from switch";
      break;
    default:
      break;
    }
    std::printf("| %-31s || %-3s%-18s | %-3s%-23s | \n", jc_input_name.c_str(), v_jc_los[i] ? "ERR" : "OK", v_jc_sticky_los[i] ? ", stopped (sticky)" : "", v_jc_oof[i] ? "ERR" : "OK", v_jc_sticky_oof[i] ? ", out of range (sticky)" : "");
    std::printf("+---------------------------------------------------------------------------------------+\n");
  } 
  
  if(lol) {
    string reason = "No signal from the selected input clock";
    ers::error(ALTI::AltiController::ClockLoss(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  
  return status;
}

//========================================================================================================================
 
int AltiController::writeConfiguration(){
  int status = 0;
  // write configuration in HW
  if((status |= m_alti->AltiConfigWrite(*m_alti_config, cout)) !=  AltiModule::SUCCESS) {
    string reason = "during configuration with code = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  return status;
}


//========================================================================================================================
 
int AltiController::readCounters(bool print_log) {
  
  int status = 0;
  
  // copy and clear counter
  if((status |= m_alti->CNTCopyClearWrite()) !=  AltiModule::SUCCESS) {
    string reason = " during CNTCopyClearWrite with code = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  if((status |= m_alti->CNTCopyClearWait()) !=  AltiModule::SUCCESS) {
    string reason = " during CNTCopyClearWait with code = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }

  // read number of turns
  u_int turn = 0;
  if((status |= m_alti->CNTTurnCountRead(turn)) !=  AltiModule::SUCCESS) {
    string reason = " during CNTTurnCountRead with code = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }

  // read counters
  AltiCounter counter(m_cmem.get());
  if((status |= m_alti->CNTCounterRead(&counter)) !=  AltiModule::SUCCESS) {
    string reason = " during CNTCounterRead with code = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  //counter.dump();
  
  // stop the pattern if needed
  if(m_max_l1a > 0) {
    m_nb_l1a += counter.counter(9);
    if(m_nb_l1a >= m_max_l1a) {
      // stop pattern generator
      ERS_LOG(m_name << " stop pattern generator after " << m_nb_l1a << " L1A from the pattern generator");
      if ((status |= m_alti->PRMEnableWrite(false)) != AltiModule::SUCCESS) {
	string reason = " during PATGenerationEnableWrite with code = " + to_string(status);
	ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
      }
      m_nb_l1a = 0;
    }
  }
  
  AltiCounterRate rate(counter);  
  
  // read orbit counters
  u_int cnt, cnt_short, cnt_long;
  if((status |= m_alti->CNTOrbitRead(cnt, cnt_short, cnt_long)) !=  AltiModule::SUCCESS) {
    string reason = " during CNTOrbitRead with code = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  u_int min, max;
  if((status |= m_alti->CNTOrbitLengthRead(min, max)) !=  AltiModule::SUCCESS) {
    string reason = " during CNTOrbitLengthRead with code = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }

  // update IS
  for(u_int cnt=0; cnt<AltiModule::CNT_SIZE; cnt++) {
    m_is_alti->rate_counters[cnt].value = counter.counter(cnt);
    if(cnt<(AltiModule::CNT_SIZE-1)) m_is_alti->rate_counters[cnt].rate = rate.counter(cnt)*11.2455;
    else m_is_alti->rate_counters[cnt].rate = rate.counter(cnt);
    m_is_alti->rate_counters[cnt].nb_orbits = counter.turn();
    m_is_alti->rate_counters[cnt].overflow = counter.overflow();
  }
  
  m_is_alti->orbit_counters.nb_good = cnt;
  m_is_alti->orbit_counters.nb_short = cnt_short;
  m_is_alti->orbit_counters.nb_long = cnt_long;
  m_is_alti->orbit_counters.min = min;
  m_is_alti->orbit_counters.max = max;
  
  return status;
} 

//========================================================================================================================
 
int AltiController::readMinictpCounters(bool print_log) {
  
  int status = 0;
  
  // copy and clear counter
  if((status |= m_alti->CTPCntCopyClearWrite()) !=  AltiModule::SUCCESS) {
    string reason = " during CTPCntCopyClearWrite with code = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  if((status |= m_alti->CTPCntCopyClearWait()) !=  AltiModule::SUCCESS) {
    string reason = " during CTPCntCopyClearWait with code = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }

  // read number of turns
  u_int turn = 0;
  if((status |= m_alti->CTPCntTurnCountRead(turn)) !=  AltiModule::SUCCESS) {
    string reason = " during CTPCntTurnCountRead with code = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }

  // read L1A counters
  u_int cnt_l1a;
  if((status |= m_alti->CTPCntL1aCounterRead(cnt_l1a)) !=  AltiModule::SUCCESS) {
    string reason = " during CTPCntL1aCounterRead with code = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  
  // read counters
  u_int cnt_tbp, cnt_tap, cnt_tav;
  for(u_int itm = 0; itm < AltiModule::ITEM_NUMBER; itm++) {
    // Trigger before prescale
    if((status |= m_alti->CTPCntTbpCounterRead(itm, cnt_tbp)) !=  AltiModule::SUCCESS) {
      string reason = " during CTPCntTbpCounterRead with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    m_is_alti->minictp_counters[itm*AltiModule::MINICTP_CNT_NUMBER].value = cnt_tbp;
    m_is_alti->minictp_counters[itm*AltiModule::MINICTP_CNT_NUMBER].rate = ((float)cnt_tbp / (float)turn)*11.2455;
    m_is_alti->minictp_counters[itm*AltiModule::MINICTP_CNT_NUMBER].nb_orbits = turn;
    m_is_alti->minictp_counters[itm*AltiModule::MINICTP_CNT_NUMBER].overflow = false;
   
    // Trigger after prescale
    if((status |= m_alti->CTPCntTapCounterRead(itm, cnt_tap)) !=  AltiModule::SUCCESS) {
      string reason = " during CTPCntTapCounterRead with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    m_is_alti->minictp_counters[itm*AltiModule::MINICTP_CNT_NUMBER + 1].value = cnt_tap;
    m_is_alti->minictp_counters[itm*AltiModule::MINICTP_CNT_NUMBER + 1].rate = ((float)cnt_tap / (float)turn)*11.2455;
    m_is_alti->minictp_counters[itm*AltiModule::MINICTP_CNT_NUMBER + 1].nb_orbits = turn;
    m_is_alti->minictp_counters[itm*AltiModule::MINICTP_CNT_NUMBER + 1].overflow = false;

    // Trigger after veto
    if((status |= m_alti->CTPCntTavCounterRead(itm, cnt_tav)) !=  AltiModule::SUCCESS) {
      string reason = " during CTPCntTavCounterRead with code = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }
    m_is_alti->minictp_counters[itm*AltiModule::MINICTP_CNT_NUMBER + 2].value = cnt_tav;
    m_is_alti->minictp_counters[itm*AltiModule::MINICTP_CNT_NUMBER + 2].rate = ((float)cnt_tav / (float)turn)*11.2455;
    m_is_alti->minictp_counters[itm*AltiModule::MINICTP_CNT_NUMBER + 2].nb_orbits = turn;
    m_is_alti->minictp_counters[itm*AltiModule::MINICTP_CNT_NUMBER + 2].overflow = false;     
  }

  // L1A
  m_is_alti->minictp_counters[AltiModule::MINICTP_CNT_SIZE_TOTAL - 1].value = cnt_l1a;
  m_is_alti->minictp_counters[AltiModule::MINICTP_CNT_SIZE_TOTAL - 1].rate = ((float)cnt_l1a / (float)turn)*11.2455;
  m_is_alti->minictp_counters[AltiModule::MINICTP_CNT_SIZE_TOTAL - 1].nb_orbits = turn;
  m_is_alti->minictp_counters[AltiModule::MINICTP_CNT_SIZE_TOTAL - 1].overflow = false;
  
  return status;
} 

//========================================================================================================================

int AltiController::configureBusyMonitoring() {

  int status = 0; 
  
  // change to manual writing to avoid accidental writes during configuration
  if ((status |= m_alti->BSYMonitoringControlWrite(false, false)) != AltiModule::SUCCESS) {
    string reason = "VME error during BSYMonitoringControlWrite(): " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }

  // define what is written to fifo: everyting except INTERVAL (as is implicitly given when running with automatic writing)
  m_busy_counter.selectAll(true);
  m_busy_counter.select(AltiBusyCounter::INTERVAL, false);

  // write counter
  if ((status |= m_alti->BSYSelectWrite(m_busy_counter)) != AltiModule::SUCCESS) {
    string reason = "VME error during BSYSelectWrite(): " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  
  // a write interval of 10000000 should give us ~ 30 s to readout the Fifo  before it is full
  const size_t interval = 10000000;
  cout << m_uid << " Setting interval for BSY FIFO to " << interval << " "
       << "~" << (float(interval) / 40000000 /* MHz*/) << " s "
       << endl;
  // write BSY interval
  if ((status |= m_alti->BSYIntervalWrite(interval)) != AltiModule::SUCCESS) {
    string reason = "VME error during BSYIntervalWrite(): " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason)); 
  }
  m_busy_counter.interval(interval);
  
  // consider the fifo almost full at ~ 80 %
  const size_t watermark = AltiModule::BSY_FIFO_SIZE * 0.8; 
  cout << m_uid << "Setting BSY FIFO water mark to "
       << watermark << " of " << AltiModule::BSY_FIFO_SIZE
       << endl;
  // write BSY FIFO watermask
  if ((status |= m_alti->BSYFifoWatermarkWrite(watermark)) != AltiModule::SUCCESS) {
    string reason = "VME error during BSYFifoWatermarkWrite(): " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
    }

  // change back to automatic writing to start filling fifo
  if ((status |= m_alti->BSYMonitoringControlWrite(true, false)) != AltiModule::SUCCESS) {
    string reason = "VME error during BSYControlWrite(): " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }

  // clear fifo to avoid data corruption
  if ((status |= m_alti->BSYFifoReset()) != AltiModule::SUCCESS) {
    string reason = "VME error during BSYFifoReset(): " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }

  // clear counters
  if ((status |= m_alti->BSYCounterReset()) != AltiModule::SUCCESS) {
    string reason = "VME error during BSYCounterReset(): " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }
  
  return status;
}


//========================================================================================================================

int AltiController::readBusyMonitoring() {

  int status = 0; 
  
  // Busy counters
  if ((status |= m_alti->BSYSelectRead(m_busy_counter)) != AltiModule::SUCCESS) {
    string reason = "VME error during BSYSelectRead(): " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  }   
  if ((status |= m_alti->BSYFifoRead(m_busy_counter)) != AltiModule::SUCCESS) {
    string reason = "VME error during BSYFifoRead(): " + to_string(status); 
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, ROS::IOMPlugin::m_uid, reason));
  } 


  // Counts to rates
  AltiBusyCounterRate busy_rate = m_busy_counter;
  busy_rate.average(); 
  
  // update IS
  bool enabled = false;
  for(u_int cnt=0; cnt < AltiBusyCounter::BUSY_NUMBER - 1; cnt++) {
    m_is_alti->busy_counters[cnt].fraction = busy_rate[cnt];
    m_is_alti->busy_counters[cnt].value    = m_busy_counter[cnt];
    m_is_alti->busy_counters[cnt].interval = m_busy_counter.interval();
    m_is_alti->busy_counters[cnt].overflow = m_busy_counter.overflow();  
    switch (AltiBusyCounter::BUSY(cnt)) {
    case AltiBusyCounter::BUSY::CTP:
      enabled = m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_CTP];
      break;
    case AltiBusyCounter::BUSY::ALTI:
      enabled = m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_ALTI];
      break;
    case AltiBusyCounter::BUSY::FP:
      enabled = m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_FRONT_PANEL];
      break;
    case AltiBusyCounter::BUSY::LOCAL:
      enabled = true;
      break;
    case AltiBusyCounter::BUSY::PG:
      enabled = m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_PG];
      break;
    case AltiBusyCounter::BUSY::VME:
      enabled = m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_VME];
      break;
    case AltiBusyCounter::BUSY::ECR:
      enabled = m_alti_config->bsy_in_select[AltiModule::BUSY_INPUT::BUSY_FROM_ECR];
      break;
    default:
      break;
    }
    m_is_alti->busy_counters[cnt].enabled  = enabled;
  }
 
  return status;
}

//========================================================================================================================

void AltiController::configureMasterTrigger(const ALTIDAL::AltiMasterTriggerConfig* config) {

  ERS_LOG(m_name << " Entered");
 config->print(1,true,std::cout);

 // ECR configuration from OKS
 m_ecr_periodic = config->get_ECR_periodic();
 uint32_t ecr_period = config->get_ECR_period();
 uint32_t ecr_length = config->get_ECR_length();
 uint32_t ecr_pre_deadtime = config->get_ECR_pre_deadtime();
 uint32_t ecr_post_deadtime = config->get_ECR_post_deadtime();
 
 // LB configuration from OKS
 m_lumiblock_period = config->get_LB_period();
 m_lumiblock_min_distance = config->get_LB_minimum_distance();

 ERS_LOG(m_name << " creating AltiMasterBusy object");
 m_master_busy = new AltiMasterBusy(*m_alti, m_uid, m_ipc_partition);
 
 ERS_LOG(m_name << " creating AltiMasterEcr object");
 m_master_ecr = new AltiMasterEcr(*m_alti, m_uid, m_ipc_partition);

 // set period, length and pre/post deadtime for periodic ECR
 m_master_ecr->setPeriod(ecr_period);
 m_master_ecr->setLength(ecr_length);
 m_master_ecr->setDeadtime(ecr_pre_deadtime, ecr_post_deadtime);
 // do not wite it in the configuration to not start it too early

 // TTC2LAN configuration
 m_use_ttc2lan = config->get_TTC2LAN_activate();
 u_int period = 10; // default 10ms
 if(m_use_ttc2lan) {

   string frequency = config->get_TTC2LAN_poll_frequency();
   // convert poll frequency to a period in ms
   // frequency Hz:     10Hz, 100Hz, 1kHz
   //           ms:     100,    10,    1
   if (frequency == ALTIDAL::AltiMasterTriggerConfig::TTC2LAN_poll_frequency::_10Hz) {
     period = 100;
     ERS_LOG(m_name << " set TTCL2LAM frequency at 10 Hz");
   }
   else if (frequency == ALTIDAL::AltiMasterTriggerConfig::TTC2LAN_poll_frequency::_100Hz) {
     period = 10;
     ERS_LOG(m_name << " set TTCL2LAM frequency at 100 Hz");
   }
   else if (frequency == ALTIDAL::AltiMasterTriggerConfig::TTC2LAN_poll_frequency::_1kHz) {
     period = 1;
     ERS_LOG(m_name << " set TTCL2LAM frequency at 1 kHz");
   }
   else {
     ERS_LOG(m_name << " invalid Range for TTC2LAN poll frequency " << frequency << ". Using default 100 Hz");
   }
 }  
 ERS_LOG(m_name << " creating AltiMasterTTC2LAN object");
 m_master_ttc2lan = new AltiMasterTTC2LAN(*m_alti, m_uid, *m_master_busy, m_ipc_partition, period);
 
 ERS_LOG(m_name << " creating AltiMasterLumiBlock object");
 m_master_lumiblock = new AltiMasterLumiBlock(m_uid, *m_master_busy, *m_master_ttc2lan, m_ipc_partition);
 m_master_lumiblock->setPeriod(m_lumiblock_period*1000);
 m_master_lumiblock->setMinimumDistance(m_lumiblock_min_distance*1000);
 m_master_lumiblock->disableLumiBlocks();
 m_master_lumiblock->disablePeriodicLumiBlocks();
 
 // reset all busy channels
 // not that the reset will set the trigger busy, which will be unset during prepareForRun,
 // as well as the runcontrol busy, which will be unset with the first resume command
 ERS_LOG(m_name << " calling m_master_busy->reset()");
 m_master_busy->reset();
 m_master_busy->dump();
 
 ERS_LOG(m_name << " Done");
}

//========================================================================================================================

// MasterTrigger methods

daq::trigger::HoldTriggerInfo AltiController::hold(const std::string& dm) {
  ERS_LOG(m_name << " Entered with argument " << dm);

  m_master_busy->dump();
  ERS_LOG(m_name << " set Run Control Busy");
  m_master_busy->setRunControlBusy();
  m_master_busy->dump();

  uint32_t nb_ecr = 0;
  if(m_ecr_periodic) nb_ecr = m_master_ecr->stopEcr();
  m_master_ecr->publishToIS();
  ERS_LOG(m_name << " number of ECR generated = 0x" << std::hex << nb_ecr << std::dec << " = " << nb_ecr);
  
  m_master_ttc2lan->readL1ID();
  uint32_t l1id = m_master_ttc2lan->getLastL1ID();
  ERS_LOG(m_name << " last L1ID = 0x" << std::hex << l1id << std::dec);
 
  daq::trigger::HoldTriggerInfo trg_info;
  trg_info.ecrCounter = nb_ecr;
  trg_info.lastValidEL1ID = l1id;

  return trg_info;
}

//========================================================================================================================

void AltiController::resume(const std::string& dm) {
  ERS_LOG(m_name << " Entered  with argument " << dm);
  
  
  if (m_ecr_periodic) m_master_ecr->startEcr();
  m_master_ecr->publishToIS();
  
  ERS_LOG(m_name << " unset RunControl Busy");
  m_master_busy->unsetRunControlBusy();
  
  m_master_busy->dump();

  ERS_LOG(m_name << " Done");
}

//========================================================================================================================

void AltiController::setPrescales(uint32_t l1p, uint32_t hltp) {
  ERS_LOG(m_name << " called with l1p = " << l1p << ", hltp = " << hltp << ". Nothing to be done for ALTI.");
}

//========================================================================================================================

void AltiController::setL1Prescales(uint32_t l1p) {
   ERS_LOG(m_name << " called with l1p = " << l1p << ". Nothing to be done for ALTI.");
}

//========================================================================================================================

void AltiController::setHLTPrescales(uint32_t hltp) {
   ERS_LOG(m_name << " called with hltp = " << hltp << ". Nothing to be done for ALTI.");
}

//========================================================================================================================

void AltiController::increaseLumiBlock(uint32_t run_number) {
  ERS_LOG(m_name << " called with run number = " << run_number);
  try {
    m_master_lumiblock->requestIncreaseLBN();
  } catch(ALTI::AltiController::FailedIncreaseLBN& ex) {
    ERS_LOG(m_name << " could not increaseLumiBlock. Throwing exception ::TRIGGER::CommandExecutionFailed");
    throw ::TRIGGER::CommandExecutionFailed(ex.what());
  }
  ERS_LOG(m_name << " Done");
}

//========================================================================================================================

void AltiController::setLumiBlockInterval(uint32_t seconds) {
  ERS_LOG(m_name << " called with interval = " << seconds << " seconds");
  m_master_lumiblock->setPeriod(seconds*1000);
  ERS_LOG(m_name << " Done");
}

//========================================================================================================================

void AltiController::setMinLumiBlockLength(uint32_t seconds) {
  ERS_LOG(m_name << " called with length = " << seconds << " seconds");
  ERS_LOG(m_name << " Done");
}

//========================================================================================================================

void AltiController::setPrescalesAndBunchgroup(uint32_t l1p, uint32_t hltp, uint32_t bg) {
  ERS_LOG(m_name << " called with l1p = " << l1p << ", hltp = " << hltp << ". bg = " << bg << ", Nothing to be done for ALTI.");
}

//========================================================================================================================

void AltiController::setBunchGroup(uint32_t bg) {
  ERS_LOG(m_name << " called with bg = " << bg << ". Nothing to be done for ALTI.");
}

//========================================================================================================================

void AltiController::setConditionsUpdate(uint32_t folder_index, uint32_t lb) {
  ERS_LOG(m_name << " called with folderIndex = " << folder_index << ", lb = " << lb << ". Nothing to be done for ALTI.");
}


//========================================================================================================================

// Following lines needed for dynamic loading (i.e., plugin way)

extern "C"
{
  extern ROS::ReadoutModule* createAltiBoard();
}

ROS::ReadoutModule* createAltiBoard()
{
  return (new AltiController());
}

