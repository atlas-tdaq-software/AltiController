#include "AltiController/Exceptions.h"
#include "AltiController/AltiMasterLumiBlock.h"

#include <ctime>

// IS
#include "ers/ers.h"
#include "is/info.h"
#include "is/infodictionary.h"
#include "TTCInfo/LumiBlock.h"
#include "TTCInfo/LBSettings.h"

using namespace std;
using namespace LVL1;

AltiMasterLumiBlock::AltiMasterLumiBlock(string uid, AltiMasterBusy& master_busy, AltiMasterTTC2LAN& master_ttc2lan, IPCPartition& p) : 
  ScheduledUserAction(250), // 0.25 sec hardcoded
  m_uid(uid),
  m_master_busy(&master_busy),
  m_master_ttc2lan(&master_ttc2lan),
  m_ipc_partition(p)
{ 
  ERS_LOG(m_uid << " Entered");
  
  ERS_LOG(m_uid << " Done");
}

//========================================================================================================================

AltiMasterLumiBlock::~AltiMasterLumiBlock() {
   ERS_LOG(m_uid << " Entered");
  
  ERS_LOG(m_uid << " Done");
}

//========================================================================================================================

void AltiMasterLumiBlock::requestIncreaseLBN() 
{
  uint64_t now = this->getTimeNow();
  
  boost::mutex::scoped_lock scoped_lock(m_mutex);

  // take requests only when lumi blocks are enabled, i.e. during a run
  if (m_lumiblocks_enabled) {
    if (now >= m_veto_lb_update) {
      this->increaseLBN();
      now = this->getTimeNow();
      m_veto_lb_update = now+m_lb_min_distance;
      m_next_lb_update_is_a_request = false;
      m_next_lb_update = now+m_lb_period;
    } else {
      ERS_LOG(m_uid << " Received LB request at " << now << ", waiting until " << m_veto_lb_update);
      m_next_lb_update_is_a_request = true;
      m_next_lb_update=m_veto_lb_update;
    }
  } else {
    std::ostringstream text;
    text << "AltiMasterLumiBlock::requestIncreaseLBN(): failed to increase LBN";
    throw ALTI::AltiController::FailedIncreaseLBN(ERS_HERE, text.str() , "");
  }
  
}

//========================================================================================================================

void AltiMasterLumiBlock::getCurrentLBN(uint32_t& lbn, uint64_t& timestamp)
{
  m_mutex.lock();
  lbn = m_lbn;
  timestamp = m_current_lb_start_time;
  m_mutex.unlock();
}

//========================================================================================================================

void AltiMasterLumiBlock::setPeriod(uint32_t period_ms)
{
 
  m_mutex.lock();
  m_lb_period = static_cast<uint64_t>(period_ms)*1000000ull;
 
  // publish settings to IS
  try {
    this->publishLBSettingsToIS();
  } catch (ALTI::AltiController::PublishToIS& ex) {
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, "could not publish to IS: ", ex.what()));
  }
  m_mutex.unlock();
}

//========================================================================================================================

void AltiMasterLumiBlock::setMinimumDistance(uint32_t distance_ms)
{

  m_mutex.lock();
  m_lb_min_distance = static_cast<uint64_t>(distance_ms)*1000000ull;
  
  // publish settings to IS
  try {
    this->publishLBSettingsToIS();
  } catch (ALTI::AltiController::PublishToIS& ex) {
    std::ostringstream text;
    text << "Could not publish to IS: AltiModulePublishToIS ex.what()=" << ex.what();
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  }
  
  m_mutex.unlock();

}

//========================================================================================================================

void AltiMasterLumiBlock::enablePeriodicLumiBlocks()
{
  m_mutex.lock();
  m_periodic_lumiblocks_enabled = true;
  m_mutex.unlock();
}

//========================================================================================================================

void AltiMasterLumiBlock::disablePeriodicLumiBlocks()
{
  m_mutex.lock();
  m_periodic_lumiblocks_enabled = false;
  m_mutex.unlock();
}

//========================================================================================================================

void AltiMasterLumiBlock::enableLumiBlocks()
{
  m_mutex.lock();
  m_lumiblocks_enabled = true;
  m_mutex.unlock();
}

//========================================================================================================================

void AltiMasterLumiBlock::disableLumiBlocks()
{
  m_mutex.lock();
  m_lumiblocks_enabled = false;
  m_mutex.unlock();
}

//========================================================================================================================

void AltiMasterLumiBlock::reset(uint32_t run_number) {
  
  ERS_LOG(m_uid << " called with argument run_number = " << run_number);
  // The very first lumiblock with LBN=0 is published by the RootController
  // at the start of run. We read this from IS and feed the run number, start time,
  // and startlbn into the masterlumiblock action
  // we also perform a few cross-checks

  // read from IS
  // there is one pitfall here: the RootController publishes without history.
  // if there is a program which publishes with history once, then
  // we'll always poll this value (with tag>0) rather than the one
  // from the RootController. To safe-guard against this, we read explicitely
  // the tag 0
  LumiBlock is_entry;
  ISInfoDictionary dict(m_ipc_partition);
  try {
    dict.getValue("RunParams.LumiBlock", 0, is_entry);
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(m_uid << " Could not retrieve RunParams.LumiBlock publication: CORBA::SystemException ex._name()=" << ex._name());
    std::ostringstream text;
    text << "could not retrieve RunParams.LumiBlock publication for LBN=0. ";
    throw ALTI::AltiController::FailedFirstLumiBlock(ERS_HERE, m_uid, text.str() , ex._name());
  } catch (daq::is::Exception & ex) {
    ERS_LOG(m_uid << " Could not retrieve RunParams.LumiBlock publication: daq::is::Exception & ex ex._name()=" << ex.what());
    std::ostringstream text;
    text << "could not retrieve RunParams.LumiBlock publication for LBN=0. ";
    throw ALTI::AltiController::FailedFirstLumiBlock(ERS_HERE, m_uid, text.str(), ex.what());
  } catch (std::exception& ex) { 
    ERS_LOG(m_uid << " Could not retrieve RunParams.LumiBlock publication: std::exception& ex ex._name()=" << ex.what());
    std::ostringstream text;
    text << "could not retrieve RunParams.LumiBlock publication for LBN=0. ";
    throw ALTI::AltiController::FailedFirstLumiBlock(ERS_HERE, m_uid, text.str(), ex.what());
  } catch (...) {
    ERS_LOG(m_uid << " Could not retrieve RunParams.LumiBlock publication: unknown exception");
    std::ostringstream text;
    text << "could not retrieve RunParams.LumiBlock publication for LBN=0. ";
    throw ALTI::AltiController::FailedFirstLumiBlock(ERS_HERE, m_uid, text.str(), "Unknown Exception");
  }

  // is_entry.RunNumber = m_runnumber;
  uint32_t start_run_number = is_entry.RunNumber;
  uint32_t start_lbn = is_entry.LumiBlockNumber;
  uint64_t start_of_run_time = is_entry.Time;
  ERS_LOG(m_uid << " Retrieved from IS (RunParams.LumiBlock): RunNumber = " << start_run_number);
  ERS_LOG(m_uid << " Retrieved from IS (RunParams.LumiBlock): LumiBlockNumber = " << start_lbn);
  ERS_LOG(m_uid << " Retrieved from IS (RunParams.LumiBlock): Time = " << start_of_run_time);


  // take those values as starting point ... 
  m_mutex.lock();
  m_current_lb_start_time = start_of_run_time;
  m_lb_change_in_progress = false;
  m_next_lb_update = start_of_run_time+m_lb_period;
  m_next_lb_update_is_a_request = false;
  m_veto_lb_update = start_of_run_time+m_lb_min_distance;
  m_lbn = start_lbn;
  m_run_number = start_run_number;
  m_periodic_lumiblocks_enabled = false;
  m_lumiblocks_enabled = false;
  m_nl1a_last = 0;
  m_mutex.unlock();

  // make a few consistency checks
  if (start_run_number != run_number) {
    std::ostringstream text;
    text << "Retrieved inconsistent run number from RunParams.LumiBlock. Run number from LBN=0 " << start_run_number << ", inconsistent with expectation from RunParams " << run_number;
    throw ALTI::AltiController::FailedFirstLumiBlock(ERS_HERE, m_uid, text.str(), "");
  }
  if (start_lbn != 0 ) {
    std::ostringstream text;
    text << "Retrieved inconsistent start LBN from RunParams.LumiBlock. Got LBN = " << start_lbn << ", inconsistent with expectation 0";
    throw ALTI::AltiController::FailedFirstLumiBlock(ERS_HERE, m_uid, text.str(), "");
  }
  uint64_t now = this->getTimeNow();
  // make comparison of time stamps
  int64_t diff = static_cast<int64_t>(start_of_run_time)-static_cast<int64_t>(now);
  ERS_LOG(m_uid << " Time difference between timestamps SOR minus now = " <<  diff << " nanoseconds");

  return;
}

//========================================================================================================================

void AltiMasterLumiBlock::increaseLBN() 
{
  ERS_LOG(m_uid << " Entered");
  m_lb_change_in_progress = true;

  // record start time
  uint64_t start_time = this->getTimeNow();

  // set busy
  m_master_busy->setLumiBlockBusy();
  
  // wait until the busy is really raised
  while(!m_master_busy->isLumiBlockBusy()) {}
  
  // increase LBN
  ++m_lbn;
  // record timestamp
  m_current_lb_start_time = this->getTimeNow();
  // send out IS information
  this->publishLBToIS();

  ERS_LOG(m_uid << " Issuing new lumiblock with RN=" << m_run_number << ", LBN=" << m_lbn << " and Time=" << m_current_lb_start_time);

  // release busy
  m_master_busy->unsetLumiBlockBusy();

  // record end time
  uint64_t stop_time = this->getTimeNow();
  // make a log entry on timing performance
  ERS_LOG(m_uid << " Transition Benchmark for RN=" << m_run_number << ", LBN=" << m_lbn << " time=" << (stop_time - start_time)/1000000. << " ms");

  m_lb_change_in_progress = false;
}

//========================================================================================================================

uint64_t AltiMasterLumiBlock::getTimeNow() {
  struct timeval tv;
  gettimeofday(&tv, nullptr);
  return(static_cast<uint64_t>(tv.tv_sec)*1000000000ull + static_cast<uint64_t>(tv.tv_usec*1000));
}

//========================================================================================================================

uint32_t AltiMasterLumiBlock::getRunNumber() {
  uint32_t rn(0);
  m_mutex.lock();
  rn = m_run_number;
  m_mutex.unlock();
  return(rn);
}

//========================================================================================================================

void AltiMasterLumiBlock::publishLBToIS() {

  // this method should only be called by other methods locking m_mutex
  // this means also, that this method should not use m_mutex nor call
  // other methods that use m_mutex

  try {

    LumiBlock is_entry;
    is_entry.RunNumber = m_run_number;
    is_entry.LumiBlockNumber = m_lbn;
    is_entry.Time = m_current_lb_start_time;
    
    ISInfoDictionary dict(m_ipc_partition);
    dict.checkin("RunParams.LumiBlock", is_entry, false);
  
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(m_uid << " Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());
    throw ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, "AltiMasterLumiBlock::publishLBSettingsToIS() ", ex._name());
  } catch (daq::is::Exception & ex) {
    ERS_LOG(m_uid << " Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
    throw ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, "AltiMasterLumiBlock::publishLBSettingsToIS() ", ex.what());
  } catch (std::exception& ex) {
    ERS_LOG(m_uid << " Could not publish to IS: std::exception ex.what()=" << ex.what());
    throw ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, "AltiMasterLumiBlock::publishLBSettingsToIS() ", ex.what());
  } catch (...) {
    ERS_LOG(m_uid << " Could not publish to IS. Unknown exception. Rethrowing.");
    throw ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, "AltiMasterLumiBlock::publishLBSettingsToIS() ", "Unknown Exception");
  }
}

//========================================================================================================================

void AltiMasterLumiBlock::publishLBSettingsToIS() {

  // this method should only be called by other methods locking m_mutex
  // this means also, that this method should not use m_mutex nor call
  // other methods that use m_mutex

  try {
    LBSettings is_entry;
    is_entry.SwitchingMode = LBSettings::TIME;
    is_entry.Interval =  static_cast<u_int>(m_lb_period/1000000000ull);
    is_entry.MinInterval =  static_cast<u_int>(m_lb_min_distance/1000000000ull);
    
    ISInfoDictionary dict(m_ipc_partition);
    dict.checkin("RunParams.LBSettings", is_entry, true);
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(m_uid << " Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());
    throw ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, "AltiMasterLumiBlock::publishLBSettingsToIS() ", ex._name());
  } catch (daq::is::Exception & ex) {
    ERS_LOG(m_uid << " Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
    throw ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, "AltiMasterLumiBlock::publishLBSettingsToIS() ", ex.what());
  } catch (std::exception& ex) {
    ERS_LOG(m_uid << " Could not publish to IS: std::exception ex.what()=" << ex.what());
    throw ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, "AltiMasterLumiBlock::publishLBSettingsToIS() ", ex.what());
  } catch (...) {
    ERS_LOG(m_uid << " Could not publish to IS. Unknown exception. Rethrowing.");
    throw ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, "AltiMasterLumiBlock::publishLBSettingsToIS() ", "Unknown Exception");
  }
}

//========================================================================================================================


void AltiMasterLumiBlock::reactTo() {

  boost::mutex::scoped_lock scoped_lock(m_mutex);

  // increase LB only if it was a specific request
  // or we are in periodic lumiblock mode and it is time
  if (m_periodic_lumiblocks_enabled || m_next_lb_update_is_a_request) {
    uint64_t now = this->getTimeNow();
    if ((now>=m_next_lb_update) && (now>=m_veto_lb_update)) {
      // only increase LBN in case LumiBlocks are enabled
      if (m_lumiblocks_enabled)	this->increaseLBN();
  
      uint64_t now = this->getTimeNow();
      m_next_lb_update = now+m_lb_period;
      m_next_lb_update_is_a_request = false;
      m_veto_lb_update = now+m_lb_min_distance; 
    }
  }
}
