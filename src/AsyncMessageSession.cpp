#include "AltiController/AsyncMessageSession.h"

using namespace ALTI;

void Session::onOpen() noexcept
{
  ERS_LOG("Session is open:" << remoteEndpoint());
  
  // initiate first receive
  asyncReceive();
}


std::unique_ptr<daq::asyncmsg::InputMessage> Session::createMessage(std::uint32_t typeId, std::uint32_t  transactionId,
                                                           std::uint32_t size) noexcept
{
  ERS_ASSERT_MSG(typeId == XONOFF::ID, "Unexpected type ID in message: " << typeId);
  ERS_ASSERT_MSG(size == sizeof(uint32_t), "Unexpected size: " << size);
  ERS_DEBUG(3,"createMessage, size = " << size);
  
  // all ok, create a new XONOFF object
  return std::unique_ptr<XONOFF>(new XONOFF());
}

//
// This is called when the full message has been received.
void Session::onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message)
{
  ERS_DEBUG(3,"onReceive");
  auto msg = std::unique_ptr<XONOFF>(dynamic_cast<XONOFF*>(message.release()));
  
  if(msg == 0) {
    ERS_LOG("Received invalid message type " << msg->typeId());
    return;
  }
  
  m_status = msg->status();
  if (m_status == XONOFFStatus::OFF){
    ERS_DEBUG(3, "Set CTP busy: ()");
    m_master_busy->setHLTSVBusy();
    m_hltsv_xoff_count++;
  } else {
    ERS_DEBUG(3, "Releasing CTP busy: ()");
    m_master_busy->unsetHLTSVBusy();
    m_hltsv_xon_count++;
  }
  ERS_DEBUG(1, "Got XONOFF message, new status = " << (m_status == XONOFFStatus::ON ? "ON" : "OFF"));
  
  // Initiate next receive
  asyncReceive();
}

void Session::onSendError(const boost::system::error_code& error,
                 std::unique_ptr<const daq::asyncmsg::OutputMessage> /* message */) noexcept
{
  ERS_LOG("Send error: " << error);
  abort(); 
}

