#include "AltiController/Exceptions.h"
#include "AltiController/AltiMasterTTC2LAN.h"

// DAQ software
#include "RunControl/Common/OnlineServices.h"

// IS
#include "is/info.h"
#include "is/infodictionary.h"
#include "TTCInfo/TTC2LAN_IS.h"

using namespace std;

//========================================================================================================================

AltiMasterTTC2LAN::AltiMasterTTC2LAN(LVL1::AltiModule& alti, string uid, AltiMasterBusy& master_busy, IPCPartition& p, u_int period) : 
  m_alti(&alti),
  m_uid(uid),
  m_master_busy(&master_busy),
  m_ipc_partition(p),
  m_last_l1id(0),
  m_current_l1id(0),
  m_event_count(0),
  m_hltsv_xon_count(0),
  m_hltsv_xoff_count(0),
  m_number_of_messages(0),
  m_hltsv_xon(true),
  m_is_connected(false),  
  m_status(ALTI::XONOFFStatus::ON),
  m_poll_l1id(false),
  m_period(period)
{ 
  ERS_LOG(m_uid << " Entered");
  
  m_last_l1id.reset();
 
  ERS_LOG(m_uid << " Done");
}


//========================================================================================================================

AltiMasterTTC2LAN::~AltiMasterTTC2LAN() {
  ERS_LOG(m_uid << " Entered");
  ERS_LOG(m_uid << " reset");
  this->reset();
  ERS_LOG(m_uid << " Done");
}

//========================================================================================================================

void AltiMasterTTC2LAN::reset() {
  boost::mutex::scoped_lock scoped_lock(m_mutex);


  m_last_l1id= LVL1::EventIdentifier();

  m_event_count = 0;
  m_hltsv_xon_count = 0;
  m_hltsv_xoff_count = 0;
  m_number_of_messages = 0;
  
  m_hltsv_xon = true;
}



//========================================================================================================================

bool AltiMasterTTC2LAN::connect() {

  ERS_LOG(m_uid << " setting up daq::asyncmesg connection with TTC2LANReceiver (in hltsv)");
  
  bool success = true;
  try {
    // Create the name service object to lookup the HLTSV address
    daq::asyncmsg::NameService name_service(m_ipc_partition, std::vector<std::string>());
    
    // Do the lookup, the HLTSV will publish its endpoint is IS: 'DF.MSG_TTC2LANReceiver'
    ERS_LOG(m_uid << " resolving address of TTC2LANReceiver");
    auto addr = name_service.resolve("TTC2LANReceiver");

    ERS_LOG(m_uid << " resetting worker");
    m_work.reset(new work(m_service.get_executor()));

    // Start the Boost.ASIO thread
    ERS_LOG(m_uid << " starting thread");
    m_io_thread = std::thread([&]() { ERS_LOG("io_service starting"); m_service.run(); ERS_LOG("io_service finished"); });
    
    // Create the session with the endpoint we just got
    ERS_LOG(m_uid << " creating session object");
    m_session = std::make_shared<ALTI::Session>(m_service, m_status, m_hltsv_xon_count, m_hltsv_xoff_count, m_master_busy);
    ERS_LOG(m_uid << " opening async message connection");
    m_session->asyncOpen(daq::rc::OnlineServices::instance().applicationName(), addr);
    
    // We should not return from this transition before the session is open;
    ERS_LOG("waiting until connection is open");
    while(m_session->state() != daq::asyncmsg::Session::State::OPEN) {
      usleep(10000);
    }
  } catch (std::exception &e) {
    ERS_LOG(m_uid << " CtpTTC2LAN::connect(): could not resolve addr of HLTSV: "<<e.what() << " TTC2LAN will be disabled");
    throw ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, " CtpTTC2LAN::connect() ", e.what());
    success =  false;
  }
  
  m_is_connected = true;

  ERS_LOG(m_uid << " done");
  return success;
}

//========================================================================================================================

void AltiMasterTTC2LAN::prepareForRun() {
  ERS_LOG(m_uid << " Enter");
  // start thread polling L1ID from ALTI hardware
  m_poll_l1id = true;
  m_read_l1id_thread = std::thread([&]() { ERS_LOG(m_uid << " readL1ID_thread starting"); pollL1ID(); ERS_LOG("readL1ID_thread finished");});
  ERS_LOG(m_uid << " Done");
}

//========================================================================================================================

void AltiMasterTTC2LAN::stopRecording() {
  ERS_LOG(m_uid << " Enter");
  // stop thread polling L1ID from ALTI hardware
  m_poll_l1id = false;
  m_read_l1id_thread.join();
  ERS_LOG(m_uid << " Done");

}

//========================================================================================================================

void AltiMasterTTC2LAN::disconnect() {
  ERS_LOG(m_uid << " Enter");

  // close communication channel with HLTSV via DAQ async messages  
  ERS_LOG(m_uid << " closing async message connection to HLTSV");
  m_session->asyncClose();
  
  // do not return from this transition before the session is closed
  while(m_session->state() != daq::asyncmsg::Session::State::CLOSED) {
    usleep(10000);
  }
  
  ERS_LOG(m_uid << " resetting m_service object");
  m_service.stop();
  ERS_LOG(m_uid << " joining io thread");
  m_io_thread.join();
  ERS_LOG(m_uid << " resetting m_work object");
  m_work.reset(); 

  ERS_LOG(m_uid << " Done");
}

//========================================================================================================================

void AltiMasterTTC2LAN::pollL1ID() {
 
  u_int counter = 0;
  
  while (m_poll_l1id) {
    counter++;
    std::this_thread::sleep_for(std::chrono::milliseconds(m_period)); 
    readL1ID();
    if (counter > 100){ // publish info to IS every ~1 sec
      this->publishToIS();
      counter = 0;
    }
  }
}

//========================================================================================================================

void AltiMasterTTC2LAN::readL1ID() {
 
  m_mutex.lock();
  
  // read extended L1ID from hardware
  std::vector<unsigned int> v_l1ids;
  
  m_alti->T2LFifoWrite();
  m_alti->T2LFifoRead(v_l1ids);
  
  for (u_int i : v_l1ids) {
    LVL1::EventIdentifier l1id = LVL1::EventIdentifier(i);
    
    // check if the event count changed. No update on ECR only:
    if (l1id.data() != m_last_l1id.data()  // nothing happened yet
	&& l1id.L1ID() != 0xffffff) {      // no event yet (we do not care about ECRs)
      
      // do some accounting
      m_current_l1id = l1id.data();
      if (l1id.ECRC() == m_last_l1id.ECRC()) {
	m_event_count += l1id.L1ID() - m_last_l1id.L1ID();
      }
      
      // send l1id to HLTSV
      m_last_l1id = l1id;
      if(m_is_connected) {
	this->sendMessage(l1id.data());
      }
    }
  }
  m_mutex.unlock();
  
}

//========================================================================================================================

uint32_t AltiMasterTTC2LAN::getLastL1ID() {
  
  uint32_t l1id;
  m_mutex.lock();
  l1id = m_last_l1id.data();
  m_mutex.unlock();

  return l1id;
}


//========================================================================================================================

void AltiMasterTTC2LAN::publishToIS() {
  boost::mutex::scoped_lock scoped_lock(m_mutex);
 
  try {
    TTC2LAN_IS is_entry;
    is_entry.L1A = m_event_count;
    is_entry.Last_L1ID = m_last_l1id;
    is_entry.TTC2LAN_enabled = true;
    is_entry.Number_of_messages = m_number_of_messages;
    is_entry.XON = (m_status == ALTI::XONOFFStatus::ON);
    is_entry.XON_count = m_hltsv_xon_count;
    is_entry.XOFF_count = m_hltsv_xoff_count;

    ISInfoDictionary dict(m_ipc_partition);
    dict.checkin("DF.TTC2LAN", is_entry, true);
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(m_uid << " Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, " publishToIS ", ex._name()));
  } catch (daq::is::Exception & ex) {
    ERS_LOG(m_uid << " Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, " publishToIS ", ex.what()));
  } catch (std::exception& ex) {
    ERS_LOG(m_uid << " Could not publish to IS: std::exception ex.what()=" << ex.what());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, " publishToIS ", ex.what()));
  } catch (...) {
    ERS_LOG(m_uid << " Could not publish to IS. Unknown exception. ");
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, " publishToIS ", "Unknown exception" ));
  }

}

//========================================================================================================================

void AltiMasterTTC2LAN::sendMessage(uint32_t l1id) {
  // this method is not allowed to use m_mutex nor methods that use m_mutex
  //char helper[30];
  //sprintf(helper,"0x%08x", l1id);
  //ERS_LOG(m_uid << " AltiMasterTTC2LAN::sendMessage called with L1ID = " << helper);
  
  std::unique_ptr<ALTI::Update> msg(new ALTI::Update(l1id));
  m_session->asyncSend(std::move(msg));
  m_number_of_messages++;
}

