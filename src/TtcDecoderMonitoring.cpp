#include "AltiController/TtcDecoderMonitoring.h"
#include "AltiController/Exceptions.h"

#include <iostream>
#include <string>
#include <fstream>

#include "ers/Issue.h"
 
#include "is/infoany.h"

using namespace LVL1;
using namespace std;

LVL1::AltiModule* TtcDecoderMonitoring::m_alti = nullptr; 
string TtcDecoderMonitoring::m_is_value = "";
u_int TtcDecoderMonitoring::m_is_index = 0;
string TtcDecoderMonitoring::m_uid = "";


TtcDecoderMonitoring::TtcDecoderMonitoring(LVL1::AltiModule& alti, string uid, const ALTIDAL::AltiTTCdecoder& ttc_decoder) :

  m_is_receiver(nullptr)
{
  ERS_LOG(uid << " Entered");
  
  u_int status = 0;
  m_alti = &alti;
  m_uid = uid;
  
  ERS_LOG(m_uid << " Reset TTC decoder");
  if ((status |= m_alti->DECReset() ) != AltiModule::SUCCESS) {
    string reason = " during DECReset with code = " + to_string(status) ;
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
  }
  ERS_LOG(m_uid << " Set TTC decoder in loop mode");
  if ((status |= m_alti->DECLoopWrite(true) ) != AltiModule::SUCCESS) {
    string reason = " during DECLoopWrite with code = " + to_string(status) ;
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
  }
  ERS_LOG(m_uid << " Enable TTC decoder");
  if ((status |= m_alti->DECEnableWrite(true) ) != AltiModule::SUCCESS) {
    string reason = " during DECEnableWrite with code = " + to_string(status) ;
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
  }
  
  // configure trigger condition
  if(ttc_decoder.get_trigger_condition()) {
    int L1A               = ttc_decoder.get_trig_L1A();
    int word              = ttc_decoder.get_trig_command_word();
    int word_mask         = LVL1::ALTI_TRM_TRIGGERWORDLS_BITSTRING::STAT_MASK_COMMAND;
    int ext_addr          = ttc_decoder.get_trig_command_external_address();
    int ext_addr_mask     = LVL1::ALTI_TRM_TRIGGERWORDLS_BITSTRING::STAT_MASK_ADDRESSLONG;
    int int_addr          = ttc_decoder.get_trig_command_internal_address();
    int int_addr_mask     = LVL1::ALTI_TRM_TRIGGERWORDLS_BITSTRING::STAT_MASK_ADDRESSLOCAL;
    int external          = ttc_decoder.get_trig_command_external();
    int any_addressed_cmd = ttc_decoder.get_trig_any_addressed_command();
    int any_broadcast_cmd = ttc_decoder.get_trig_any_broadcast_command();
    int single_bit_err    = ttc_decoder.get_trig_single_bit_error();
    int double_bit_err    = ttc_decoder.get_trig_double_bit_error();

    ERS_LOG(m_uid << " Configure trigger condition of TTC decoder");
    if ((status |= m_alti->DECTriggerWordWrite(true, word, word_mask, ext_addr, ext_addr_mask, int_addr, int_addr_mask, L1A, external, any_addressed_cmd, any_broadcast_cmd, single_bit_err, double_bit_err) ) != AltiModule::SUCCESS) {
      string reason = " during DECTriggerWordWrite with code = " + to_string(status) ;
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
    }
  }
  else {
    ERS_LOG(m_uid << " Configure TTC decoder in free-run mode");
    if ((status |= m_alti->DECTriggerWordWrite(false, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) ) != AltiModule::SUCCESS) {
      string reason = " during DECTriggerWordWrite with code = " + to_string(status) ;
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
    }
  }

  // stop of TTC decoder with IS callback 
  if(ttc_decoder.get_IS_stop_enabled()) {
    string is_partition = ttc_decoder.get_IS_partition();
    string is_name = ttc_decoder.get_IS_name();
    m_is_value = ttc_decoder.get_IS_value();
    m_is_index = ttc_decoder.get_IS_index();

    IPCPartition  ipc_partition(is_partition); 
    m_is_receiver = new (nothrow) ISInfoReceiver(ipc_partition);
    // Subscribe to IS update
    try {
      m_is_receiver->subscribe(is_name, IScallback);
    } 
    catch (daq::is::Exception& ex) {
      ERS_REPORT_IMPL( ers::warning, ers::Message, m_uid + " failed to subscribe to IS publications for TTC decoder monitoring, exception caught: " + ex.what(), );
    }
    catch (...) {
      ERS_REPORT_IMPL( ers::warning, ers::Message, m_uid + " failed to subscribe to IS publications for TTC decoder monitoring", );
    }         
  }
  
  ERS_LOG(m_uid << " Done");
  
}

//========================================================================================================================

TtcDecoderMonitoring::~TtcDecoderMonitoring() noexcept {
  ERS_LOG(m_uid<< " Entered");
  
  if (m_is_receiver != nullptr) {
    ERS_LOG(m_uid << " delete m_is_receiver");
    delete m_is_receiver;
    m_is_receiver = nullptr;
  }

  ERS_LOG(m_uid << " Done");
}


//========================================================================================================================

void TtcDecoderMonitoring::IScallback(ISCallbackInfo * isc) {
  
  u_int status = 0;
  
  string value_str = "";
  ISInfoAny value;
  isc->value(value); 
  for(u_int i=0;i<value.countAttributes();i++){ 
    auto type = value.getAttributeType() ;
    switch ( type ) {
    case ISType::Boolean:
      bool value_bool;
      value >> value_bool;
      value_str = to_string(value_bool);
      break;
    case ISType::S8:
      char value_char;
      value >> value_char;
      value_str = to_string(value_char);
      break;
    case ISType::U8:
      unsigned char value_uchar;
      value >> value_uchar;
      value_str = to_string(value_uchar);
      break;
    case ISType::S16:
      short value_short;
      value >> value_short;
      value_str = to_string(value_short);
      break;
    case ISType::U16:
      unsigned short value_ushort;
      value >> value_ushort;
      value_str = to_string(value_ushort);
      break;
    case ISType::S32:
      int value_int;
      value >> value_int;
      value_str = to_string(value_int);
      break;
    case ISType::U32:
      unsigned int value_uint;
      value >> value_uint;
      value_str = to_string(value_uint);
      break;
    case ISType::S64:
      long value_long;
      value >> value_long;
      value_str = to_string(value_long);
      break;
    case ISType::U64:
      unsigned long value_ulong;
      value >> value_ulong;
      value_str = to_string(value_ulong);
      break;
    case ISType::Float:
      float value_float;
      value >> value_float;
      value_str = to_string(value_float);
      break;
    case ISType::Double:
      double value_double;
      value >> value_double;
      value_str = to_string(value_double);
      break;
    case ISType::String:
      value >> value_str;
      break;
    default:
      ERS_LOG(m_uid << " type " << type << " not recognised");      
      break;
    }
    if(i != m_is_index) continue;
 
    int num_value = std::stoi(value_str);
    int num_threshold = std::stoi(m_is_value);

    if(num_value >= num_threshold ) {
      ERS_LOG(m_uid << " Stop TTC decoder with value = " << value_str);
      if ((status = m_alti->DECEnableWrite(false)) != AltiModule::SUCCESS) {
	string reason = m_uid + " during DECEnableWrite with code = " + to_string(status) ;
	ERS_REPORT_IMPL( ers::warning, ers::Message, reason, );
      }
    }
  }
}

