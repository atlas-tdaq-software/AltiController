#include "AltiController/Exceptions.h"
#include "AltiController/AltiMasterBusy.h"

#include "is/info.h"
#include "is/infodictionary.h"
#include "TTCInfo/GLOBALBUSY.h"

using namespace std;
using namespace LVL1;

AltiMasterBusy::AltiMasterBusy(LVL1::AltiModule& alti, string uid,  IPCPartition& p) : 
  m_alti(&alti),
  m_uid(uid),
  m_ipc_partition(p),
  m_busy_trigger(true),
  m_busy_counter_runcontrol(1),
  m_busy_lumiblock(false),
  m_busy_ecr(false),
  m_busy_hltcounter(false),
  m_busy_hltsv(false),
  m_global_busy(true)
{ 
  ERS_LOG(m_uid << " Entered");
  
  ERS_LOG(m_uid << " Done");
}

AltiMasterBusy::~AltiMasterBusy() {
  ERS_LOG(m_uid << " Entered");
  
  ERS_LOG(m_uid << " Done");
}

void AltiMasterBusy::reset() {
  m_mutex.lock();
  // set trigger busy to true to be sure triggers a blocked at the start
  m_busy_trigger=true;
  m_busy_counter_runcontrol=1;
  m_busy_lumiblock=false;
  m_busy_ecr=false;
  m_busy_hltcounter=false;
  m_busy_hltsv=false;
  m_global_busy=true;

  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
}

void AltiMasterBusy::setTriggerBusy() {
  m_mutex.lock();
  m_busy_trigger = true;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void AltiMasterBusy::unsetTriggerBusy() {
  m_mutex.lock();
  m_busy_trigger = false;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void AltiMasterBusy::setRunControlBusy() {
  m_mutex.lock();
  ++m_busy_counter_runcontrol;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void AltiMasterBusy::setRunControlBusy(int counter) {
  m_mutex.lock();
  m_busy_counter_runcontrol = counter;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void AltiMasterBusy::unsetRunControlBusy() {
  m_mutex.lock();
  if (m_busy_counter_runcontrol>0) {
    --m_busy_counter_runcontrol;
  }
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void AltiMasterBusy::setLumiBlockBusy() {
  m_mutex.lock();
  m_busy_lumiblock = true;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void AltiMasterBusy::unsetLumiBlockBusy() {
  m_mutex.lock();
  m_busy_lumiblock = false;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void AltiMasterBusy::setEcrBusy() {
  m_mutex.lock();
  m_busy_ecr = true;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void AltiMasterBusy::unsetEcrBusy() {
  m_mutex.lock();
  m_busy_ecr = false;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void AltiMasterBusy::setHLTCounterBusy() {
  m_mutex.lock();
  m_busy_hltcounter = true;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void AltiMasterBusy::unsetHLTCounterBusy() {
  m_mutex.lock();
  m_busy_hltcounter = false;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void AltiMasterBusy::setHLTSVBusy() {
  m_mutex.lock();
  m_busy_hltsv = true;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void AltiMasterBusy::unsetHLTSVBusy() {
  m_mutex.lock();
  m_busy_hltsv = false;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void AltiMasterBusy::updateGlobalBusy() {
  // this method should only be called by other methods locking m_mutex
  // this means also, that this method should not use m_mutex nor call
  // other methods that use m_mutex
  m_global_busy = m_busy_trigger || 
    m_busy_counter_runcontrol || 
    m_busy_lumiblock || 
    m_busy_ecr || 
    m_busy_hltcounter ||
    m_busy_hltsv;
  
  int status = 0;
  if (m_global_busy) {
    status |= m_alti->BSYConstLevelRegWrite(true);
  } else {
    status |= m_alti->BSYConstLevelRegWrite(false);
  }
  if(status !=  AltiModule::SUCCESS) {
    string reason = "writing global busy with code = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
  }
  return;
}

bool AltiMasterBusy::isBusy() {
  m_mutex.lock();
  bool global_busy = m_global_busy;
  m_mutex.unlock();
  return(global_busy);
}

bool AltiMasterBusy::isTriggerBusy() {
  m_mutex.lock();
  bool trigger = m_busy_trigger;
  m_mutex.unlock();
  return(trigger);
}


uint32_t AltiMasterBusy::isRunControlBusy() {
  m_mutex.lock();
  uint32_t runcontrol = m_busy_counter_runcontrol;
  m_mutex.unlock();
  return(runcontrol);
}

bool AltiMasterBusy::isLumiBlockBusy() {
  m_mutex.lock();
  bool lumiblock = m_busy_lumiblock;
  m_mutex.unlock();
  return(lumiblock);
}

bool AltiMasterBusy::isEcrBusy() {
  m_mutex.lock();
  bool ecr = m_busy_ecr;
  m_mutex.unlock();
  return(ecr);
}

bool AltiMasterBusy::isHLTCounterBusy() {
  m_mutex.lock();
  bool hltcounter = m_busy_hltcounter;
  m_mutex.unlock();
  return(hltcounter);
}

bool AltiMasterBusy::isHLTSVBusy() {
  m_mutex.lock();
  bool hltsv = m_busy_hltsv;
  m_mutex.unlock();
  return(hltsv);
}

bool AltiMasterBusy::isBusy(bool& trigger,
			   uint32_t& runcontrol, 
			   bool& lumiblock, 
			   bool& ecr,
			   bool& hltcounter,
			   bool& hltsv) {
  m_mutex.lock();
  trigger    = m_busy_trigger   ;
  runcontrol = m_busy_counter_runcontrol;
  lumiblock  = m_busy_lumiblock ;
  ecr        = m_busy_ecr       ;
  hltsv      = m_busy_hltsv;
  bool globalbusy = m_global_busy;
  m_mutex.unlock();
  return(globalbusy);
}
		      

void AltiMasterBusy::publishToIS() {
  // this method should only be called by other methods locking m_mutex
  // this means also, that this method should not use m_mutex nor call
  // other methods that use m_mutex
  string sout("AltiMasterBusy::publishToIS() ");
  try {
    GLOBALBUSY is_entry;
    is_entry.MasterTriggerBusy = m_busy_trigger;
    is_entry.RunControlBusy    = m_busy_counter_runcontrol;
    is_entry.LumiBlockBusy     = m_busy_lumiblock;
    is_entry.ECRBusy           = m_busy_ecr;
    is_entry.HLTCounterBusy    = m_busy_hltcounter;
    is_entry.HLTSVBusy         = m_busy_hltsv;

    ISInfoDictionary dict(m_ipc_partition);
    dict.checkin("RunParams.GlobalBusy", is_entry, true);
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(m_uid << " Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, " publishToIS ", ex._name()));
  } catch (daq::is::Exception & ex) {
    ERS_LOG(m_uid << " Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, " publishToIS ", ex.what()));
  } catch (std::exception& ex) {
    ERS_LOG(m_uid << " Could not publish to IS: std::exception ex.what()=" << ex.what());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, " publishToIS ", ex.what()));
  } catch (...) {
    ERS_LOG(m_uid << " Could not publish to IS. Unknown exception. ");
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, " publishToIS ", "Unknown exception" ));
  }
  return;
}


/*
bool AltiMasterBusy::AltiConstantBusyAsserted() {
  bool ret(false);
  m_mutex.lock();
  //ret = m_alti->BUSY_constant_level_is_asserted();
  m_mutex.unlock();

  return(ret);
}
*/
void AltiMasterBusy::dump() {
  ERS_LOG(m_uid << " Busy status:");
  cout << m_uid << "OR of all busies    : " << (this->isBusy()? "busy" : "not busy") << endl;
  cout << m_uid << " - trigger busy     : " << (this->isTriggerBusy()? "busy" : "not busy") << endl;
  cout << m_uid << " - run control busy : " << (this->isRunControlBusy()? "busy" : "not busy") << endl;
  cout << m_uid << " - lumi block busy  : " << (this->isLumiBlockBusy()? "busy" : "not busy") << endl;
  cout << m_uid << " - ECR busy         : " << (this->isEcrBusy()? "busy" : "not busy") << endl;
  cout << m_uid << " - HLTCounter busy  : " << (this->isHLTCounterBusy()? "busy" : "not busy") << endl;
  cout << m_uid << " - HLTSV busy       : " << (this->isHLTSVBusy()? "busy" : "not busy") << endl;
  return;
}
