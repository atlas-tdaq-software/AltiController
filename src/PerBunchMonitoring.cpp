#include "AltiController/PerBunchMonitoring.h"
#include "AltiController/Exceptions.h"

#include <iostream>
#include <string>
#include <fstream>

#include "ers/Issue.h"

#include "ROSCore/ReadoutModule.h"
 
#include <oh/OHRootProvider.h>
#include "is/infoany.h"
#include "TTCInfo/LumiBlock.h"

using namespace LVL1;
using namespace std;

LVL1::AltiModule* PerBunchMonitoring::m_alti = nullptr; 
string PerBunchMonitoring::m_uid = "";
IPCPartition PerBunchMonitoring::m_ipc_partition ="";
RCD::CMEMSegment* PerBunchMonitoring::m_segment = nullptr;
std::vector<TH1F*> PerBunchMonitoring::m_histo = {};

PerBunchMonitoring::PerBunchMonitoring(LVL1::AltiModule& alti, string uid) :
  m_is_receiver(nullptr)
{
  ERS_LOG(uid << " Entered");
  
  u_int status = 0;
  m_alti = &alti;
  m_uid = uid;
  
  ERS_LOG(m_uid << " Reset PBM counters");
  if ((status |= m_alti->PBMClearCounters() ) != AltiModule::SUCCESS) {
    string reason = " during PBMClearCounters with code = " + to_string(status) ;
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
  }

  ERS_LOG(m_uid << " Enable PBM counters");
  if ((status |= m_alti->DECEnableWrite(true) ) != AltiModule::SUCCESS) {
    string reason = " during DECEnableWrite with code = " + to_string(status) ;
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
  }

  // open segment
  m_segment = new RCD::CMEMSegment("Alti PBM counters", LVL1::ALTI::PBM_COUNTERSRAM_INDEX_NUMBER*sizeof(uint32_t), true);
  if((*m_segment)() != CMEM_RCC_SUCCESS) {
    ostringstream text;
    text << "error opening CMEM segment  of size " << m_segment->Size();
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  }
  printf("opened counters CMEM segment of size 0x%08x, phys 0x%08x, virt 0x%08x\n", m_segment->Size(), (unsigned int) m_segment->PhysicalAddress(), (unsigned int) m_segment->VirtualAddress());
  
  // read PBM counters at each LB with IS callback 
  m_ipc_partition = getenv("TDAQ_PARTITION"); 
  m_is_receiver = new (nothrow) ISInfoReceiver(m_ipc_partition);
  // Subscribe to IS update
  try {
    m_is_receiver->subscribe("RunParams.LumiBlock", IScallback);
  }
  catch (daq::is::Exception& ex) {
    ERS_REPORT_IMPL( ers::warning, ers::Message, m_uid + " failed to subscribe to IS publications for per-bunch monitoring, exception caught: " + ex.what(), );
  }
  catch (...) {
    ERS_REPORT_IMPL( ers::warning, ers::Message, m_uid + " failed to subscribe to IS publications for per-bunch decoder monitoring", );
  }         

  // initialize histograms
  OHRootProvider oh(m_ipc_partition, "AltiPerBunch", getenv("TDAQ_APPLICATION_NAME") );
  m_histo.resize(AltiModule::PBM_SIGNAL_NUMBER);
  for(u_int i = 0; i < AltiModule::PBM_SIGNAL_NUMBER; i++) { 
    string name = m_uid + "_" + AltiModule::PBM_SIGNAL_NAME[i]; 
    string title = "Rate in kHz in each BCID for " + AltiModule::PBM_SIGNAL_NAME[i]; 
    m_histo[i] = new TH1F(name.c_str(), title.c_str(), 3564, -0.5, 3563.5);
    
    try {
      oh.publish(m_histo[i], name);
    }
    catch(daq::oh::ObjectTypeMismatch & ex) {
      ERS_REPORT_IMPL( ers::warning, ers::Message, m_uid + " failed to subscribe to publish to OH for per-bunch monitoring, exception caught: " + ex.what(), );
    } 
    catch(daq::oh::RepositoryNotFound & ex) {
      ERS_REPORT_IMPL( ers::warning, ers::Message, m_uid + " failed to subscribe to publish to OH for per-bunch monitoring, exception caught: " + ex.what(), );
    }
    catch (...) {
      ERS_REPORT_IMPL( ers::warning, ers::Message, m_uid + " failed to subscribe to publish to OH for per-bunch monitoring", );
    } 
  }
  
  ERS_LOG(m_uid << " Done");  
}

//========================================================================================================================

PerBunchMonitoring::~PerBunchMonitoring() noexcept {
  ERS_LOG(m_uid<< " Entered");
  
  if (m_is_receiver != nullptr) {
    ERS_LOG(m_uid << " delete m_is_receiver ");
    delete m_is_receiver;
    m_is_receiver = nullptr;
  }

  ERS_LOG(m_uid << " delete m_histo");
  for(u_int i = 0; i < AltiModule::PBM_SIGNAL_NUMBER; i++) {
    if(m_histo[i]) delete m_histo[i];
  }
  
  ERS_LOG(m_uid << " delete m_segment");
  if(m_segment != nullptr) {
    delete m_segment;
    m_segment = nullptr;
  }
  
  ERS_LOG(m_uid << " Done");
}


//========================================================================================================================

void PerBunchMonitoring::IScallback(ISCallbackInfo * isc) {
  
  u_int status = 0;
  
  float rate = 0;
  u_int turn = 0;
  unsigned int *data;
  string name;

  LumiBlock is_lumi_block;
  isc->value(is_lumi_block); 
  u_int lumi_block = is_lumi_block.LumiBlockNumber;

  OHRootProvider oh(m_ipc_partition, "AltiPerBunch", getenv("TDAQ_APPLICATION_NAME") );

  // stop counters
   if ((status |= m_alti->PBMEnableCounters(false)) != AltiModule::SUCCESS) {
      string reason = " disabling the per-bunch monitoring counters";
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
    }

   // read turn counter
   if ((status |= m_alti->PBMReadTurnCounter(turn)) != AltiModule::SUCCESS) {
      string reason = " reading the per-bunch monitoring turn counter";
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
    }

  for(u_int i = 0; i < AltiModule::PBM_SIGNAL_NUMBER; i++) { 
    AltiModule::PBM_SIGNAL signal = AltiModule::PBM_SIGNAL(i);
    // read counters
    if ((status |= m_alti->PBMReadMemory(signal, m_segment)) != AltiModule::SUCCESS) {
      string reason = " reading the per-bunch monitoring counters";
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
    }

    // reset histogram
    m_histo[i]->Reset();

    // fill histograms
    data = (unsigned int *) m_segment->VirtualAddress();    
    for (u_int bcid = 0; bcid < 3563; bcid++){
      rate = 0;
      if(turn > 0) rate = ((float) data[bcid] / (float) turn) * 11.2455; 
      m_histo[i]->SetBinContent(bcid+1, rate );
    }    
    // set the underflow to the lumiblock
    m_histo[i]->SetBinContent(0, lumi_block);
    // set overflow to the turn counter
    m_histo[i]->SetBinContent(3565, turn);

    // publish to OH
    name = m_uid + "_" + AltiModule::PBM_SIGNAL_NAME[i]; 
    try {
      oh.publish(m_histo[i], name);
    }
    catch(daq::oh::ObjectTypeMismatch & ex) {
      ERS_REPORT_IMPL( ers::warning, ers::Message, m_uid + " failed to subscribe to publish to OH for per-bunch monitoring, exception caught: " + ex.what(), );
    } 
    catch(daq::oh::RepositoryNotFound & ex) {
      ERS_REPORT_IMPL( ers::warning, ers::Message, m_uid + " failed to subscribe to publish to OH for per-bunch monitoring, exception caught: " + ex.what(), );
    }
    catch (...) {
      ERS_REPORT_IMPL( ers::warning, ers::Message, m_uid + " failed to subscribe to publish to OH for per-bunch monitoring", );
    } 
  }

  // reset counters
  if ((status |= m_alti->PBMClearCounters()) != AltiModule::SUCCESS) {
      string reason = " clearing the per-bunch monitoring counters";
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
    }

  // re-enable counters
 if ((status |= m_alti->PBMEnableCounters(true)) != AltiModule::SUCCESS) {
   string reason = " enabling the per-bunch monitoring counters";
   ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
 }

}
