#include "AltiController/Exceptions.h"
#include "AltiController/AltiMasterEcr.h"
#include "ALTI/EventIdentifier.h"

//IS
#include "is/info.h"
#include "is/infodictionary.h"
#include "TTCInfo/ECR.h"

#include <unistd.h>

using namespace std;
using namespace LVL1;

AltiMasterEcr::AltiMasterEcr(LVL1::AltiModule& alti, string uid, IPCPartition& p) : 
  m_alti(&alti),
  m_uid(uid),
  m_ipc_partition(p),
  m_ecr_period(0),
  m_ecr_length(0),
  m_pre_ecr_deadtime(0),
  m_post_ecr_deadtime(0),
  m_ecr_counter(0),
  m_previous_ecr_counter(0),
  m_is_started(false)
{ 
  ERS_LOG(m_uid << " Entered");
  
  ERS_LOG(m_uid << " Done");
}

AltiMasterEcr::~AltiMasterEcr() {
   ERS_LOG(m_uid << " Entered");
  
   ERS_LOG(m_uid << " Done");
}

//========================================================================================================================

void AltiMasterEcr::reset() {
  boost::mutex::scoped_lock scoped_lock(m_mutex);
  
  // reset event counter value
  m_alti->T2LControlReset(true, true, true);
  
  m_ecr_counter = 0;
  m_previous_ecr_counter = 0;
  m_is_started = false;
}

//========================================================================================================================

void AltiMasterEcr::setPeriod(uint32_t period_ms) {
  boost::mutex::scoped_lock scoped_lock(m_mutex);
  // convert period from micro seconds to BC
  m_ecr_period = (period_ms*1000.) / 25.;
}

//========================================================================================================================

void AltiMasterEcr::setDeadtime(uint32_t pre_ecr_deadtime, uint32_t post_ecr_deadtime) {
  boost::mutex::scoped_lock scoped_lock(m_mutex);
  // convert deadtime from micro seconds to BC
  m_pre_ecr_deadtime  = (pre_ecr_deadtime * 1000.) / 25.;
  m_post_ecr_deadtime = (post_ecr_deadtime * 1000.) / 25.;
}

//========================================================================================================================

void AltiMasterEcr::setLength(uint32_t length) {
  boost::mutex::scoped_lock scoped_lock(m_mutex);
  m_ecr_length = length;
}

//========================================================================================================================

int AltiMasterEcr::startEcr() { 
  ERS_LOG(m_uid << " Entered");
  int status = 0;

  if(m_is_started) {
    // generate 1 ECR after resume trigger
    if((status |= m_alti->ECRGenerate()) != AltiModule::SUCCESS) {
      string reason = "generating ECR = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
    }
    
    // wait for ECR to be ready again
    if((status |= m_alti->ECRReadyWait()) != AltiModule::SUCCESS) {
      string reason = "waiting for ECR to be ready again = " + to_string(status);
      ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
    }
  }

  AltiModule::ECR_GENERATION ecr;
  ecr.type = AltiModule::ECR_TYPE::ECR_INTERNAL; // don't use ECR_VME in controller
  ecr.length = m_ecr_length;
  ecr.frequency = m_ecr_period;
  ecr.busy_before = m_pre_ecr_deadtime;
  ecr.busy_after = m_post_ecr_deadtime;
  ecr.orbit_offset = 1000; // default value
  ecr.dump();
  if((status |= m_alti->ECRGenerationWrite(ecr)) != AltiModule::SUCCESS) {
    string reason = "writing ECR generation = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
  }

  m_is_started = true;

  ERS_LOG(m_uid << " Done");
  return status;
}

//========================================================================================================================

uint32_t AltiMasterEcr::stopEcr() { 

  ERS_LOG(m_uid << " Entered");
  int status = 0;
  
  AltiModule::ECR_GENERATION ecr;
  ecr.type = AltiModule::ECR_TYPE::ECR_VME; 
  ecr.length = m_ecr_length;
  ecr.frequency = m_ecr_period;
  ecr.busy_before = m_pre_ecr_deadtime;
  ecr.busy_after = m_post_ecr_deadtime;
  ecr.orbit_offset = 1000; // default value
    
  if((status |= m_alti->ECRGenerationWrite(ecr)) != AltiModule::SUCCESS) {
    string reason = "writing ECR generation = " + to_string(status);
    ers::error(ALTI::AltiController::VmeError(ERS_HERE, m_uid, reason));
  }
  publishToIS();
  ERS_LOG(m_uid << " Done");

  return (0xff&m_ecr_counter); //8bit ECR counter is needed by TDAQ 
} 

//========================================================================================================================

void AltiMasterEcr::publishToIS() {
  boost::mutex::scoped_lock scoped_lock(m_mutex);
  
  int status = 0;

  // first wait for the pre-ECR deadtime to make sure to get the latest L1ID
  usleep((m_pre_ecr_deadtime*25)/1000);

  try {
   ECR is_entry;

   EventIdentifier evid;
   status |= m_alti->T2LEventIdentifierRead(evid);

   uint32_t nb_ecr = evid.ECRC();
   uint32_t l1a_u = static_cast<unsigned int>(evid.L1ID()) & 0x00ffffff;
   uint32_t ecr_u = (static_cast<unsigned int>(nb_ecr) & 0x000000ff) << 24;
 
   m_ecr_counter += (nb_ecr - m_previous_ecr_counter);
   if (nb_ecr < m_previous_ecr_counter) {
     m_ecr_counter += 256;
    }

   is_entry.ECRCounter = m_ecr_counter;
   m_previous_ecr_counter = nb_ecr;
   is_entry.ECRCounter8Bit = static_cast<char>(0xff&nb_ecr);
   is_entry.LatestL1ID = ecr_u | l1a_u;
   ISInfoDictionary dict(m_ipc_partition);
   dict.checkin("RunParams.ECR", is_entry, true);
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(m_uid << " Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, " publishToIS ", ex._name()));
  } catch (daq::is::Exception & ex) {
    ERS_LOG(m_uid << " Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, " publishToIS ", ex.what()));
  } catch (std::exception& ex) {
    ERS_LOG(m_uid << " Could not publish to IS: std::exception ex.what()=" << ex.what());
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, " publishToIS ", ex.what()));
  } catch (...) {
    ERS_LOG(m_uid << " Could not publish to IS. Unknown exception. ");
    ers::error(ALTI::AltiController::PublishToIS(ERS_HERE, m_uid, " publishToIS ", "Unknown exception" ));
  }

 if (status != 0) {
    ostringstream text;
    text << m_uid << " VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  } 

}
