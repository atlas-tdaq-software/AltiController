#!/bin/bash

export tdaq_release="tdaq-10-00-00"
export alti_release="l1ct-10-00-00"
export detcommon_version="23.0.13"
export partition_name="PartitionAlti_CTP" 
export logs_path="/logs/${TDAQ_VERSION}"
export pc_host="pc-tbed-pub-24.cern.ch"
export sbc_host="sbcl1ct-33.cern.ch"
export tag_name="x86_64-centos7-gcc8-opt"
export linux="centos7"

find . -type f  -name 'editPartition.sh' -exec  sed -i "s#$partition_name#PARTITION_NAME#g" {} \;
find . -type f  -name 'editPartition.sh' -exec  sed -i "s#$tdaq_release#TDAQ_RELEASE#g" {} \;
find . -type f  -name 'runPartition.sh' -exec  sed -i "s#$partition_name#PARTITION_NAME#g" {} \;
find . -type f  -name 'runPartition.sh' -exec  sed -i "s#$tdaq_release#TDAQ_RELEASE#g" {} \;
find . -type f  -name 'PartitionAlti.data.xml' -exec  sed -i "s#$PWD#LOCAL_PATH#g" {} \;
find . -type f  -name '*data.xml' -exec  sed -i "s#$partition_name#PARTITION_NAME#g" {} \;
find . -type f  -name '*data.xml' -exec  sed -i "s#$tdaq_release#TDAQ_RELEASE#g" {} \;
find . -type f  -name '*data.xml' -exec  sed -i "s#$logs_path#LOG_PATH#g" {} \;
find . -type f  -name '*data.xml' -exec  sed -i "s#$pc_host#PC_HOST#g" {} \;
find . -type f  -name '*data.xml' -exec  sed -i "s#$sbc_host#SBC_HOST#g" {} \;
find . -type f  -name '*data.xml' -exec  sed -i "s#$CTP_INST_PATH#CTP_INST_PATH#g" {} \;
find . -type f  -name '*data.xml' -exec  sed -i "s#$alti_release#ALTI_RELEASE#g" {} \;  
find . -type f  -name '*data.xml' -exec  sed -i "s#$detcommon_version#DETCOMMON_VERSION#g" {} \;  
find . -type f  -name '*data.xml' -exec  sed -i "s#$tag_name#TAG_NAME#g" {} \; 
find . -type f  -name '*data.xml' -exec  sed -i "s#$linux#LINUX#g" {} \; 

 
