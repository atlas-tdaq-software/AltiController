#!/bin/bash

 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiBoard2#AltiBoard#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiModeConfigBase2#AltiModeConfigBase#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiModeCtpSlave2#AltiModeCtpSlave#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiModeAltiSlave2#AltiModeAltiSlave#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiModeLemoSlave2#AltiModeLemoSlave#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiModeAltiMaster2#AltiModeAltiMaster#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiModeAltiExpert2#AltiModeAltiExpert#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiMasterTriggerConfig2#AltiMasterTriggerConfig#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiPatternGeneratorConfig2#AltiPatternGeneratorConfig#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#PGSignalMask2#PGSignalMask#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#TTCL1aSource2#TTCL1aSource#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiBGo2L1A2#AltiBGo2L1A#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiTransmitter2#AltiTransmitter#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiTiming2#AltiTiming#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiCalibrationRequest2#AltiCalibrationRequest#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiBChannelConfig2#AltiBChannelConfig#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiCycleBase2#AltiCycleBase#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiCycleContainer2#AltiCycleContainer#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiCycleLong2#AltiCycleLong#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiCycleShort2#AltiCycleShort#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiBGoECR2#AltiBGoECR#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiBGoBCR2#AltiBGoBCR#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiBGoChannel2#AltiBGoChannel#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiBGoChannelBase2#AltiBGoChannelBase#g" {} \; 
 find . -type f  -name '*data.xml' -exec  sed -i "s#AltiTTCdecoder2#AltiTTCdecoder#g" {} \; 
 
